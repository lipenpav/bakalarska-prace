module.exports = {
	globDirectory: 'build/',
	globPatterns: [
		'**/*.{json,svg,jpg,png,html,css,js,txt}'
	],
	maximumFileSizeToCacheInBytes: 5000000,
	swDest: 'build/sw.js',
	swSrc: "src/sw.js"
};