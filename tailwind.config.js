module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        c0: '#7F7C82',
        c1: '#C6C0CB',
        c2: '#BFA2DB',
        c3: '#F0D9FF',
        c4: '#F3F1F5',
        c5: '#86C271',
        c6: '#DE7D7D',
        cText: '#3B3A3D',
        cPositive: '#19B432',
        cNegative: '#CC1B1B',
      },
      fontSize: {
        xxs: ['.69rem', '.69rem'],
      },
      height: {
        minContent: 'min-content',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
