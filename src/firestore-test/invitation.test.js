const { setup, teardown } = require('./helpers');

const mockData = {
    'invitations/inv1': {
        creator_user_avatar: "avatar",
        creator_user_id: "c123",
        creator_user_name: "cName",
        family_id: "fId",
        family_name: "fName",
        invited_user_id: "uId"
    }
};

describe('invitation rules', () => {
    afterEach(async () => {
        await teardown();
    });

    test('Fail when not authenticated', async () => {
        const db = await setup();
        const ref = db.collection('invitations');

        await expect(ref.get()).toDeny();
        await expect(ref.add({})).toDeny();
    });

    test('Pass then authenticated', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('invitations');

        await expect(ref.get()).toAllow();
        await expect(ref.add({})).toDeny();
        await expect(ref.add({
            creator_user_avatar: "avatar",
            creator_user_id: "c123",
            creator_user_name: "cName",
            family_id: "fId",
            family_name: "fName",
            invited_user_id: "uId"})).toAllow();
    });

    test('Correct data', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('invitations');

        await expect(ref.add({
            creator_user_avatar: "",
            creator_user_id: "",
            creator_user_name: "",
            family_id: "",
            family_name: "",
            invited_user_id: ""})).toDeny();
        await expect(ref.add({
            creator_user_avatar: "avatar",
            creator_user_id: "c123",
            creator_user_name: "cName",
            family_id: "fId",
            family_name: "fName",
            invited_user_id: "uId"})).toAllow();
        await expect(ref.doc('inv1')).toAllow();
    });

    test('Delete only invited user', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('invitations');

        await expect(ref.doc('inv1').delete()).toDeny();

        const db2 = await setup({
            uid: 'uId',
            email: 'test@firestore.com'
        }, mockData);
        const ref2 = db2.collection('invitations');
        await expect(ref2.doc('inv1').delete()).toAllow();
    });


});