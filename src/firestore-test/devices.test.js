const { setup, teardown } = require('./helpers');

const mockData = {
    'devices/id': {
        token: "token",
        userId: "id",
    }
};

describe('devices rules', () => {
    afterEach(async () => {
        await teardown();
    });

    test('Fail when not authenticated', async () => {
        const db = await setup();
        const ref = db.collection('devices');

        await expect(ref.get()).toDeny();
        await expect(ref.add({})).toDeny();
    });

    test('Pass then authenticated', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('devices');

        await expect(ref.get()).toAllow();
        await expect(ref.add({})).toDeny();
        await expect(ref.add({
            token: "token",
            userId: "id"})).toAllow();
    });

    test('Correct data', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('devices');

        await expect(ref.add({
            token: "",
            userId: "",})).toDeny();
        await expect(ref.add({
            token: "token2",
            userId: "id2",})).toAllow();
        await expect(ref.doc('inv1')).toAllow();
    });

});