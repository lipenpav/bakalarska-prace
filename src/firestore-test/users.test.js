const { setup, teardown } = require('./helpers');

const mockData = {
    'users/user1': {
        authProvider: "firebase",
        avatar: "",
        birthday: "cName",
        current_family: "fId",
        email: "email",
        name: "userName",
        tel: "",
        uid: "user1"
    }
};

describe('users rules', () => {
    afterEach(async () => {
        await teardown();
    });

    test('Fail when not authenticated', async () => {
        const db = await setup();
        const ref = db.collection('users');

        await expect(ref.get()).toDeny();
        await expect(ref.add({})).toDeny();
    });

    test('Pass then authenticated', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('users');

        await expect(ref.get()).toAllow();
        await expect(ref.add({})).toDeny();
        await expect(ref.add({
            authProvider: "firebase",
            avatar: "",
            birthday: "cName",
            current_family: "fId",
            email: "email",
            name: "userName",
            tel: "",
            uid: "uid"})).toAllow();
    });

    test('Correct data', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('users');

        await expect(ref.add({
            authProvider: "",
            avatar: "",
            birthday: "",
            current_family: "",
            email: "",
            name: "",
            tel: "",
            uid: ""})).toDeny();
        await expect(ref.add({
            authProvider: "firebase",
            avatar: "",
            birthday: "cName",
            current_family: "fId",
            email: "email",
            name: "userName",
            tel: "",
            uid: "uid"})).toAllow();
        await expect(ref.doc('inv1')).toAllow();
    });

    test('Delete only owner user', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('users');

        await expect(ref.doc('user1').delete()).toDeny();

        const db2 = await setup({
            uid: 'user1',
            email: 'test@firestore.com'
        }, mockData);
        const ref2 = db2.collection('users');
        await expect(ref2.doc('user1').delete()).toAllow();
    });


});