const { setup, teardown } = require('./helpers');

const mockData = {
    'families/family1': {
        id: "id",
        name: "family",
        owner: "uid"
    }
};

describe('families rules', () => {
    afterEach(async () => {
        await teardown();
    });

    test('Fail when not authenticated', async () => {
        const db = await setup();
        const ref = db.collection('families');

        await expect(ref.get()).toDeny();
        await expect(ref.add({})).toDeny();
    });

    test('Pass then authenticated', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('families');

        await expect(ref.get()).toAllow();
        await expect(ref.add({})).toDeny();
        await expect(ref.add({
            id: "id",
            name: "family",
            owner: "uid"})).toAllow();
    });

    test('Correct data', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('families');

        await expect(ref.add({
            id: "",
            name: "",
            owner: ""})).toDeny();
        await expect(ref.add({
            id: "id",
            name: "family",
            owner: "uid"})).toAllow();
        await expect(ref.doc('family1')).toAllow();
    });

    test('Delete only owner of family', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('families');

        await expect(ref.doc('family1').delete()).toDeny();

        const db2 = await setup({
            uid: 'uid',
            email: 'test@firestore.com'
        }, mockData);
        const ref2 = db2.collection('families');
        await expect(ref2.doc('family1').delete()).toAllow();
    });
});