const { setup, teardown } = require('./helpers');

const mockData = {
  'users_family/userFamily1': {
      family_id: "fId", family_name: "fName", user_id: "uId"
  }
};

describe('users_family rules', () => {
    afterEach(async () => {
        await teardown();
    });

    test('Fail when not authenticated', async () => {
        const db = await setup();
        const ref = db.collection('users_family');

        await expect(ref.get()).toDeny();
        await expect(ref.add({})).toDeny();
    });

    test('Pass then authenticated', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('users_family');

        await expect(ref.get()).toAllow();
        await expect(ref.add({})).toDeny();
        await expect(ref.add({family_id: "something", family_name: "something", user_id: "something"})).toAllow()
    });

    test('Correct data', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('users_family');

        await expect(ref.add({family_id: "", family_name: "", user_id: ""})).toDeny();
        await expect(ref.add({family_id: "something", family_name: "something", user_id: "something"})).toAllow();
        await expect(ref.doc('userFamily1')).toAllow();
    });

});