const { setup, teardown } = require('./helpers');

const mockData = {
    'families/family/events/ev': {
        allDay: false,
        color: "blue",
        end: "2021-12-08 23:02",
        owner: "uId",
        start: "2021-12-08 22:02",
        title: "Nákup"
    }
};

describe('event rules', () => {
    afterEach(async () => {
        await teardown();
    });

    test('Fail when not authenticated', async () => {
        const db = await setup();
        const ref = db.collection('families').doc('family').collection('events');

        await expect(ref.get()).toDeny();
        await expect(ref.add({})).toDeny();
    });

    test('Pass then authenticated', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('families').doc('family').collection('events');

        await expect(ref.get()).toAllow();
        await expect(ref.add({})).toDeny();
        await expect(ref.add({
            allDay: false,
            color: "blue",
            end: "2021-12-08 23:02",
            owner: "uId",
            start: "2021-12-08 22:02",
            title: "Nákup"})).toAllow();
    });

    test('Correct data', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('families').doc('family').collection('events');

        await expect(ref.add({
            allDay: false,
            color: "",
            end: "",
            owner: "",
            start: "",
            title: ""})).toDeny();
        await expect(ref.add({
            allDay: false,
            color: "blue",
            end: "2021-12-08 23:02",
            owner: "uId",
            start: "2021-12-08 22:02",
            title: "Nákup"})).toAllow();
        await expect(ref.doc('ev')).toAllow();
    });

    test('Delete only owner user', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('families').doc('family').collection('events');

        await expect(ref.doc('user1').delete()).toDeny();

        const db2 = await setup({
            uid: 'uId',
            email: 'test@firestore.com'
        }, mockData);
        const ref2 = db2.collection('families').doc('family').collection('events');
        await expect(ref2.doc('ev').delete()).toAllow();
    });
});