const { setup, teardown } = require('./helpers');

const mockData = {
    'families/family/shoppings/task': {
        count: 0,
        name: "test",
        owner: "uId",
        visible: true
    }
};

describe('Shopping tasks rules', () => {
    afterEach(async () => {
        await teardown();
    });

    test('Fail when not authenticated', async () => {
        const db = await setup();
        const ref = db.collection('families').doc('family').collection('shoppings');

        await expect(ref.get()).toDeny();
        await expect(ref.add({})).toDeny();
    });

    test('Pass then authenticated', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('families').doc('family').collection('shoppings');

        await expect(ref.get()).toAllow();
        await expect(ref.add({
            count: 0,
            name: "test",
            owner: "uId",
            visible: true})).toAllow();
    });

    test('Correct data', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('families').doc('family').collection('shoppings');

        await expect(ref.add({
            count: 0,
            name: "",
            owner: "",
            visible: true})).toAllow();
        await expect(ref.add({
            count: 0,
            name: "test",
            owner: "uId",
            visible: true})).toAllow();
        await expect(ref.doc('task')).toAllow();
    });
});