const { setup, teardown } = require('./helpers');

const mockData = {
    'families/family/finances/fin': {
        datetime: "2021-12-21T21:28",
        description: "Coffe",
        lentToUser: "user1",
        price: 4,
        settleUp: false,
        user: "user2"
    }
};

describe('finanes rules', () => {
    afterEach(async () => {
        await teardown();
    });

    test('Fail when not authenticated', async () => {
        const db = await setup();
        const ref = db.collection('families').doc('family').collection('finances');

        await expect(ref.get()).toDeny();
        await expect(ref.add({})).toDeny();
    });

    test('Pass then authenticated', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('families').doc('family').collection('finances');

        await expect(ref.get()).toAllow();
        await expect(ref.add({})).toDeny();
        await expect(ref.add({
            datetime: "2021-12-21T21:28",
            description: "Coffe",
            lentToUser: "user1",
            price: 4,
            settleUp: false,
            user: "user2"})).toAllow();
    });

    test('Correct data', async () => {
        const db = await setup({
            uid: 'id123',
            email: 'test@firestore.com'
        }, mockData);
        const ref = db.collection('families').doc('family').collection('finances');

        await expect(ref.add({
            datetime: "",
            description: "",
            lentToUser: "",
            price: 0,
            settleUp: false,
            user: ""})).toDeny();
        await expect(ref.add({
            datetime: "2021-12-21T21:28",
            description: "Coffe",
            lentToUser: "user1",
            price: 4,
            settleUp: false,
            user: "user2"})).toAllow();
        await expect(ref.doc("fin")).toAllow();
    });

});