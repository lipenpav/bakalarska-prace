const { setup, teardown } = require('./helpers');

describe('Database basic rules', () => {
    let db;
    let ref;

    beforeAll(async () => {
        db = await setup();
        ref = db.collection('some-nonexistent-collection');
    });

    afterAll(async () => {
        await teardown();
    });

    test('fail when reading/writing an unauthorized collection', async () => {
       await expect(ref.get()).toDeny();
       await expect(ref.add({})).toDeny();

    });
})