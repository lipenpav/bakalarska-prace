import NavBasic from "../../components/Nav/NavBasic";
import {Field, Form, Formik} from "formik";
import {validateName} from "../../validation/Validation";
import ErrorMessage from "../../components/Messages/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage";
import React, {useContext, useState} from "react";
import {AuthContext} from "../../auth/Auth";
import {createShoppingList} from "../../database/ShoppingList/createShoppingList";
import {useHistory} from "react-router-dom";

const NewTaskList = () => {
    const { currentFamily } = useContext(AuthContext);
    const { currentUser } = useContext(AuthContext);
    const [errorMessage, setErrorMessage] = useState(null);
    const [successMessage, setSuccessMessage] = useState(null);
    const history = useHistory();

    return(
        <main className="bg-c3 h-screen overscroll-x-none sm:bg-c4 sm:w-auto sm:min-h-full">
            <NavBasic heading={"New shopping list"} urlBack={"/shopping-lists"}/>
            <section className="px-5 mt-8 sm:bg-c3 sm:max-w-screen-sm sm:mx-auto sm:p-20 sm:rounded-2xl sm:mt-28 sm:shadow-md sm:flex sm:flex-col">
                <h3 className="text-center text-cText">Type a name for your new shopping list and set it to be visible to everyone</h3>
                <Formik initialValues={{ name: '', visible: false}}
                        onSubmit={async (values, {resetForm}) => {
                            setErrorMessage(null);
                            setSuccessMessage(null);
                            createShoppingList(currentFamily.id, values.name, currentUser, values.visible, setSuccessMessage, setErrorMessage)
                            resetForm();
                            history.push("/shopping-lists")
                        }}>

                    {({ isSubmitting, errors, touched }) => (
                        <Form>
                            <label htmlFor="name" className="hidden">Name of shopping list</label>
                            <svg
                                  className="pointer-events-none absolute mt-10 ml-4 z-0" width="24" height="26"
                                  aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shopping-basket" role="img"
                                  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                <path fill="#7F7C82"
                                      d="M576 216v16c0 13.255-10.745 24-24 24h-8l-26.113 182.788C514.509 462.435 494.257 480 470.37 480H105.63c-23.887 0-44.139-17.565-47.518-41.212L32 256h-8c-13.255 0-24-10.745-24-24v-16c0-13.255 10.745-24 24-24h67.341l106.78-146.821c10.395-14.292 30.407-17.453 44.701-7.058 14.293 10.395 17.453 30.408 7.058 44.701L170.477 192h235.046L326.12 82.821c-10.395-14.292-7.234-34.306 7.059-44.701 14.291-10.395 34.306-7.235 44.701 7.058L484.659 192H552c13.255 0 24 10.745 24 24zM312 392V280c0-13.255-10.745-24-24-24s-24 10.745-24 24v112c0 13.255 10.745 24 24 24s24-10.745 24-24zm112 0V280c0-13.255-10.745-24-24-24s-24 10.745-24 24v112c0 13.255 10.745 24 24 24s24-10.745 24-24zm-224 0V280c0-13.255-10.745-24-24-24s-24 10.745-24 24v112c0 13.255 10.745 24 24 24s24-10.745 24-24z">
                                </path>
                            </svg>

                            <Field
                                name="name"
                                placeholder="Name of list"
                                type="text"
                                validate={validateName}
                                className="defaultInput pl-14 mt-7"
                            />
                            {touched.name && errors.name &&
                            <div className="errorMessageInput">⚠ {errors.name}</div>
                            }

                            <div className="flex flex-col -mt-5 mb-5 pb-4">
                                <label className="relative items-center mt-2 ml-1 text-cText text-sm">
                                    Visible for everyone
                                    <Field name="visible"
                                           type="checkbox"
                                           className="absolute opacity-0 cursor-pointer h-0 w-0"
                                    />
                                    <span className="checkMark absolute top-0 ml-2 h-5 w-5 bg-c2 border-c2 border-2"/>
                                </label>
                            </div>

                            {errorMessage &&
                            <ErrorMessage errorMessage={errorMessage} />
                            }

                            {successMessage &&
                            <SuccessMessage successMessage={successMessage} />
                            }

                            <button className="buttonLarge mb-4 w-full sm:w-full" type="submit" disabled={isSubmitting}>Create shopping list</button>
                        </Form>
                    )}
                </Formik>
            </section>
        </main>
    );
}

export default NewTaskList;