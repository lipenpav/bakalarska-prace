import React, {useContext, useEffect, useState} from "react";
import NavBasic from "../../components/Nav/NavBasic";
import AddCircleButton from "../../components/Buttons/AddCircleButton";
import TaskHead from "../../components/Task/TaskHead";
import {useHistory} from "react-router-dom";
import {AuthContext} from "../../auth/Auth";
import {getShoppingListsVisible} from "../../database/ShoppingList/getShoppingListsVisible";
import {getShoppingListsOfUser} from "../../database/ShoppingList/getShoppingListsOfUser";

const ShoppingLists = () => {
    const [navHeight, setNavHeight] = useState(0);
    const history = useHistory();
    const {currentUser, currentFamily} = useContext(AuthContext);
    const [privateShopping, setPrivateShopping] = useState([]);
    const [publicShopping, setPublicShopping] = useState([]);
    const [allShopping, setAllShopping] = useState([]);

    useEffect(() => {
        setNavHeight(document.getElementById("navRef").clientHeight);
    }, []);

    const handleClick = () => {
        history.push('shopping-lists/new');
    }

    useEffect(() => {
        getShoppingListsVisible(currentFamily.id, setPublicShopping);
        getShoppingListsOfUser(currentFamily.id, currentUser, setPrivateShopping);
    }, [currentFamily, currentUser]);

    useEffect(() => {
        let tmp = [];
        tmp = [...tmp, ...publicShopping];
        tmp = [...tmp, ...privateShopping];

        const filtered = tmp.filter(function({id}) {
            const key =`${id}`;
            return !this.has(key) && this.add(key);
        }, new Set());

        setAllShopping(Array.from(filtered));
    }, [privateShopping, publicShopping]);

    return(
        <main className="bg-c4 h-screen overscroll-x-none">
            <NavBasic heading={"Shopping lists"} urlBack={"/dashboard"}/>
            <section className="overflow-y-scroll xl:max-w-screen-xl xl:mx-auto" style={{height: `calc(100vh - ${navHeight}px`}}>
                {allShopping.length !== 0 &&
                allShopping.map((item, index) => {
                    return(
                        <TaskHead key={index} id={item.id} name={item.data().name} numberTasks={item.data().count} shared={item.data().visible} isShoppingList={true} />
                    );
                })
                }
            </section>
            <AddCircleButton onClick={handleClick} />
        </main>
    );
}

export default ShoppingLists;