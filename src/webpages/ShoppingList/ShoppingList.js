import React, {useContext, useEffect, useState} from "react";
import {NavLink, useHistory, useParams} from "react-router-dom";
import AddTask from "../../components/Task/AddTask";
import TaskItem from "../../components/Task/TaskItem";
import {AuthContext} from "../../auth/Auth";
import {getShoppingList} from "../../database/ShoppingList/getShoppingList";
import {setShoppingVisibility} from "../../database/ShoppingList/setShoppingVisibility";
import {deleteShoppingList} from "../../database/ShoppingList/deleteShoppingList";

function getWindowHeight() {
    const { innerHeight: height } = window;
    return { height };
}

function useWindowHeight() {
    const [windowHeight, setWindowHeight] = useState(getWindowHeight());

    useEffect(() => {
        function handleResize() {
            setWindowHeight(getWindowHeight());
        }

        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return windowHeight;
}

const ShoppingList = () => {
    const {currentUser, currentFamily} = useContext(AuthContext);
    const { height } = useWindowHeight();
    const [navHeight, setNavHeight] = useState(0);
    const [inputHeight, setInputHeight] = useState(58);
    const {id} = useParams();
    const [list, setList] = useState([]);
    const [idList, setIdList] = useState();
    const [tasks, setTasks] = useState([]);
    const history = useHistory();

    useEffect(() => {
        setNavHeight(document.getElementById("navRef").clientHeight);
    }, []);

    useEffect(() => {
        setInputHeight(document.getElementById("inputNewTask").clientHeight);
    }, []);

    useEffect(() => {
        getShoppingList(id, currentFamily.id, setList, setTasks, setIdList);
    }, [currentFamily.id, id]);

    const handleVisible = () => {
        if(currentUser.uid === list.owner){
            setShoppingVisibility(currentFamily.id, idList, !list.visible);
        }
    }

    const handleDelete = () => {
        deleteShoppingList(currentFamily.id, id);
        history.push('/shopping-lists');
    }

    return(
        <main className="bg-c4 h-screen overscroll-x-none">
            <section id={"navRef"} className="bg-c2 px-5 w-full shadow z-10">
                <nav className="flex justify-between  py-1.5 xl:max-w-screen-xl xl:mx-auto">
                    <NavLink className="relative z-10" to={"/shopping-lists"}>
                        <svg className="my-1" width="22" height="23" viewBox="0 0 27 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clipPath="url(#clip0)">
                                <path d="M15.522 24.6158L14.184 25.8298C13.6175 26.3439 12.7014 26.3439 12.141 25.8298L0.424888 15.204C-0.141629 14.69 -0.141629 13.8587 0.424888 13.3501L12.141 2.71889C12.7075 2.20483 13.6235 2.20483 14.184 2.71889L15.522 3.93295C16.0945 4.45248 16.0825 5.30014 15.4979 5.80873L8.2356 12.0869H25.5566C26.3581 12.0869 27.003 12.672 27.003 13.3994V15.1494C27.003 15.8767 26.3581 16.4619 25.5566 16.4619H8.2356L15.4979 22.74C16.0885 23.2486 16.1006 24.0962 15.522 24.6158Z" fill="white"/>
                            </g>
                            <defs>
                                <clipPath id="clip0">
                                    <rect width="27" height="28" fill="white"/>
                                </clipPath>
                            </defs>
                        </svg>
                    </NavLink>
                    {list && currentUser.uid === list.owner ?
                        <button className="text-lg font-normal self-start my-1 text-white h-2" onClick={handleDelete}>
                            Delete
                        </button>
                        :
                        <div className="h-2" />
                    }
                </nav>
                <div className="flex flex-col mt-4 pb-4 xl:max-w-screen-xl xl:mx-auto">
                    <h1 className="text-white text-3xl h-8">{list && list.name}</h1>
                    {list && list.owner === currentUser.uid ?
                        <label className="relative items-center mt-2 ml-1 text-white font-light text-sm">
                            Visible for everyone
                            <input className="absolute opacity-0 cursor-pointer h-0 w-0"
                                   type={"checkbox"}
                                   checked={list.visible}
                                   onClick={handleVisible}
                            />
                            <span className="checkMark absolute top-0 ml-2 h-5 w-5 bg-c2 border-white border-2"/>
                        </label>
                        :
                        <label className="relative items-center mt-2 ml-1 text-gray-300 font-light text-sm">
                            Visible for everyone
                            <input className="absolute opacity-0 cursor-pointer h-0 w-0"
                                   type={"checkbox"}
                                   checked={list && list.visible}
                                   onClick={handleVisible}
                                   disabled={true}
                            />
                            <span className="checkMarkGray absolute top-0 ml-2 h-5 w-5 bg-c2 border-gray-300 border-2"/>
                        </label>
                    }
                </div>
            </section>
            <section className="pt-1 overflow-y-scroll xl:max-w-screen-xl xl:mx-auto xl:shadow-xl" style={{height : (height - inputHeight - navHeight)}}>
                {tasks.length !== 0 &&
                tasks.map((item, index) => {
                    return (
                        <TaskItem key={index}
                                  name={item.data().name}
                                  isChecked={item.data().checked}
                                  idTasks={id} id={item.id}
                                  isPrice={true}
                                  price={item.data().price}/>
                    );
                })
                }
            </section>

            <AddTask isPrice={true} taskId={idList}/>
        </main>

    );

}

export default ShoppingList;