import React, {useContext, useEffect, useRef, useState} from "react";
import {Link, NavLink, useHistory} from "react-router-dom";
import {Field, Form, Formik} from "formik";
import {validateEmail, validateName, validatePhone} from "../validation/Validation";
import {AuthContext} from "../auth/Auth";
import {getUserData} from "../database/User/getUserData";
import firebase from "../firebase";
import {UpdateProfilePhoto} from "../database/User/updateProfilePhoto";
import Avatar from "../components/User/Avatar";
import {updateUserProfile} from "../database/User/updateUserProfile";
import ErrorMessage from "../components/Messages/ErrorMessage";
import SuccessMessage from "../components/Messages/SuccessMessage";
import WarningMessage from "../components/Messages/WarningMessage";
import {leaveFamily} from "../database/Family/leaveFamily";
import {Notifications} from "../Notification";
import {getDevicesByAllFamily} from "../database/FCM/getDevicesByAllFamily";

const EditProfile = () => {
    const { currentUser, currentFamily } = useContext(AuthContext);
    const [user, setUser] = useState({});
    const history = useHistory();
    const inputFile = useRef(null);
    const [imageAsUrl, setImageAsUrl] = useState('');
    const [errorMessage, setErrorMessage] = useState(null);
    const [successMessage, setSuccessMessage] = useState(null);
    const [warningMessage, setWarningMessage] = useState(null);
    const [devices, setDevices] = useState([]);

    useEffect(() => {
        getDevicesByAllFamily(currentFamily.id, setDevices);
    }, [currentFamily]);

    useEffect(() =>{
        getUserData(currentUser.uid, setUser);
    },[currentUser, imageAsUrl]);

    const onButtonClick = () => {
        inputFile.current.click();
    };

    const handleFireBaseUpload = e => {
        console.log( inputFile.current.files[0]);
        const uploadTask = firebase.storage().ref(`profilePhoto/${inputFile.current.files[0].name}`).put(inputFile.current.files[0]);
        uploadTask.on(
            "state_changed",
            snapshot => {
            },
            error => {
                console.log(error);
            },
            () => {
                firebase.storage()
                    .ref("profilePhoto")
                    .child(inputFile.current.files[0].name)
                    .getDownloadURL()
                    .then(url => {
                        console.log(url)
                        setImageAsUrl(url);
                        UpdateProfilePhoto(currentUser, url);
                    });
            }
        );
    }

    function handleLogOut(){
        firebase.auth().signOut().then((r) => {
            console.log(r);
        });
    }

    return(
        <main className="bg-c4 min-h-screen overscroll-x-none">
            <nav className=" bg-c2 px-5 py-1.5 w-full shadow z-10 ">
                <div className="flex justify-between xl:max-w-screen-xl xl:mx-auto">
                    <NavLink className="relative z-10" to={"/dashboard"}>
                        <svg className="my-1" width="27" height="28" viewBox="0 0 27 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clipPath="url(#clip0)">
                                <path d="M15.522 24.6158L14.184 25.8298C13.6175 26.3439 12.7014 26.3439 12.141 25.8298L0.424888 15.204C-0.141629 14.69 -0.141629 13.8587 0.424888 13.3501L12.141 2.71889C12.7075 2.20483 13.6235 2.20483 14.184 2.71889L15.522 3.93295C16.0945 4.45248 16.0825 5.30014 15.4979 5.80873L8.2356 12.0869H25.5566C26.3581 12.0869 27.003 12.672 27.003 13.3994V15.1494C27.003 15.8767 26.3581 16.4619 25.5566 16.4619H8.2356L15.4979 22.74C16.0885 23.2486 16.1006 24.0962 15.522 24.6158Z" fill="white"/>
                            </g>
                            <defs>
                                <clipPath id="clip0">
                                    <rect width="27" height="28" fill="white"/>
                                </clipPath>
                            </defs>
                        </svg>
                    </NavLink>
                    <div className="flex flex-col mt-8 mb-2">
                        <div className="relative self-center">

                            <Avatar user={user} small={false}/>
                            <div onClick={onButtonClick} className="absolute -top-1 -right-1 p-2 bg-c5 rounded-full cursor-pointer">
                                <input type="file" id="file" ref={inputFile} className="hidden" onChange={handleFireBaseUpload}/>
                                <svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clipPath="url(#clip0)">
                                        <path d="M6.81447 2.00304L9.81494 4.75347L3.29955 10.7259L0.624395 10.9966C0.26627 11.0329 -0.036308 10.7553 0.00353579 10.4271L0.301192 7.97312L6.81447 2.00304ZM11.6707 1.59355L10.2619 0.302124C9.82244 -0.100708 9.10971 -0.100708 8.67025 0.302124L7.34486 1.51707L10.3453 4.26749L11.6707 3.05255C12.1102 2.64951 12.1102 1.99638 11.6707 1.59355Z" fill="white"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="12" height="11" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                        </div>
                        <h2 className="text-white self-center">{user.name}</h2>
                    </div>
                    <button form="formikEdit" className="self-start my-1" type="submit">
                        <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clipPath="url(#clip0)">
                                <path d="M9.51003 24.0299L0.410033 14.9299C-0.136678 14.3832 -0.136678 13.4968 0.410033 12.95L2.38988 10.9701C2.9366 10.4233 3.82308 10.4233 4.36979 10.9701L10.5 17.1002L23.6302 3.97009C24.1769 3.42338 25.0634 3.42338 25.6101 3.97009L27.5899 5.95C28.1366 6.49671 28.1366 7.38314 27.5899 7.9299L11.4899 24.03C10.9432 24.5767 10.0567 24.5767 9.51003 24.0299Z" fill="white"/>
                            </g>
                            <defs>
                                <clipPath id="clip0">
                                    <rect width="28" height="28" fill="white"/>
                                </clipPath>
                            </defs>
                        </svg>
                    </button>
                </div>

            </nav>

            <section className="bg-white px-3 pt-3 xl:max-w-screen-xl xl:mx-auto">
                {errorMessage &&
                <ErrorMessage errorMessage={errorMessage} />
                }

                {successMessage &&
                <SuccessMessage successMessage={"Your data has been updated"} />
                }
                {warningMessage &&
                <WarningMessage warningMessage={warningMessage} />
                }
                <Formik initialValues={{ name: user.name, email: user.email, phone: user.tel, birthday: user.birthday}}
                        enableReinitialize={true}
                        onSubmit={async (values) => {
                            setSuccessMessage(null);
                            setErrorMessage(null);
                            setWarningMessage(null);
                            if(user.authProvider === "google.com"){
                                updateUserProfile(currentUser, values.name, user.email, values.phone, values.birthday,setSuccessMessage, setErrorMessage, setWarningMessage);
                            }else{
                                updateUserProfile(currentUser, values.name, values.email, values.phone, values.birthday,setSuccessMessage, setErrorMessage, setWarningMessage);
                            }
                        }}>

                    {({  errors, touched }) => (
                        <Form id="formikEdit">
                            <label htmlFor="name" className="text-c0 font-bold text-lg">Name</label>
                            <Field
                                name="name"
                                placeholder="Name"
                                type="text"
                                validate={validateName}
                                className="defaultInput px-4"
                            />
                            {touched.name && errors.name &&
                            <div className="errorMessageInput">⚠ {errors.name}</div>
                            }

                            <label htmlFor="email" className="text-c0 font-bold text-lg">Email</label>
                            {user.authProvider === "google.com" ?
                                <Field type="text"
                                       name="email"
                                       placeholder="Email"
                                       className="defaultInput px-4"
                                       value={"You can not change email"}
                                />
                                :
                                <Field type="email"
                                       name="email"
                                       placeholder="Email"
                                       validate={validateEmail}
                                       className="defaultInput px-4"
                                />
                            }

                            {touched.email && errors.email &&
                            <div className="errorMessageInput">⚠ {errors.email}</div>
                            }

                            <label htmlFor="phone" className="text-c0 font-bold text-lg">Phone</label>
                            <Field type="phone"
                                   name="phone"
                                   placeholder="+420777000888"
                                   validate={validatePhone}
                                   className="defaultInput px-4"
                            />
                            {touched.phone && errors.phone &&
                            <div className="errorMessageInput">⚠ {errors.phone}</div>
                            }

                            <label htmlFor="birthday" className="text-c0 font-bold text-lg">Birthday</label>
                            <Field type="date"
                                   name="birthday"
                                   className="defaultInput px-4"
                            />
                            {touched.birthday && errors.birthday &&
                            <div className="errorMessageInput">⚠ {errors.birthday}</div>
                            }
                        </Form>
                    )}
                </Formik>
            </section>
            <section className="mt-6 xl:max-w-screen-xl xl:mx-auto">
                <Link className="flex items-center h-16 bg-white px-3 mb-3" to={"/change-password"}>
                    <svg width="26" height="31" viewBox="0 0 26 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M23.2143 15.5H8.82143V9.25766C8.82143 6.86001 10.6612 4.86802 12.9594 4.8438C15.2808 4.81958 17.1786 6.78735 17.1786 9.20317V10.1719C17.1786 10.9772 17.7996 11.625 18.5714 11.625H20.4286C21.2004 11.625 21.8214 10.9772 21.8214 10.1719V9.20317C21.8214 4.11724 17.846 -0.0181044 12.971 5.96094e-05C8.09598 0.0182236 4.17857 4.20806 4.17857 9.29399V15.5H2.78571C1.24777 15.5 0 16.8018 0 18.4063V28.0938C0 29.6982 1.24777 31 2.78571 31H23.2143C24.7522 31 26 29.6982 26 28.0938V18.4063C26 16.8018 24.7522 15.5 23.2143 15.5ZM15.3214 24.7031C15.3214 26.0412 14.2826 27.125 13 27.125C11.7174 27.125 10.6786 26.0412 10.6786 24.7031V21.7969C10.6786 20.4588 11.7174 19.375 13 19.375C14.2826 19.375 15.3214 20.4588 15.3214 21.7969V24.7031Z" fill="#7F7C82"/>
                    </svg>
                    <h3 className="ml-4 text-c0">Change my password</h3>
                </Link>
                <div className="h-16 px-5 flex items-center bg-white mb-10 cursor-pointer" onClick={handleLogOut}>
                    <svg className="mr-2" width="30" height="25" viewBox="0 0 30 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.5848 13.6059L19.5843 24.5416C18.6914 25.518 17.1437 24.8345 17.1437 23.435V17.186H9.04807C8.25636 17.186 7.61943 16.4895 7.61943 15.6238V9.37483C7.61943 8.50909 8.25636 7.81259 9.04807 7.81259H17.1437V1.56362C17.1437 0.170627 18.6854 -0.519362 19.5843 0.457038L29.5848 11.3927C30.1384 12.0046 30.1384 12.994 29.5848 13.6059ZM11.4291 24.2161V21.6124C11.4291 21.1828 11.1077 20.8313 10.7148 20.8313H5.71457C4.66095 20.8313 3.80971 19.9004 3.80971 18.7483V6.25035C3.80971 5.09819 4.66095 4.16736 5.71457 4.16736H10.7148C11.1077 4.16736 11.4291 3.81585 11.4291 3.38624V0.782505C11.4291 0.352889 11.1077 0.00138444 10.7148 0.00138444H5.71457C2.55965 0.00138444 0 2.8004 0 6.25035V18.7483C0 22.1982 2.55965 24.9972 5.71457 24.9972H10.7148C11.1077 24.9972 11.4291 24.6457 11.4291 24.2161Z" fill="#7F7C82"/>
                    </svg>
                    <h3 className="text-c0">Sing out</h3>
                </div>
            </section>
        </main>
    );
}

export default EditProfile;