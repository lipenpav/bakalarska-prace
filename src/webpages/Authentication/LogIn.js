import React, {useContext, useState} from "react";
import Nav from "../../components/Nav/Nav";
import {Formik, Form, Field} from "formik";
import {Link, Redirect} from "react-router-dom";
import firebase, {getToken} from '../../firebase';
import { AuthContext } from "../../auth/Auth";
import ErrorMessage from "../../components/Messages/ErrorMessage";
import {validateEmail,validatePassword} from "../../validation/Validation";
import {saveToken} from "../../database/FCM/saveToken";


const LogIn = () => {
    const { currentUser } = useContext(AuthContext);
    const [fireError, setFireError] = useState(null);

    function logInWithGoogle() {
        let provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then((cred) => {
                if(cred.additionalUserInfo.isNewUser){
                    firebase.firestore().collection('users').doc(cred.user.uid).set({
                        uid: cred.user.uid,
                        name: cred.additionalUserInfo.profile.given_name,
                        avatar: cred.additionalUserInfo.profile.picture,
                        email: cred.user.email,
                        birthday: "",
                        tel: "",
                        authProvider: "google.com"
                    }).then()
                }
                getToken().then((token) => {
                    saveToken(cred.user, token);
                });
            }).catch((err) => {
                console.error(err);
            });
    }


    return(
        <div>
            {!!currentUser ? (
                <Redirect to={{pathname: "/dashboard"}} />
            ) : (
                <main className="bg-c3 w-screen min-h-screen pb-5 sm:bg-c4 sm:w-auto sm:min-h-full">
                    <Nav />
                    <section className="pt-16 mx-5 sm:bg-c3 sm:max-w-screen-sm sm:mx-auto sm:p-20 sm:rounded-2xl sm:mt-28 sm:shadow-md sm:flex sm:flex-col">
                        <h1 className="text-center">Welcome back!</h1>
                        <p className="smallestText text-center text-c0 m-4">Log in to your exist account of Family App</p>
                        <Formik initialValues={{ email: '', password: '' }}
                            onSubmit={(values, isSubmitting) => {
                            setFireError(null);

                            firebase
                                .auth()
                                .signInWithEmailAndPassword(values.email, values.password)
                                .then(user => {
                                    getToken().then((token) => {
                                        saveToken(user.user, token);
                                    });
                                })
                                .catch((err) => {
                                    console.error(err);
                                    setFireError(err.message);
                                    isSubmitting.setSubmitting(false);
                                });
                        }}>

            {({ isSubmitting, errors, touched }) => (
                <Form>
                <label htmlFor="email" className="hidden">Email</label>
                <svg className="pointer-events-none absolute mt-11 ml-4 z-0" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M18.125 2.5H1.875C0.839453 2.5 0 3.33945 0 4.375V15.625C0 16.6605 0.839453 17.5 1.875 17.5H18.125C19.1605 17.5 20 16.6605 20 15.625V4.375C20 3.33945 19.1605 2.5 18.125 2.5ZM18.125 4.375V5.96895C17.2491 6.68219 15.8528 7.79125 12.8677 10.1287C12.2098 10.6462 10.9067 11.8893 10 11.8748C9.09344 11.8895 7.78988 10.646 7.13231 10.1287C4.14766 7.7916 2.75098 6.6823 1.875 5.96895V4.375H18.125ZM1.875 15.625V8.37492C2.77008 9.08785 4.03941 10.0883 5.97414 11.6033C6.82793 12.2754 8.32312 13.759 10 13.75C11.6686 13.759 13.1449 12.2969 14.0255 11.6036C15.9602 10.0886 17.2299 9.08793 18.125 8.37496V15.625H1.875Z" fill="#7F7C82"/>
                </svg>
                <Field
                name="email"
                placeholder="Email"
                type="email"
                validate={validateEmail}
                className="defaultInput pl-12 mt-7"
                />
            {touched.email && errors.email &&
                <div className="errorMessageInput">⚠ {errors.email}</div>
            }

                <label htmlFor="password" className="hidden">Password</label>
                <svg className="pointer-events-none absolute mt-4 ml-4" width="17" height="20" viewBox="0 0 17 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clipPath="url(#clip0)">
                <path d="M15.1786 10H5.76786V5.9727C5.76786 4.42582 6.97076 3.14066 8.47344 3.12504C9.99129 3.10941 11.2321 4.37895 11.2321 5.93754V6.56254C11.2321 7.08207 11.6382 7.50004 12.1429 7.50004H13.3571C13.8618 7.50004 14.2679 7.08207 14.2679 6.56254V5.93754C14.2679 2.65629 11.6685 -0.0116803 8.48103 3.84578e-05C5.29353 0.0117572 2.73214 2.71488 2.73214 5.99613V10H1.82143C0.815848 10 0 10.8399 0 11.875V18.125C0 19.1602 0.815848 20 1.82143 20H15.1786C16.1842 20 17 19.1602 17 18.125V11.875C17 10.8399 16.1842 10 15.1786 10ZM10.0179 15.9375C10.0179 16.8008 9.33862 17.5 8.5 17.5C7.66138 17.5 6.98214 16.8008 6.98214 15.9375V14.0625C6.98214 13.1993 7.66138 12.5 8.5 12.5C9.33862 12.5 10.0179 13.1993 10.0179 14.0625V15.9375Z" fill="#7F7C82"/>
                </g>
                <defs>
                <clipPath id="clip0">
                <rect width="17" height="20" fill="white"/>
                </clipPath>
                </defs>
                </svg>
                <Field type="password"
                name="password"
                placeholder="Password"
                validate={validatePassword}
                className="defaultInput pl-12"
                />
            {touched.password && errors.password &&
                <div className="errorMessageInput">⚠ {errors.password}</div>
            }
                <div className="flex justify-end mb-4">
                    <Link to={"/forgot-password"} className="text-black smallestText ">Forgot Password?</Link>
                </div>
                    {fireError &&
                        <ErrorMessage errorMessage={fireError} />
                    }
                <button className="buttonLarge mb-10 sm:w-full" type="submit" disabled={isSubmitting}>Log in</button>
                </Form>
                )}
                </Formik>

                <p className="smallestText text-c0 text-center mb-10">Or connect using</p>

                <div className="w-full flex font-medium">
                <button onClick={logInWithGoogle}
                        className="bg-white rounded-2xl py-3 w-full text-xl text-c0 flex justify-center shadow-lg">
                <svg className="mr-2" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fillRule="evenodd" clipRule="evenodd" d="M23.5196 12.2725C23.5196 11.4216 23.4432 10.6035 23.3014 9.81802H11.9998V14.4598H18.4578C18.1797 15.9598 17.3342 17.2306 16.0633 18.0815V21.0924H19.9415C22.2105 19.0033 23.5196 15.927 23.5196 12.2725Z" fill="#4285F4"/>
                <path fillRule="evenodd" clipRule="evenodd" d="M11.9997 23.9994C15.2396 23.9994 17.9559 22.9249 19.9414 21.0922L16.0632 18.0813C14.9887 18.8013 13.6142 19.2268 11.9997 19.2268C8.87425 19.2268 6.22884 17.1159 5.28522 14.2796H1.27618V17.3886C3.2507 21.3104 7.30882 23.9994 11.9997 23.9994Z" fill="#34A853"/>
                <path fillRule="evenodd" clipRule="evenodd" d="M5.28538 14.2799C5.04538 13.5599 4.90902 12.7909 4.90902 12C4.90902 11.2091 5.04538 10.44 5.28538 9.72001V6.61096H1.27634C0.46363 8.23094 0 10.0636 0 12C0 13.9363 0.46363 15.769 1.27634 17.389L5.28538 14.2799Z" fill="#FBBC05"/>
                <path fillRule="evenodd" clipRule="evenodd" d="M11.9997 4.77266C13.7615 4.77266 15.3433 5.3781 16.5869 6.56718L20.0286 3.12541C17.9505 1.18907 15.2342 0 11.9997 0C7.30882 0 3.2507 2.68905 1.27618 6.61081L5.28522 9.71986C6.22884 6.88353 8.87425 4.77266 11.9997 4.77266Z" fill="#EA4335"/>
                </svg>
                Google
                </button>
                </div>

                <div className="flex justify-center align-center mt-10">
                <span className="smallestText">Don’t have an account?</span>
                <Link to={"/singup"} className="smallestText ml-2 text-indigo-600"> Sing Up</Link>
                </div>
                </section>
                </main>
                )}
        </div>
    );
}

export default LogIn;