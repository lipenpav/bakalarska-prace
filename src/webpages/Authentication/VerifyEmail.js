import React, {useContext, useState} from "react";
import firebase from "../../firebase";
import {AuthContext} from "../../auth/Auth";
import {Link} from "react-router-dom";
import NavBasic from "../../components/Nav/NavBasic";
import ErrorMessage from "../../components/Messages/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage";


const VerifyEmail = () => {
    const { currentUser } = useContext(AuthContext);
    const [fireMessage, setFireMessage] = useState(false);
    const [verified, setVerified] = useState(false);
    const [fireError, setFireError] = useState(null);

    function sendVerificationEmail(){
        if(firebase.auth().currentUser.emailVerified){
            const res = firebase.auth().currentUser.emailVerified;
            setVerified(res);
        }else{
            currentUser.sendEmailVerification().then(()=>{
                setFireMessage(true);
            }).catch((err) => {
                setFireError(err);
            });
        }
    }

    return(
        <main className="bg-c3 w-screen min-h-screen pb-5 sm:bg-c4 sm:w-auto sm:min-h-full">
            <NavBasic urlBack={"/dashboard"} heading={"Verify your email"}/>
            <section className="pt-16 mx-5 sm:bg-c3 sm:max-w-screen-sm sm:mx-auto sm:p-20 sm:rounded-2xl sm:mt-28 sm:shadow-md sm:flex sm:flex-col">

                {fireMessage &&
                    <SuccessMessage successMessage={"We just sent a verification link to your email."} />
                }

                {fireError &&
                    <ErrorMessage errorMessage={fireError.message} />
                }

                {verified ? (
                    <>
                        <div className="p-1 px-4 py-2 text-green-500 text-sm bg-green-100 rounded-2xl my-4">
                            ✓ Your email has already been verified
                        </div>
                        <Link className="buttonLarge mb-10 w-full mx-auto block" to={"/dashboard"}>Go back</Link>
                    </>
                    ):(
                        <>
                            <p className="smallestText text-center text-c0 m-4">Check your email and confirm by link.</p>
                            <button className="buttonLarge mb-10 w-full" onClick={sendVerificationEmail}>Send new email</button>
                        </>
                    )
                }
            </section>
        </main>
    );
}

export default VerifyEmail;