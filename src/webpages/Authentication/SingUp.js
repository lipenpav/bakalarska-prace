import React, {useContext, useState} from "react";
import Nav from "../../components/Nav/Nav";
import {Formik, Form, Field} from "formik";
import {Link, Redirect} from "react-router-dom";
import firebase from '../../firebase';
import {AuthContext} from "../../auth/Auth";
import ErrorMessage from "../../components/Messages/ErrorMessage";
import {validatePassword, validateEmail, validateName, validateConfirmPassword} from "../../validation/Validation";

const SingUp = () => {
    const { currentUser } = useContext(AuthContext);
    const [fireError, setFireError] = useState(null);

    const onSingIn = (email, password, name) => {
        firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then((cred) => {
                cred.user.sendEmailVerification();
                firebase.firestore().collection('users').doc(cred.user.uid).set({
                    uid: cred.user.uid,
                    name: name,
                    email: email.toLowerCase(),
                    tel: "",
                    avatar: "",
                    birthday: "",
                    authProvider: "firebase"
                });
            })
            .catch((err) => {
                setFireError(err.message);
            });
    }

    return(
        <div>
            {!!currentUser ? (
                <Redirect to={{pathname: "/dashboard"}} />
            ) : (
                    <main className="bg-c3 w-screen min-h-screen pb-5 sm:bg-c4 sm:w-auto sm:min-h-full">
                        <Nav />
                        <section className="pt-16 mx-5 sm:bg-c3 sm:max-w-screen-sm sm:mx-auto sm:p-20 sm:rounded-2xl sm:mt-28 sm:shadow-md sm:flex sm:flex-col">
                            <h1 className="text-center">Let’s get Started!</h1>
                            <p className="smallestText text-center text-c0 m-4">Create an account to Family App to get all features</p>
                            <Formik initialValues={{name: '', email: '', password: '', confirmPassword: ''}}
                                    onSubmit={async (values) => {

                                        onSingIn(values.email, values.password, values.name);

                                    }}>

                                {({ isSubmitting, errors, touched, values }) => (
                                    <Form className="">
                                        <label htmlFor="Name" className="hidden">Name</label>
                                        <svg className="pointer-events-none absolute mt-11 ml-4" width="18" height="21" viewBox="0 0 18 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M12.6 12.4688C11.4469 12.4688 10.8924 13.125 9 13.125C7.10759 13.125 6.55714 12.4688 5.4 12.4688C2.41875 12.4688 0 14.9379 0 17.9812V19.0312C0 20.1182 0.863839 21 1.92857 21H16.0714C17.1362 21 18 20.1182 18 19.0312V17.9812C18 14.9379 15.5813 12.4688 12.6 12.4688ZM16.0714 19.0312H1.92857V17.9812C1.92857 16.0289 3.4875 14.4375 5.4 14.4375C5.98661 14.4375 6.93884 15.0938 9 15.0938C11.0772 15.0938 12.0094 14.4375 12.6 14.4375C14.5125 14.4375 16.0714 16.0289 16.0714 17.9812V19.0312ZM9 11.8125C12.1942 11.8125 14.7857 9.16699 14.7857 5.90625C14.7857 2.64551 12.1942 0 9 0C5.8058 0 3.21429 2.64551 3.21429 5.90625C3.21429 9.16699 5.8058 11.8125 9 11.8125ZM9 1.96875C11.1254 1.96875 12.8571 3.73652 12.8571 5.90625C12.8571 8.07598 11.1254 9.84375 9 9.84375C6.87455 9.84375 5.14286 8.07598 5.14286 5.90625C5.14286 3.73652 6.87455 1.96875 9 1.96875Z" fill="#7F7C82"/>
                                        </svg>
                                        <Field
                                            name="name"
                                            placeholder="User name"
                                            type="text"
                                            validate={validateName}
                                            className="defaultInput pl-12 mt-7"
                                        />
                                        {touched.Name && errors.Name &&
                                        <div className="errorMessageInput">⚠ {errors.Name}</div>
                                        }

                                        <label htmlFor="email" className="hidden">Email</label>
                                        <svg className="pointer-events-none absolute mt-4 ml-4" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M18.125 2.5H1.875C0.839453 2.5 0 3.33945 0 4.375V15.625C0 16.6605 0.839453 17.5 1.875 17.5H18.125C19.1605 17.5 20 16.6605 20 15.625V4.375C20 3.33945 19.1605 2.5 18.125 2.5ZM18.125 4.375V5.96895C17.2491 6.68219 15.8528 7.79125 12.8677 10.1287C12.2098 10.6462 10.9067 11.8893 10 11.8748C9.09344 11.8895 7.78988 10.646 7.13231 10.1287C4.14766 7.7916 2.75098 6.6823 1.875 5.96895V4.375H18.125ZM1.875 15.625V8.37492C2.77008 9.08785 4.03941 10.0883 5.97414 11.6033C6.82793 12.2754 8.32312 13.759 10 13.75C11.6686 13.759 13.1449 12.2969 14.0255 11.6036C15.9602 10.0886 17.2299 9.08793 18.125 8.37496V15.625H1.875Z" fill="#7F7C82"/>
                                        </svg>
                                        <Field
                                            name="email"
                                            placeholder="Email"
                                            type="email"
                                            validate={validateEmail}
                                            className="defaultInput pl-12"
                                        />
                                        {touched.email && errors.email &&
                                        <div className="errorMessageInput">⚠ {errors.email}</div>
                                        }

                                        <label htmlFor="password" className="hidden">Password</label>
                                        <svg className="pointer-events-none absolute mt-4 ml-4" width="17" height="20" viewBox="0 0 17 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <g clipPath="url(#clip0)">
                                                <path d="M15.1786 10H5.76786V5.9727C5.76786 4.42582 6.97076 3.14066 8.47344 3.12504C9.99129 3.10941 11.2321 4.37895 11.2321 5.93754V6.56254C11.2321 7.08207 11.6382 7.50004 12.1429 7.50004H13.3571C13.8618 7.50004 14.2679 7.08207 14.2679 6.56254V5.93754C14.2679 2.65629 11.6685 -0.0116803 8.48103 3.84578e-05C5.29353 0.0117572 2.73214 2.71488 2.73214 5.99613V10H1.82143C0.815848 10 0 10.8399 0 11.875V18.125C0 19.1602 0.815848 20 1.82143 20H15.1786C16.1842 20 17 19.1602 17 18.125V11.875C17 10.8399 16.1842 10 15.1786 10ZM10.0179 15.9375C10.0179 16.8008 9.33862 17.5 8.5 17.5C7.66138 17.5 6.98214 16.8008 6.98214 15.9375V14.0625C6.98214 13.1993 7.66138 12.5 8.5 12.5C9.33862 12.5 10.0179 13.1993 10.0179 14.0625V15.9375Z" fill="#7F7C82"/>
                                            </g>
                                            <defs>
                                                <clipPath id="clip0">
                                                    <rect width="17" height="20" fill="white"/>
                                                </clipPath>
                                            </defs>
                                        </svg>
                                        <Field type="password"
                                               name="password"
                                               placeholder="Password"
                                               validate={validatePassword}
                                               className="defaultInput pl-12"
                                        />
                                        {touched.password && errors.password &&
                                        <div className="errorMessageInput">⚠ {errors.password}</div>
                                        }

                                        <label htmlFor="confirmPassword" className="hidden">Confirm password</label>
                                        <svg className="pointer-events-none absolute mt-4 ml-4" width="17" height="20" viewBox="0 0 17 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <g  clipPath="url(#clip0)">
                                                <path d="M15.1786 10H5.76786V5.9727C5.76786 4.42582 6.97076 3.14066 8.47344 3.12504C9.99129 3.10941 11.2321 4.37895 11.2321 5.93754V6.56254C11.2321 7.08207 11.6382 7.50004 12.1429 7.50004H13.3571C13.8618 7.50004 14.2679 7.08207 14.2679 6.56254V5.93754C14.2679 2.65629 11.6685 -0.0116803 8.48103 3.84578e-05C5.29353 0.0117572 2.73214 2.71488 2.73214 5.99613V10H1.82143C0.815848 10 0 10.8399 0 11.875V18.125C0 19.1602 0.815848 20 1.82143 20H15.1786C16.1842 20 17 19.1602 17 18.125V11.875C17 10.8399 16.1842 10 15.1786 10ZM10.0179 15.9375C10.0179 16.8008 9.33862 17.5 8.5 17.5C7.66138 17.5 6.98214 16.8008 6.98214 15.9375V14.0625C6.98214 13.1993 7.66138 12.5 8.5 12.5C9.33862 12.5 10.0179 13.1993 10.0179 14.0625V15.9375Z" fill="#7F7C82"/>
                                            </g>
                                            <defs>
                                                <clipPath id="clip0">
                                                    <rect width="17" height="20" fill="white"/>
                                                </clipPath>
                                            </defs>
                                        </svg>
                                        <Field type="password"
                                               name="confirmPassword"
                                               placeholder="Confirm Password"
                                               validate={() => validateConfirmPassword(values.confirmPassword, values.password)}
                                               className="defaultInput pl-12"
                                        />
                                        {touched.confirmPassword && errors.confirmPassword &&
                                        <div className="errorMessageInput">⚠ {errors.confirmPassword}</div>
                                        }

                                        {fireError &&
                                            <ErrorMessage errorMessage={fireError} />
                                        }

                                        <button className="buttonLarge mb-10 w-full sm:w-full" type="submit" disabled={isSubmitting}>Create</button>
                                    </Form>
                                )}
                            </Formik>


                            <div className="flex justify-center align-center mt-10">
                                <span className="smallestText">Already have an account? </span>
                                <Link to={"/login"} className="smallestText ml-2 text-indigo-600">Login here</Link>
                            </div>
                        </section>
                    </main>
                )}
        </ div>
    );
}

export default SingUp;