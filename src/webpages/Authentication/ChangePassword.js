import React, {useContext, useEffect, useState} from "react";
import NavBasic from "../../components/Nav/NavBasic";
import {Field, Form, Formik} from "formik";
import firebase from '../../firebase';
import { AuthContext } from "../../auth/Auth";
import ErrorMessage from "../../components/Messages/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage";
import {getUserData} from "../../database/User/getUserData";
import WarningMessage from "../../components/Messages/WarningMessage";

const ChangePassword = () => {
    const { currentUser } = useContext(AuthContext);
    const [successMessage, setSuccessMessage] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);
    const [user, setUser] = useState({});

    useEffect(() =>{
        getUserData(currentUser.uid, setUser);
    },[currentUser]);


    function validatePassword(value) {
        let error;
        if ( value.length < 8 ) {
            error = 'Password is too short (at least 8 chars)';
        }
        return error;
    }

    function validateConfirmPassword(pass, value) {
        let error;

        if (!pass && value) {
            error = "Confirm the password."
        }else if ( (pass && value) && (pass !== value) ) {
            error = "Password not matched";
        }
        return error;
    }

    const reauthenticate = (currentPassword) => {
        let credential = firebase.auth.EmailAuthProvider.credential(
            currentUser.email,
            currentPassword
        );
        return currentUser.reauthenticateWithCredential(credential);
    }

    const onChangePass = (currentPassword, newPassword) => {
        setErrorMessage(null);
        setSuccessMessage(null);
        firebase.firestore().collection('users').doc(currentUser.uid).get().then(data => {
            if(data.data().authProvider === "firebase"){
                reauthenticate(currentPassword).then(() => {
                    firebase.auth().currentUser.updatePassword(newPassword)
                        .then(() => {
                            setSuccessMessage("Your password has been changed ");
                        })
                        .catch((err) => {
                            setErrorMessage(err.message);
                        });
                }).catch((err) => {
                    setErrorMessage(err.message);
                });
            } else {
                setErrorMessage("You cannot change your password because you are logged in with a google account.");
            }
        });


    }

    return(
        <main className="bg-white h-screen overscroll-x-none">
            <NavBasic heading={"Change password"} urlBack={"/dashboard"}/>
            <section className="pt-16 mx-5 sm:bg-c3 sm:max-w-screen-sm sm:mx-auto sm:p-20 sm:rounded-2xl sm:mt-28 sm:shadow-md sm:flex sm:flex-col">
                {user.authProvider !== "google.com" ?
                    <Formik initialValues={{oldPass: '', password: '', confirmPassword: ''}}
                            onSubmit={async (values) => {

                                onChangePass(values.oldPass, values.password);

                            }}>

                        {({ isSubmitting, errors, touched, values }) => (
                            <Form>
                                <label htmlFor="oldPass" className="text-c0 font-bold text-lg">Old password</label>
                                <svg className="pointer-events-none absolute mt-4 ml-4" width="17" height="20" viewBox="0 0 17 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clipPath="url(#clip0)">
                                        <path d="M15.1786 10H5.76786V5.9727C5.76786 4.42582 6.97076 3.14066 8.47344 3.12504C9.99129 3.10941 11.2321 4.37895 11.2321 5.93754V6.56254C11.2321 7.08207 11.6382 7.50004 12.1429 7.50004H13.3571C13.8618 7.50004 14.2679 7.08207 14.2679 6.56254V5.93754C14.2679 2.65629 11.6685 -0.0116803 8.48103 3.84578e-05C5.29353 0.0117572 2.73214 2.71488 2.73214 5.99613V10H1.82143C0.815848 10 0 10.8399 0 11.875V18.125C0 19.1602 0.815848 20 1.82143 20H15.1786C16.1842 20 17 19.1602 17 18.125V11.875C17 10.8399 16.1842 10 15.1786 10ZM10.0179 15.9375C10.0179 16.8008 9.33862 17.5 8.5 17.5C7.66138 17.5 6.98214 16.8008 6.98214 15.9375V14.0625C6.98214 13.1993 7.66138 12.5 8.5 12.5C9.33862 12.5 10.0179 13.1993 10.0179 14.0625V15.9375Z" fill="#7F7C82"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="17" height="20" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                                <Field
                                    name="oldPass"
                                    placeholder="Old password"
                                    type="password"
                                    className="defaultInput pl-12"
                                />
                                {touched.oldPass && errors.oldPass &&
                                <div className="errorMessageInput">⚠ {errors.oldPass}</div>
                                }

                                <div className="h-8" />

                                <label htmlFor="password" className="text-c0 font-bold text-lg">New password</label>
                                <svg className="pointer-events-none absolute mt-4 ml-4" width="17" height="20" viewBox="0 0 17 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clipPath="url(#clip0)">
                                        <path d="M15.1786 10H5.76786V5.9727C5.76786 4.42582 6.97076 3.14066 8.47344 3.12504C9.99129 3.10941 11.2321 4.37895 11.2321 5.93754V6.56254C11.2321 7.08207 11.6382 7.50004 12.1429 7.50004H13.3571C13.8618 7.50004 14.2679 7.08207 14.2679 6.56254V5.93754C14.2679 2.65629 11.6685 -0.0116803 8.48103 3.84578e-05C5.29353 0.0117572 2.73214 2.71488 2.73214 5.99613V10H1.82143C0.815848 10 0 10.8399 0 11.875V18.125C0 19.1602 0.815848 20 1.82143 20H15.1786C16.1842 20 17 19.1602 17 18.125V11.875C17 10.8399 16.1842 10 15.1786 10ZM10.0179 15.9375C10.0179 16.8008 9.33862 17.5 8.5 17.5C7.66138 17.5 6.98214 16.8008 6.98214 15.9375V14.0625C6.98214 13.1993 7.66138 12.5 8.5 12.5C9.33862 12.5 10.0179 13.1993 10.0179 14.0625V15.9375Z" fill="#7F7C82"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="17" height="20" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                                <Field type="password"
                                       name="password"
                                       placeholder="Password"
                                       validate={validatePassword}
                                       className="defaultInput pl-12"
                                />
                                {touched.password && errors.password &&
                                <div className="errorMessageInput">⚠ {errors.password}</div>
                                }

                                <label htmlFor="confirmPassword" className="text-c0 font-bold text-lg">Confirm password</label>
                                <svg className="pointer-events-none absolute mt-4 ml-4" width="17" height="20" viewBox="0 0 17 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g  clipPath="url(#clip0)">
                                        <path d="M15.1786 10H5.76786V5.9727C5.76786 4.42582 6.97076 3.14066 8.47344 3.12504C9.99129 3.10941 11.2321 4.37895 11.2321 5.93754V6.56254C11.2321 7.08207 11.6382 7.50004 12.1429 7.50004H13.3571C13.8618 7.50004 14.2679 7.08207 14.2679 6.56254V5.93754C14.2679 2.65629 11.6685 -0.0116803 8.48103 3.84578e-05C5.29353 0.0117572 2.73214 2.71488 2.73214 5.99613V10H1.82143C0.815848 10 0 10.8399 0 11.875V18.125C0 19.1602 0.815848 20 1.82143 20H15.1786C16.1842 20 17 19.1602 17 18.125V11.875C17 10.8399 16.1842 10 15.1786 10ZM10.0179 15.9375C10.0179 16.8008 9.33862 17.5 8.5 17.5C7.66138 17.5 6.98214 16.8008 6.98214 15.9375V14.0625C6.98214 13.1993 7.66138 12.5 8.5 12.5C9.33862 12.5 10.0179 13.1993 10.0179 14.0625V15.9375Z" fill="#7F7C82"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0">
                                            <rect width="17" height="20" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                                <Field type="password"
                                       name="confirmPassword"
                                       placeholder="Confirm Password"
                                       validate={() => validateConfirmPassword(values.confirmPassword, values.password)}
                                       className="defaultInput pl-12"
                                />
                                {touched.confirmPassword && errors.confirmPassword &&
                                <div className="errorMessageInput">⚠ {errors.confirmPassword}</div>
                                }

                                {errorMessage &&
                                <ErrorMessage errorMessage={errorMessage} />
                                }
                                {successMessage &&
                                <SuccessMessage successMessage={successMessage} />
                                }

                                <button className="buttonLarge mb-10 w-full sm:w-full" type="submit" disabled={isSubmitting}>Change password</button>
                            </Form>
                        )}
                    </Formik>
                    :
                    <div>
                        <WarningMessage warningMessage={"You cannot change your password, you are logged in through google"} />
                    </div>
                }

            </section>
        </main>
    );
}

export default ChangePassword;