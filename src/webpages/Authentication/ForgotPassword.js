import React, {useState} from "react";
import Nav from "../../components/Nav/Nav";
import {Field, Form, Formik} from "formik";
import {Link} from "react-router-dom";
import firebase from '../../firebase';
import ErrorMessage from "../../components/Messages/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage";

const ForgotPassword = () => {
    const [errMessage, setErrMessage] = useState(null);
    const [sucMessage, setSucMessage] = useState(null);

    function validateEmail(value) {
        let error;
        if (!value) {
            error = 'Email is not filled';
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
            error = 'Invalid email address';
        }
        return error;
    }

    return(
        <main className="bg-c3 w-screen min-h-screen pb-5 sm:bg-c4 sm:w-auto sm:min-h-full">
            <Nav />
            <section className="pt-16 mx-5 sm:bg-c3 sm:max-w-screen-sm sm:mx-auto sm:p-20 sm:rounded-2xl sm:mt-28 sm:shadow-md sm:flex sm:flex-col">
                <h1 className="text-center">Forgot password?</h1>
                <p className="smallestText text-center text-c0 m-4">Tell us your email and we send you instruction on email.</p>
                <Formik initialValues={{ email: ''}}
                        onSubmit={async (values) => {
                             firebase.auth().sendPasswordResetEmail(values.email)
                                .then(() => {
                                    setSucMessage(true);
                                }).catch((err) =>{
                                    setErrMessage(err);
                                });
                        }}>

                    {({ isSubmitting, errors, touched }) => (
                        <Form className="sm:flex sm:flex-col">
                            <label htmlFor="email" className="hidden">Email</label>
                            <svg className="pointer-events-none absolute mt-11 ml-4" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M18.125 2.5H1.875C0.839453 2.5 0 3.33945 0 4.375V15.625C0 16.6605 0.839453 17.5 1.875 17.5H18.125C19.1605 17.5 20 16.6605 20 15.625V4.375C20 3.33945 19.1605 2.5 18.125 2.5ZM18.125 4.375V5.96895C17.2491 6.68219 15.8528 7.79125 12.8677 10.1287C12.2098 10.6462 10.9067 11.8893 10 11.8748C9.09344 11.8895 7.78988 10.646 7.13231 10.1287C4.14766 7.7916 2.75098 6.6823 1.875 5.96895V4.375H18.125ZM1.875 15.625V8.37492C2.77008 9.08785 4.03941 10.0883 5.97414 11.6033C6.82793 12.2754 8.32312 13.759 10 13.75C11.6686 13.759 13.1449 12.2969 14.0255 11.6036C15.9602 10.0886 17.2299 9.08793 18.125 8.37496V15.625H1.875Z" fill="#7F7C82"/>
                            </svg>
                            <Field
                                name="email"
                                placeholder="Email"
                                type="email"
                                validate={validateEmail}
                                className="defaultInput pl-12 mt-7"
                            />
                            {touched.email && errors.email &&
                            <div className="errorMessageInput">⚠ {errors.email}</div>
                            }

                            {errMessage &&
                                <ErrorMessage errorMessage={errMessage} />
                            }
                            {sucMessage &&
                                <SuccessMessage successMessage={"Check your email for password reset link."} />
                            }

                            <button className="buttonLarge mb-10 w-full sm:mx-auto sm:w-full" type="submit" disabled={isSubmitting}>Send instruction</button>
                        </Form>
                    )}
                </Formik>
                <div className="flex justify-center align-center mt-10">
                    <span className="smallestText">Don’t have an account?</span>
                    <Link to={"/singup"} className="smallestText ml-2 text-indigo-600"> Sing Up</Link>
                </div>
            </section>

        </main>
    );
}

export default ForgotPassword;

