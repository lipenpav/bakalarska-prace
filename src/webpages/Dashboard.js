import React, {useContext} from "react";
import NavDashboard from "../components/Nav/NavDashboard";
import DashboardTable from "../components/Dashboad/DashboardTable";
import { AuthContext } from "../auth/Auth";
import firebase from '../firebase';
import {Link, Redirect} from "react-router-dom";
import {Notification} from "../Notification";

const Dashboard = () => {
    const { currentFamily } = useContext(AuthContext);
    return(
        <>
           {!currentFamily ? (
               <Redirect to={{pathname: "/without-family"}} />
           ) : (
                <main className="bg-c4 min-h-screen sm:py-3 xl:max-w-screen-xl xl:mx-auto">
                    <NavDashboard/>
                    {!firebase.auth().currentUser.emailVerified && (
                        <div className="p-1 px-4 py-2 text-yellow-700 text-sm bg-yellow-100 rounded-2xl mx-5 my-4 flex justify-between sm:max-w-screen-sm sm:mx-auto">
                            <p className="text-sm self-center">Your email hasn't been verified yet.</p>
                            <Link className="buttonSmall" to={"/verify-email"}>Fix it</Link>
                        </div>
                    )}
                    <DashboardTable/>
                    {/*<button onClick={() => {Notification("dAZoCDqf0Uf6E8hR73x6SW:APA91bH249FGXv8gf3GU3znKqSf8SFfuBJy2x", "Test", "description")}}>Notification</button>*/}
                </main>
                 )}
        </>


    );

}

export default Dashboard;