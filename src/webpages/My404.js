import React from "react";
import Nav from "../components/Nav/Nav";
import {Link} from "react-router-dom";

const My404 = () => {

    return(
        <main className="bg-c3 w-screen min-h-screen pb-5">
            <Nav />
            <section className="flex flex-col items-center pt-36 mx-5">
                <h1 className={"text-5xl"}>404</h1>
                <p className={"mb-10"}>This is not the page you are looking for.</p>
                <Link to={"/"} className={"buttonLarge"}>Go to homepage</Link>
            </section>
        </main>
    );

}

export default My404;