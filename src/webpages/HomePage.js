import React from "react";
import Nav from "../components/Nav/Nav";
import Footer from "../components/Footer";
import imgMobile1 from "../images/hp/hp1.png";
import imgMobile2 from "../images/hp/hp2.png";
import imgMobile3 from "../images/hp/hp3.png";
import imgMobile4 from "../images/hp/hp4.png";


const HomePage = () => {
    window.addEventListener('beforeinstallprompt', (ev) => {
        ev.preventDefault();
        HomePage.deferredInstall = ev;
        console.log('saved the install event');

    });

    const handleInstall = () => {
        if (HomePage.deferredInstall) {
            console.log(HomePage.deferredInstall);
            HomePage.deferredInstall.prompt();
            HomePage.deferredInstall.userChoice.then((choice) => {
                if (choice.outcome === 'accepted') {
                    //they installed
                    console.log('installed');
                } else {
                    console.log('cancel');
                }
            });
        }
    }

    return(
        <main className="bg-c4">
            <section className="bg-c3">
                <Nav />
                <div className="flex flex-col mx-5 items-center">
                    <h1 className="mt-28 mb-10">Family assistant</h1>
                    <p className="text-center mb-20">Your daily helper with communication, planning and organization your family.</p>
                    <button onClick={handleInstall} className="buttonLarge mb-8">Add to home screen</button>
                </div>
            </section>
            <section className="m-5 mt-8 lg:max-w-screen-lg lg:mx-auto" id={"about"}>
                <div className="lg:flex lg:items-center">
                    <div>
                        <h2 className="mb-2">Simplify your family life</h2>
                        <p className="text-base mb-2">The Family App helps organize and manage your family in daily activities such as online communication, common activities, fulfillment of common ToDo lists, financial management and location sharing. All this is in one place with the Family App.</p>
                    </div>
                    <img className="mb-10 sm:max-w-md mx-auto lg:max-w-xs lg:ml-24" src={imgMobile1} alt="shows application on mobile device"/>
                </div>
                <div className="lg:flex lg:items-center">
                    <div>
                        <h2 className="mb-2">Shared calendar</h2>
                        <p className="text-base mb-2">We can organize activities, events or birthdays of each member in the shared calendar. The calendar is shared with other family members and everyone sees all the events.</p>
                    </div>
                    <img className="mb-10 sm:max-w-md mx-auto lg:max-w-xs lg:ml-24" src={imgMobile2} alt="shows application on mobile device"/>
                </div>
                <div className="lg:flex lg:items-center">
                    <div>
                        <h2 className="mb-2">Location</h2>
                        <p className="text-base mb-2">We can share our location with family members so they know where we are. We can also see the location of all members who have already shared their coordinates on the map. </p>
                    </div>
                    <img className="mb-10 sm:max-w-md mx-auto lg:max-w-xs lg:ml-24" src={imgMobile3} alt="shows application on mobile device"/>
                </div>
                <div className="lg:flex lg:items-center">
                    <div>
                        <h2 className="mb-2">Shared finances</h2>
                        <p className="text-base mb-2">The finance card is used to record accounts between individual family members. Here we can keep track of who owes your money and to whom we have to pay.</p>
                    </div>
                    <img className="mb-10 sm:max-w-md mx-auto lg:max-w-xs lg:ml-24" src={imgMobile4} alt="shows application on mobile device"/>
                </div>
            </section>
            <Footer />
        </main>
    )
}

export default HomePage;