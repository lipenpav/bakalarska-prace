import React, {useContext, useEffect, useState} from "react";
import NavBasic from "../components/Nav/NavBasic";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";

import "react-big-calendar/lib/css/react-big-calendar.css";
import AddCircleButton from "../components/Buttons/AddCircleButton";
import NewEventModal from "../components/NewEventModal";
import {getEvents} from "../database/Calendar/getEvents";
import {AuthContext} from "../auth/Auth";

moment.locale('en-GB', {
    week: {
        dow: 1,
        doy: 1,
    },
});
const localized = momentLocalizer(moment);

let formats = {
    timeGutterFormat: 'HH:mm',
    eventTimeRangeFormat: ({
                               start,
                               end
                           }, culture, local) =>
        local.format(start, 'HH:mm', culture) + '-' +
        local.format(end, 'HH:mm', culture),
    dayFormat: 'dd DD. MM.',
    agendaTimeRangeFormat: ({
                                start,
                                end
                            }, culture, local) =>
        local.format(start, 'HH:mm', culture) + '-' +
        local.format(end, 'HH:mm', culture),
    agendaDateFormat: 'MM-DD yyyy dd',
}


const Calendar = () => {
    const [navHeight, setNavHeight] = useState(0);
    const [open, setOpen] = useState(false);
    const [update, setUpdate] = useState(false);
    const [event, setEvent] = useState();
    const [events, setEvents] = useState([]);
    const [isLoad, setIsLoad] = useState(false);
    const { currentFamily } = useContext(AuthContext);

    useEffect(() => {
        setNavHeight(document.getElementById("navRef").clientHeight);
    }, []);

    useEffect(() => {
        getEvents(currentFamily.id, setEvents, setIsLoad);
    }, [currentFamily]);


    return(
        <main>
            <NavBasic heading={"Calendar"} urlBack={"/dashboard"}/>
            <section className="p-2 bg-white overflow-y-scroll sm:py-3 xl:max-w-screen-xl xl:mx-auto" style={{height: `calc(100vh - ${navHeight}px`}}>
                {isLoad &&
                    <BigCalendar
                        className="bigCalendar"
                        localizer={localized}
                        formats={formats}
                        events={events}
                        startAccessor="start"
                        endAccessor="end"
                        eventPropGetter={event => ({
                            style: {
                                backgroundColor: event.color,
                            },
                        })}
                        views={['month', 'week', 'day']}
                        messages={{next:">",previous:"<"}}
                        onSelectEvent={event => {
                            setOpen(true);
                            setUpdate(true);
                            setEvent(event);
                        }}
                    />
                }

            </section>
            <AddCircleButton onClick={() => {
                setOpen(true)
            }} />
            {open &&
                <NewEventModal open={open} setOpen={setOpen} setUpdate={setUpdate} update={update} event={event}/>
            }
        </main>
    );
}

export default Calendar