import React, {useContext, useEffect, useState} from "react";
import NavBasic from "../components/Nav/NavBasic";
import PersonItem from "../components/User/PersonItem";
import {Link} from "react-router-dom";
import {AuthContext} from "../auth/Auth";
import {getUserData} from "../database/User/getUserData";
import {getMembersOfFamily} from "../database/Family/getMembersOfFamily";

const Members = () => {
    const [navHeight, setNavHeight] = useState(0);
    const { currentUser, currentFamily } = useContext(AuthContext);
    const [user, setUser] = useState({});
    const [members, setMembers] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        setNavHeight(document.getElementById("navRef").clientHeight);
    }, []);

    useEffect(() => {
        const fetchUser = async () => {
            getUserData(currentUser.uid, setUser);
        };

        const fetchMembers = async () => {
            await getMembersOfFamily(currentFamily, currentUser, members, setMembers);
        }

        const fetchAll = async () => {
            await fetchUser();
            await fetchMembers();
        }
        fetchAll().then(() => setIsLoading(false));

    },[currentUser]);


    return(
        <main className="bg-c4 h-screen overscroll-x-none">
            <NavBasic heading={"Family members"} urlBack={"/dashboard"}/>
            <section className="overflow-y-scroll xl:max-w-screen-xl xl:mx-auto" style={{height: `calc(100vh - ${navHeight}px`}}>
                <PersonItem user={user} isMe={true} />
                {!isLoading && members.map((member, index) => {
                    if(member.uid !== user.uid){
                        return(
                            <PersonItem key={index} user={member} isMe={false} />
                        );
                    }
                })}
                <Link to={"/members/new"} className="h-16 px-5 flex items-center bg-white my-8">
                    <div className="flex items-center">
                        <svg className="w-9 h-9 bg-c2 rounded-full mr-2 p-2" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22.2857 9.625H14.5714V1.75C14.5714 0.783672 13.8038 0 12.8571 0H11.1429C10.1962 0 9.42857 0.783672 9.42857 1.75V9.625H1.71429C0.767679 9.625 0 10.4087 0 11.375V13.125C0 14.0913 0.767679 14.875 1.71429 14.875H9.42857V22.75C9.42857 23.7163 10.1962 24.5 11.1429 24.5H12.8571C13.8038 24.5 14.5714 23.7163 14.5714 22.75V14.875H22.2857C23.2323 14.875 24 14.0913 24 13.125V11.375C24 10.4087 23.2323 9.625 22.2857 9.625Z" fill="white"/>
                        </svg>
                        <p className="text-c0 text-lg font-normal">Invite new member</p>
                    </div>
                </Link>
            </section>
        </main>
    );
}

export default Members;
