import React, {useContext, useEffect, useState} from "react";
import NavBasic from "../components/Nav/NavBasic";
import PersonItem from "../components/User/PersonItem";
import useGeoLocation from "../hooks/useGeolocation";
import ReactMapGL, {FlyToInterpolator, GeolocateControl, Marker} from "react-map-gl";
import {getUserData} from "../database/User/getUserData";
import {getMembersOfFamily} from "../database/Family/getMembersOfFamily";
import {AuthContext} from "../auth/Auth";
import {setLocation} from "../database/Location/setLocation";
import {getLocations} from "../database/Location/getLocations";

const geolocateControlStyle = {
    right: 10,
    top: 10
};

const Location = () => {
    const { currentUser, currentFamily } = useContext(AuthContext);
    const [navHeight, setNavHeight] = useState(0);
    const [buttonHeight, setButtonHeight] = useState(0);
    const location = useGeoLocation();
    const [user, setUser] = useState({});
    const [members, setMembers] = useState([]);
    const [locations, setLocations] = useState([]);
    const [memLoc, setMemLoc] = useState([]);
    const [viewport, setViewport] = useState({
        width: window.innerWidth,
        height: (window.innerHeight * 0.57),
        latitude: location.coordinates.lng,
        longitude: location.coordinates.lat,
        zoom: 8,
    });

    useEffect(() => {
        setNavHeight(document.getElementById("navRef").clientHeight);
    }, []);
    useEffect(() => {
        setButtonHeight(document.getElementById("buttonRef").clientHeight);
    }, []);

    useEffect(() => {
        const fetchUser = () => {
            getUserData(currentUser.uid, setUser);
        }
        const fetchMembers = async () => {
            await getMembersOfFamily(currentFamily, currentUser, members, setMembers);
        }
        const fetchLocations = async () => {
            await getLocations(currentFamily.id, setLocations);
        }
        fetchUser();
        fetchMembers().then(r => {});
        fetchLocations().then(r => {});
    }, [currentFamily, currentUser]);

    useEffect(() =>{
        const createMemLoc = async (allMembers, allLocations) => {
            const tmp = [];
            allMembers.forEach((member) => {
                const tmpCompare = allLocations.find((i) => i.data().user_id === member.uid);
                if(tmpCompare !== undefined && member.uid !== user.uid){
                    tmp.push({member: member, location: allLocations.find((item) => item.data().user_id === member.uid).data()});
                }
            });
            setMemLoc(Array.from(tmp));
        }

        setMemLoc([]);
        createMemLoc(members, locations).then();
    },[members, locations, user.uid]);

    function handleShareLocation(){
        setLocation(currentFamily.id, currentUser.uid, location.coordinates);
    }

    const goToLocation = (long, lat) => {
        setViewport({
            ...viewport,
            longitude: long,
            latitude: lat,
            zoom: 15,
            transitionDuration: 3000,
            transitionInterpolator: new FlyToInterpolator()
        });
    }

    return(
        <main className="bg-c4 h-screen overscroll-x-none">
            <NavBasic heading={"Location"} urlBack={"/dashboard"}/>
            <section className="overflow-y-scroll overflow-x-hidden">
                <ReactMapGL
                    mapStyle={'mapbox://styles/mapbox/streets-v11'}
                    mapboxApiAccessToken={"pk.eyJ1IjoicGFqYTEzMDkiLCJhIjoiY2t1dG1oeGx0MGx0bjJwbHRhM2NycmQ5diJ9.aTB5PXwQUqyTRkwoHnf7-w"}
                    {...viewport}
                    onViewportChange={nextViewport => setViewport(nextViewport)}
                    trackUserLocation={true}
                >
                    <GeolocateControl
                        style={geolocateControlStyle}
                        positionOptions={{enableHighAccuracy: true}}
                        trackUserLocation={true}
                        auto
                    />

                    {locations.map((itemLocation, index) => {
                        return(
                            <>
                                {itemLocation.data().user_id === user.uid &&
                                    <Marker longitude={itemLocation.data().lng}
                                            latitude={itemLocation.data().lat}
                                            offsetLeft={-19}
                                            offsetTop={-50}
                                            key={index}
                                    >
                                        <svg width="38" height="50" viewBox="0 0 38 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M17.0474 48.9912C2.66891 28.421 0 26.3099 0 18.75C0 8.39463 8.50656 0 19 0C29.4934 0 38 8.39463 38 18.75C38 26.3099 35.3311 28.421 20.9526 48.9912C20.0091 50.3363 17.9908 50.3362 17.0474 48.9912ZM19 26.5625C23.3723 26.5625 26.9167 23.0647 26.9167 18.75C26.9167 14.4353 23.3723 10.9375 19 10.9375C14.6277 10.9375 11.0833 14.4353 11.0833 18.75C11.0833 23.0647 14.6277 26.5625 19 26.5625Z" fill="#bfa2db"/>
                                        </svg>
                                    </Marker>
                                }
                            </>
                        );
                    })}
                    {memLoc.length !== 0 &&
                        memLoc.map((item, index) => {
                            return(
                                <>
                                    <Marker longitude={item.location.lng}
                                            latitude={item.location.lat}
                                            offsetLeft={-19}
                                            offsetTop={-50}
                                            key={index}
                                    >
                                        <svg width="38" height="50" viewBox="0 0 38 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M17.0474 48.9912C2.66891 28.421 0 26.3099 0 18.75C0 8.39463 8.50656 0 19 0C29.4934 0 38 8.39463 38 18.75C38 26.3099 35.3311 28.421 20.9526 48.9912C20.0091 50.3363 17.9908 50.3362 17.0474 48.9912ZM19 26.5625C23.3723 26.5625 26.9167 23.0647 26.9167 18.75C26.9167 14.4353 23.3723 10.9375 19 10.9375C14.6277 10.9375 11.0833 14.4353 11.0833 18.75C11.0833 23.0647 14.6277 26.5625 19 26.5625Z" fill="#E26868"/>
                                        </svg>
                                    </Marker>
                                </>
                            );
                        })
                    }
                </ReactMapGL>
            </section>
            <section className="overflow-y-scroll xl:max-w-screen-xl xl:mx-auto" style={{height : "calc(100vh - " + navHeight + "px - 57vh - " + buttonHeight + "px)"}}>
                {locations.map((itemLocation, index) => {
                    return(
                        <>
                            {itemLocation.data().user_id === user.uid &&
                            <PersonItem onClick={() => goToLocation(itemLocation.data().lng, itemLocation.data().lat)}
                                        key={index}
                                        user={user}
                                        isMe={true}
                                        date={new Date(itemLocation.data().timestamp.seconds * 1000)}
                            />
                            }
                        </>
                    );
                })}

                {memLoc.length !== 0 &&
                    memLoc.map((item, index) => {
                        return(
                            <>
                                <PersonItem
                                    onClick={() => goToLocation(item.location.lng, item.location.lat)}
                                    key={index}
                                    user={item.member}
                                    isMe={false}
                                    date={new Date(item.location.timestamp.seconds * 1000)}
                                />
                            </>
                        );
                    })
                }

            </section>
            <section id={"buttonRef"} className="px-5 py-2 h-15 xl:max-w-screen-xl xl:mx-auto xl:px-0">
                <button className="buttonLarge sm:w-full" onClick={handleShareLocation}>
                    Share my position
                </button>
            </section>
        </main>
    );
}

export default Location;