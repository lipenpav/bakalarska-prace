import React, {useContext, useEffect, useState} from "react";
import NavBasic from "../components/Nav/NavBasic";
import Message from "../components/Chat/Message";
import WriteMessage from "../components/Chat/WriteMessage";
import {getMessages} from "../database/Message/getMessages";
import {AuthContext} from "../auth/Auth";
import {getUserData} from "../database/User/getUserData";

function getWindowHeight() {
    const { innerHeight: height } = window;
    return { height };
}

function useWindowHeight() {
    const [windowHeight, setWindowHeight] = useState(getWindowHeight());

    useEffect(() => {
        function handleResize() {
            setWindowHeight(getWindowHeight());
        }

        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return windowHeight;
}

const Messages = () => {
    const { currentUser, currentFamily } = useContext(AuthContext);
    const [user, setUser] = useState({});
    const { height } = useWindowHeight();
    const [navHeight, setNavHeight] = useState(0);
    const [inputHeight, setInputHeight] = useState(58);
    const [messages, setMessages] = useState([]);
    const [latestDoc, setLatestDoc] = useState(null);

    useEffect(() => {
        setNavHeight(document.getElementById("navRef").clientHeight);
    }, []);

    useEffect(() => {
        function handleResize() {
            setInputHeight(document.getElementById("writeMessage").clientHeight);
        }

        document.addEventListener('input', handleResize);
        return () => document.removeEventListener('input', handleResize);
    }, []);


    useEffect(() => {
        getMessages(currentFamily.id, messages, setMessages, latestDoc);
        getUserData(currentUser.uid, setUser);
    }, [currentFamily.id, currentUser.uid, latestDoc]);


    const handleNext = () => {
        setLatestDoc(messages[messages.length - 1]);
    }

    const handleScroll = () => {
        const container = document.getElementById('scroller');
        let triggerHeight = container.offsetHeight - container.scrollTop;
        if ((container.scrollHeight - 5) <= triggerHeight) {
            handleNext();
        }
    }

    return(
        <main className="bg-c4 h-screen overscroll-x-none xl:bg-white">
            <NavBasic heading={"Messages"} urlBack={"/dashboard"} />
            <section id={"scroller"}
                     onScroll={handleScroll}
                     className="flex flex-col-reverse max-h-full px-3 overflow-y-scroll overflow-x-hidden xl:max-w-screen-xl xl:mx-auto xl:shadow-xl xl:bg-c4"
                     style={{height : (height - inputHeight - navHeight)}}
            >
                {messages.length !== 0 &&
                    messages.map((message, index) => {
                            return(
                                <Message
                                    key={index}
                                    me={message.data().user_id === currentUser.uid}
                                    picture={message.data().avatar}
                                    name={message.data().user_name}
                                    datetime={new Date(message.data().timestamp.seconds * 1000)}
                                    text={message.data().text}
                                />
                            );


                    })
                }
            </section>
            <WriteMessage family={currentFamily} user={user}/>
        </main>
    );
}

export default Messages;