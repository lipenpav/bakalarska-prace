import NavBasic from "../../components/Nav/NavBasic";
import {Field, Form, Formik} from "formik";
import {validateName} from "../../validation/Validation";
import ErrorMessage from "../../components/Messages/ErrorMessage";
import SuccessMessage from "../../components/Messages/SuccessMessage";
import React, {useContext, useState} from "react";
import {AuthContext} from "../../auth/Auth";
import {createTaskList} from "../../database/TaskList/createTaskList";
import {useHistory} from "react-router-dom";

const NewTaskList = () => {
    const { currentFamily } = useContext(AuthContext);
    const { currentUser } = useContext(AuthContext);
    const [errorMessage, setErrorMessage] = useState(null);
    const [successMessage, setSuccessMessage] = useState(null);
    const history = useHistory();

    return(
        <main className="bg-c3 h-screen overscroll-x-none sm:bg-c4 sm:w-auto sm:min-h-full">
            <NavBasic heading={"New task list"} urlBack={"/task-lists"}/>
            <section className="px-5 mt-8 sm:bg-c3 sm:max-w-screen-sm sm:mx-auto sm:p-20 sm:rounded-2xl sm:mt-28 sm:shadow-md sm:flex sm:flex-col">
                <h3 className="text-center text-cText">Type a name for your new to-do list and set it to be visible to everyone</h3>
                <Formik initialValues={{ name: '', visible: false}}
                        onSubmit={async (values, {resetForm}) => {
                            setErrorMessage(null);
                            setSuccessMessage(null);
                            createTaskList(currentFamily.id, values.name, currentUser, values.visible, setSuccessMessage, setErrorMessage);
                            resetForm();
                            history.push("/task-lists");
                        }}>

                    {({ isSubmitting, errors, touched }) => (
                        <Form>
                            <label htmlFor="name" className="hidden">Name of list</label>
                            <svg className="pointer-events-none absolute mt-10 ml-4 z-0" width="19" height="26" viewBox="0 0 19 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M16.625 3.25H12.6667C12.6667 1.45742 11.2466 0 9.5 0C7.75339 0 6.33333 1.45742 6.33333 3.25H2.375C1.0638 3.25 0 4.3418 0 5.6875V23.5625C0 24.9082 1.0638 26 2.375 26H16.625C17.9362 26 19 24.9082 19 23.5625V5.6875C19 4.3418 17.9362 3.25 16.625 3.25ZM4.75 21.5312C4.09193 21.5312 3.5625 20.9879 3.5625 20.3125C3.5625 19.6371 4.09193 19.0938 4.75 19.0938C5.40807 19.0938 5.9375 19.6371 5.9375 20.3125C5.9375 20.9879 5.40807 21.5312 4.75 21.5312ZM4.75 16.6562C4.09193 16.6562 3.5625 16.1129 3.5625 15.4375C3.5625 14.7621 4.09193 14.2188 4.75 14.2188C5.40807 14.2188 5.9375 14.7621 5.9375 15.4375C5.9375 16.1129 5.40807 16.6562 4.75 16.6562ZM4.75 11.7812C4.09193 11.7812 3.5625 11.2379 3.5625 10.5625C3.5625 9.88711 4.09193 9.34375 4.75 9.34375C5.40807 9.34375 5.9375 9.88711 5.9375 10.5625C5.9375 11.2379 5.40807 11.7812 4.75 11.7812ZM9.5 2.03125C10.1581 2.03125 10.6875 2.57461 10.6875 3.25C10.6875 3.92539 10.1581 4.46875 9.5 4.46875C8.84193 4.46875 8.3125 3.92539 8.3125 3.25C8.3125 2.57461 8.84193 2.03125 9.5 2.03125ZM15.8333 20.7188C15.8333 20.9422 15.6552 21.125 15.4375 21.125H8.3125C8.09479 21.125 7.91667 20.9422 7.91667 20.7188V19.9062C7.91667 19.6828 8.09479 19.5 8.3125 19.5H15.4375C15.6552 19.5 15.8333 19.6828 15.8333 19.9062V20.7188ZM15.8333 15.8438C15.8333 16.0672 15.6552 16.25 15.4375 16.25H8.3125C8.09479 16.25 7.91667 16.0672 7.91667 15.8438V15.0312C7.91667 14.8078 8.09479 14.625 8.3125 14.625H15.4375C15.6552 14.625 15.8333 14.8078 15.8333 15.0312V15.8438ZM15.8333 10.9688C15.8333 11.1922 15.6552 11.375 15.4375 11.375H8.3125C8.09479 11.375 7.91667 11.1922 7.91667 10.9688V10.1562C7.91667 9.93281 8.09479 9.75 8.3125 9.75H15.4375C15.6552 9.75 15.8333 9.93281 15.8333 10.1562V10.9688Z" fill="#7F7C82"/>
                            </svg>

                            <Field
                                name="name"
                                placeholder="Name of list"
                                type="text"
                                validate={validateName}
                                className="defaultInput pl-14 mt-7"
                            />
                            {touched.name && errors.name &&
                            <div className="errorMessageInput">⚠ {errors.name}</div>
                            }

                            <div className="flex flex-col -mt-5 mb-5 pb-4">
                                <label className="relative items-center mt-2 ml-1 text-cText text-sm">
                                    Visible for everyone
                                    <Field name="visible"
                                           type="checkbox"
                                           className="absolute opacity-0 cursor-pointer h-0 w-0"
                                    />
                                    <span className="checkMark absolute top-0 ml-2 h-5 w-5 bg-c2 border-c2 border-2"/>
                                </label>
                            </div>

                            {errorMessage &&
                            <ErrorMessage errorMessage={errorMessage} />
                            }

                            {successMessage &&
                            <SuccessMessage successMessage={successMessage} />
                            }

                            <button className="buttonLarge mb-4 w-full sm:w-full" type="submit" disabled={isSubmitting}>Create task list</button>
                        </Form>
                    )}
                </Formik>
            </section>
        </main>
    );
}

export default NewTaskList;