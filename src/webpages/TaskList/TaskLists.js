import React, {useContext, useEffect, useState} from "react";
import NavBasic from "../../components/Nav/NavBasic";
import TaskHead from "../../components/Task/TaskHead";
import AddCircleButton from "../../components/Buttons/AddCircleButton";
import {useHistory} from "react-router-dom";
import {getTaskListsOfUser} from "../../database/TaskList/getTaskListsOfUser";
import {AuthContext} from "../../auth/Auth";
import {getTaskListsVisible} from "../../database/TaskList/getTaskListsVisible";

const TaskLists = () => {
    const [navHeight, setNavHeight] = useState(0);
    const history = useHistory();
    const {currentUser, currentFamily} = useContext(AuthContext);
    const [privateTasks, setPrivateTasks] = useState([]);
    const [publicTasks, setPublicTasks] = useState([]);
    const [allTasks, setAllTasks] = useState([]);


    useEffect(() => {
        setNavHeight(document.getElementById("navRef").clientHeight);
    }, []);

    useEffect(() => {
        getTaskListsOfUser(currentFamily.id, currentUser, setPrivateTasks);
        getTaskListsVisible(currentFamily.id, setPublicTasks);
    }, [currentFamily, currentUser]);

    useEffect(() => {
        let tmp = [];
        tmp = [...tmp, ...publicTasks];
        tmp = [...tmp, ...privateTasks];

        const filtered = tmp.filter(function({id}) {
            const key =`${id}`;
            return !this.has(key) && this.add(key);
        }, new Set());

        setAllTasks(Array.from(filtered));
    }, [privateTasks, publicTasks]);

    const handleClick = () => {
        history.push('task-lists/new');
    }
    return(
        <main className="bg-c4 h-screen overscroll-x-none">
            <NavBasic heading={"Task lists"} urlBack={"/dashboard"}/>
            <section className="overflow-y-scroll xl:max-w-screen-xl xl:mx-auto" style={{height: `calc(100vh - ${navHeight}px`}}>
                {allTasks.length !== 0 &&
                    allTasks.map((item, index) => {
                        return(
                            <TaskHead key={index} id={item.id} name={item.data().name} numberTasks={item.data().count} shared={item.data().visible} isShoppingList={false}/>
                        );
                    })
                }
            </section>
            <AddCircleButton onClick={handleClick} />
        </main>
    );
}

export default TaskLists;