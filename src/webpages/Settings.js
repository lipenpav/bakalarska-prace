import React, {useContext, useEffect, useState} from "react";
import NavBasic from "../components/Nav/NavBasic";
import {Link, useHistory} from "react-router-dom";
import firebase from "./../firebase";
import {AuthContext} from "../auth/Auth";
import ErrorMessage from "../components/Messages/ErrorMessage";
import {deleteFamily} from "../database/Family/deleteFamily";
import {getDevicesByAllFamily} from "../database/FCM/getDevicesByAllFamily";
import {Notifications} from "../Notification";
import {getUserData} from "../database/User/getUserData";
import {leaveFamily} from "../database/Family/leaveFamily";

const Settings = () => {
    const { currentUser, currentFamily } = useContext(AuthContext);
    const [errorMessage, setErrorMessage] = useState(null);
    const history = useHistory();
    const [user, setUser] = useState({});
    const [devices, setDevices] = useState([]);

    useEffect(() => {
        getDevicesByAllFamily(currentFamily.id, setDevices);
    }, [currentFamily]);

    useEffect(() =>{
        getUserData(currentUser.uid, setUser);
    },[currentUser]);

    const handleLeaveFamily = async () => {
        if(currentFamily.owner !== currentUser.uid){
            if(window.confirm("Are you sure you want to leave the family? All your data will be deleted.")){
                await leaveFamily(currentFamily.id, currentUser.uid);
                await Notifications(devices, user.name + " left " + currentFamily.name + " family");
                await history.push("/dashboard");
            }
        }else{
            setErrorMessage("You can't leave the family, you created it.")
        }
    }

    const handleDeleteFamily = async () => {
        if(currentFamily.owner === currentUser.uid){
            if(window.confirm("Are you sure you want to delete the family? All data will be deleted.")){
                await Notifications(devices, user.name + "delete family " + currentFamily.name);
                await deleteFamily(currentFamily.id);
                await history.push("/dashboard");
            }
        }else{
            setErrorMessage("You can't delete this family, you didn't create it")
        }
    }

    return(
        <main className="bg-c4 h-screen overscroll-x-none">
            <NavBasic heading={"Settings"} urlBack={"/dashboard"}/>
            <section className="overflow-y-scroll xl:max-w-screen-xl xl:mx-auto">
                {errorMessage &&
                    <ErrorMessage errorMessage={errorMessage} />
                }

                <Link className="h-16 px-5 flex items-center bg-white mb-1" to={"/settings/family-name"}>
                    <svg className="mr-2" width="30" height="27" viewBox="0 0 30 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20.9688 4.38223L25.6667 9.13887C25.8646 9.33926 25.8646 9.66621 25.6667 9.8666L14.2917 21.3838L9.45833 21.927C8.8125 22.0008 8.26562 21.4471 8.33854 20.7932L8.875 15.8994L20.25 4.38223C20.4479 4.18184 20.7708 4.18184 20.9688 4.38223ZM29.4062 3.17461L26.8646 0.601172C26.0729 -0.200391 24.7865 -0.200391 23.9896 0.601172L22.1458 2.46797C21.9479 2.66836 21.9479 2.99531 22.1458 3.1957L26.8438 7.95234C27.0417 8.15273 27.3646 8.15273 27.5625 7.95234L29.4062 6.08555C30.1979 5.27871 30.1979 3.97617 29.4062 3.17461ZM20 18.2514V23.6197H3.33333V6.74473H15.3021C15.4687 6.74473 15.625 6.67617 15.7448 6.56016L17.8281 4.45078C18.224 4.05 17.9427 3.36973 17.3854 3.36973H2.5C1.11979 3.36973 0 4.50352 0 5.90098V24.4635C0 25.8609 1.11979 26.9947 2.5 26.9947H20.8333C22.2135 26.9947 23.3333 25.8609 23.3333 24.4635V16.142C23.3333 15.5777 22.6615 15.2982 22.2656 15.6938L20.1823 17.8031C20.0677 17.9244 20 18.0826 20 18.2514Z" fill="#7F7C82"/>
                    </svg>
                    <h3 className="text-c0">Change family name</h3>
                </Link>
                <Link className="h-16 px-5 flex items-center bg-white mb-1" to={"/edit-profile"}>
                    <svg className="mr-2" width="27" height="31" viewBox="0 0 27 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18.9 18.4062C17.1703 18.4062 16.3386 19.375 13.5 19.375C10.6614 19.375 9.83571 18.4062 8.1 18.4062C3.62812 18.4062 0 22.0512 0 26.5437V28.0938C0 29.6982 1.29576 31 2.89286 31H24.1071C25.7042 31 27 29.6982 27 28.0938V26.5437C27 22.0512 23.3719 18.4062 18.9 18.4062ZM24.1071 28.0938H2.89286V26.5437C2.89286 23.6617 5.23125 21.3125 8.1 21.3125C8.97991 21.3125 10.4083 22.2812 13.5 22.2812C16.6158 22.2812 18.0141 21.3125 18.9 21.3125C21.7688 21.3125 24.1071 23.6617 24.1071 26.5437V28.0938ZM13.5 17.4375C18.2913 17.4375 22.1786 13.5322 22.1786 8.71875C22.1786 3.90527 18.2913 0 13.5 0C8.70871 0 4.82143 3.90527 4.82143 8.71875C4.82143 13.5322 8.70871 17.4375 13.5 17.4375ZM13.5 2.90625C16.6882 2.90625 19.2857 5.51582 19.2857 8.71875C19.2857 11.9217 16.6882 14.5312 13.5 14.5312C10.3118 14.5312 7.71429 11.9217 7.71429 8.71875C7.71429 5.51582 10.3118 2.90625 13.5 2.90625Z" fill="#7F7C82"/>
                    </svg>
                    <h3 className="text-c0">Edit your profile</h3>
                </Link>
                <Link className="h-16 px-5 flex items-center bg-white mb-1" to={"/change-password"}>
                    <svg className="mr-2" width="26" height="31" viewBox="0 0 26 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M23.2143 15.5H8.82143V9.25766C8.82143 6.86001 10.6612 4.86802 12.9594 4.8438C15.2808 4.81958 17.1786 6.78735 17.1786 9.20317V10.1719C17.1786 10.9772 17.7996 11.625 18.5714 11.625H20.4286C21.2004 11.625 21.8214 10.9772 21.8214 10.1719V9.20317C21.8214 4.11724 17.846 -0.0181044 12.971 5.96094e-05C8.09598 0.0182236 4.17857 4.20806 4.17857 9.29399V15.5H2.78571C1.24777 15.5 0 16.8018 0 18.4063V28.0938C0 29.6982 1.24777 31 2.78571 31H23.2143C24.7522 31 26 29.6982 26 28.0938V18.4063C26 16.8018 24.7522 15.5 23.2143 15.5ZM15.3214 24.7031C15.3214 26.0412 14.2826 27.125 13 27.125C11.7174 27.125 10.6786 26.0412 10.6786 24.7031V21.7969C10.6786 20.4588 11.7174 19.375 13 19.375C14.2826 19.375 15.3214 20.4588 15.3214 21.7969V24.7031Z" fill="#7F7C82"/>
                    </svg>
                    <h3 className="text-c0">Change my password</h3>
                </Link>
                <div className="flex justify-between h-16 px-5 flex items-center bg-white mb-10" >
                    <div className="flex items-center">
                        <svg className="mr-2" width="28" height="31" viewBox="0 0 28 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M27.4612 21.9355C26.2537 20.6786 23.9943 18.7877 23.9943 12.5938C23.9943 7.88926 20.5893 4.12324 15.9981 3.1993V1.9375C15.9981 0.867637 15.1031 0 13.9993 0C12.8956 0 12.0006 0.867637 12.0006 1.9375V3.1993C7.40931 4.12324 4.00431 7.88926 4.00431 12.5938C4.00431 18.7877 1.74494 20.6786 0.537436 21.9355C0.162436 22.3261 -0.00381421 22.7929 -0.00068921 23.25C0.00618579 24.243 0.810561 25.1875 2.00556 25.1875H25.9931C27.1881 25.1875 27.9931 24.243 27.9993 23.25C28.0024 22.7929 27.8362 22.3254 27.4612 21.9355ZM4.21994 22.2812C5.54619 20.5878 6.99619 17.7808 7.00306 12.6289C7.00306 12.6168 6.99931 12.6059 6.99931 12.5938C6.99931 8.84832 10.1331 5.8125 13.9993 5.8125C17.8656 5.8125 20.9993 8.84832 20.9993 12.5938C20.9993 12.6059 20.9956 12.6168 20.9956 12.6289C21.0024 17.7814 22.4524 20.5884 23.7787 22.2812H4.21994ZM13.9993 31C16.2068 31 17.9974 29.2653 17.9974 27.125H10.0012C10.0012 29.2653 11.7918 31 13.9993 31Z" fill="#7F7C82"/>
                        </svg>
                        <h3 className="text-c0">Notification</h3>
                    </div>

                    <label className="mySwitch relative inline-block w-10 h-2.5 text-c1 font-light text-sm">
                        <input className="opacity-0 h-0 w-0" type={"checkbox"} defaultChecked={true}/>
                        <span className="mySlider absolute cursor-pointer top-0 left-0 right-0 bottom-0 bg-c1 bg-opacity-50 myRound"/>

                    </label>
                </div>

                <button className="flex items-center w-full h-16 bg-white px-3 mb-3" onClick={handleLeaveFamily}>
                    <svg width="36" height="31" viewBox="0 0 36 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M34.9453 16.3298L23.1328 29.4548C22.0781 30.6267 20.25 29.8063 20.25 28.1267V20.6267H10.6875C9.75234 20.6267 9 19.7907 9 18.7517V11.2517C9 10.2126 9.75234 9.37666 10.6875 9.37666H20.25V1.87666C20.25 0.204787 22.0711 -0.623338 23.1328 0.548537L34.9453 13.6735C35.5992 14.4079 35.5992 15.5954 34.9453 16.3298ZM13.5 29.0642V25.9392C13.5 25.4235 13.1203 25.0017 12.6562 25.0017H6.75C5.50547 25.0017 4.5 23.8845 4.5 22.5017V7.50166C4.5 6.11885 5.50547 5.00166 6.75 5.00166H12.6562C13.1203 5.00166 13.5 4.57979 13.5 4.06416V0.939162C13.5 0.423537 13.1203 0.00166161 12.6562 0.00166161H6.75C3.02344 0.00166161 0 3.36104 0 7.50166V22.5017C0 26.6423 3.02344 30.0017 6.75 30.0017H12.6562C13.1203 30.0017 13.5 29.5798 13.5 29.0642Z" fill="#DE7D7D"/>
                    </svg>
                    <h3 className="ml-4 text-c0">Leave {currentFamily.name}</h3>
                </button>

                <div className="h-16 px-5 flex items-center bg-white mb-10 cursor-pointer" onClick={handleDeleteFamily}>
                    <svg className="mr-2 opacity-70"  width="27" height="31" viewBox="0 0 27 31" fill="#CC1B1B" xmlns="http://www.w3.org/2000/svg">
                        <g clipPath="url(#clip0_23:2316)">
                            <path d="M1.92857 28.0936C1.92857 28.8644 2.23335 29.6036 2.77587 30.1486C3.31839 30.6936 4.0542 30.9998 4.82143 30.9998H22.1786C22.9458 30.9998 23.6816 30.6936 24.2241 30.1486C24.7666 29.6036 25.0714 28.8644 25.0714 28.0936V7.74983H1.92857V28.0936ZM18.3214 12.5936C18.3214 12.3366 18.423 12.0902 18.6039 11.9086C18.7847 11.7269 19.03 11.6248 19.2857 11.6248C19.5415 11.6248 19.7867 11.7269 19.9676 11.9086C20.1484 12.0902 20.25 12.3366 20.25 12.5936V26.1561C20.25 26.413 20.1484 26.6594 19.9676 26.8411C19.7867 27.0228 19.5415 27.1248 19.2857 27.1248C19.03 27.1248 18.7847 27.0228 18.6039 26.8411C18.423 26.6594 18.3214 26.413 18.3214 26.1561V12.5936ZM12.5357 12.5936C12.5357 12.3366 12.6373 12.0902 12.8181 11.9086C12.999 11.7269 13.2443 11.6248 13.5 11.6248C13.7557 11.6248 14.001 11.7269 14.1819 11.9086C14.3627 12.0902 14.4643 12.3366 14.4643 12.5936V26.1561C14.4643 26.413 14.3627 26.6594 14.1819 26.8411C14.001 27.0228 13.7557 27.1248 13.5 27.1248C13.2443 27.1248 12.999 27.0228 12.8181 26.8411C12.6373 26.6594 12.5357 26.413 12.5357 26.1561V12.5936ZM6.75 12.5936C6.75 12.3366 6.85159 12.0902 7.03243 11.9086C7.21327 11.7269 7.45854 11.6248 7.71429 11.6248C7.97003 11.6248 8.2153 11.7269 8.39614 11.9086C8.57698 12.0902 8.67857 12.3366 8.67857 12.5936V26.1561C8.67857 26.413 8.57698 26.6594 8.39614 26.8411C8.2153 27.0228 7.97003 27.1248 7.71429 27.1248C7.45854 27.1248 7.21327 27.0228 7.03243 26.8411C6.85159 26.6594 6.75 26.413 6.75 26.1561V12.5936ZM26.0357 1.93733H18.8036L18.2371 0.805101C18.117 0.563043 17.9322 0.359429 17.7033 0.217166C17.4744 0.0749021 17.2105 -0.000366439 16.9413 -0.00017249H10.0527C9.78411 -0.00120969 9.52069 0.0737783 9.29258 0.2162C9.06448 0.358622 8.88092 0.562716 8.76295 0.805101L8.19643 1.93733H0.964286C0.708541 1.93733 0.463271 2.03939 0.282433 2.22107C0.101594 2.40274 0 2.64915 0 2.90608L0 4.84358C0 5.10051 0.101594 5.34691 0.282433 5.52859C0.463271 5.71026 0.708541 5.81233 0.964286 5.81233H26.0357C26.2915 5.81233 26.5367 5.71026 26.7176 5.52859C26.8984 5.34691 27 5.10051 27 4.84358V2.90608C27 2.64915 26.8984 2.40274 26.7176 2.22107C26.5367 2.03939 26.2915 1.93733 26.0357 1.93733Z" fill="#CC1B1B"/>
                        </g>
                        <defs>
                            <clipPath id="clip0_23:2316">
                                <rect width="27" height="31" fill="white"/>
                            </clipPath>
                        </defs>
                    </svg>
                    <h3 className="text-c6">Delete family</h3>
                </div>
            </section>
        </main>
    );
}

export default Settings;