import React, {useContext, useEffect, useState} from "react";
import NavBasic from "../components/Nav/NavBasic";
import {Formik, Form, Field} from "formik";
import ErrorMessage from "../components/Messages/ErrorMessage";
import SuccessMessage from "../components/Messages/SuccessMessage";
import InvitationTable from "../components/Invitation/InvitationTable";
import {validateEmail} from "../validation/Validation";
import {createInvitation} from "../database/Invitation/createInvitation";
import {AuthContext} from "../auth/Auth";
import {getUserData} from "../database/User/getUserData";


const InviteNewPerson = () => {
    const { currentFamily } = useContext(AuthContext);
    const { currentUser } = useContext(AuthContext);
    const [errorMessage, setErrorMessage] = useState(null);
    const [successMessage, setSuccessMessage] = useState(null);
    const [user, setUser] = useState({});

    useEffect(() =>{
        getUserData(currentUser.uid, setUser);
    },[currentUser]);

    return(
        <main className="bg-c3 w-screen min-h-screen pb-5 sm:bg-c4 sm:w-auto sm:min-h-full">
            <NavBasic heading={"Invite new person"} urlBack={"/members"}/>
            <section className="pt-16 mx-5 sm:bg-c3 sm:max-w-screen-sm sm:mx-auto sm:p-20 sm:rounded-2xl sm:mt-28 sm:shadow-md sm:flex sm:flex-col">
                <h3 className="text-center">Write the email of a registered user and send him an invitation to your family.</h3>
                <Formik initialValues={{ email: ''}}
                        onSubmit={async (values, {resetForm}) => {
                            setErrorMessage(null);
                            setSuccessMessage(null);
                            createInvitation(values.email, currentUser, user, currentFamily, setSuccessMessage, setErrorMessage);
                            resetForm();
                        }}>

                    {({ isSubmitting, errors, touched }) => (
                        <Form>
                            <label htmlFor="familyName" className="hidden">Family name</label>
                            <svg className="pointer-events-none absolute mt-11 ml-4 z-0" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M18.125 2.5H1.875C0.839453 2.5 0 3.33945 0 4.375V15.625C0 16.6605 0.839453 17.5 1.875 17.5H18.125C19.1605 17.5 20 16.6605 20 15.625V4.375C20 3.33945 19.1605 2.5 18.125 2.5ZM18.125 4.375V5.96895C17.2491 6.68219 15.8528 7.79125 12.8677 10.1287C12.2098 10.6462 10.9067 11.8893 10 11.8748C9.09344 11.8895 7.78988 10.646 7.13231 10.1287C4.14766 7.7916 2.75098 6.6823 1.875 5.96895V4.375H18.125ZM1.875 15.625V8.37492C2.77008 9.08785 4.03941 10.0883 5.97414 11.6033C6.82793 12.2754 8.32312 13.759 10 13.75C11.6686 13.759 13.1449 12.2969 14.0255 11.6036C15.9602 10.0886 17.2299 9.08793 18.125 8.37496V15.625H1.875Z" fill="#7F7C82"/>
                            </svg>
                            <Field
                                name="email"
                                placeholder="Email"
                                type="email"
                                validate={validateEmail}
                                className="defaultInput pl-14 mt-7"
                            />
                            {touched.email && errors.email &&
                            <div className="errorMessageInput">⚠ {errors.email}</div>
                            }

                            {errorMessage &&
                            <ErrorMessage errorMessage={errorMessage} />
                            }

                            {successMessage &&
                            <SuccessMessage successMessage={successMessage} />
                            }

                            <button className="buttonLarge mb-4 w-full sm:w-full" type="submit" disabled={isSubmitting}>Send invitation</button>
                        </Form>
                    )}
                </Formik>

                <InvitationTable/>
            </section>
        </main>
    );
}

export default InviteNewPerson;