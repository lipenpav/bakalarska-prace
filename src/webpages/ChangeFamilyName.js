import React, {useContext, useState} from "react";
import NavBasic from "../components/Nav/NavBasic";
import {Field, Form, Formik} from "formik";
import {AuthContext} from "../auth/Auth";
import WarningMessage from "../components/Messages/WarningMessage";
import {updateFamilyName} from "../database/Family/updateFamilyName";
import ErrorMessage from "../components/Messages/ErrorMessage";
import SuccessMessage from "../components/Messages/SuccessMessage";
import {validateFamilyName} from "../validation/Validation";


const ChangeFamilyName = () => {
    const { currentUser } = useContext(AuthContext);
    const { currentFamily } = useContext(AuthContext);
    const { setCurrentFamily } = useContext(AuthContext);
    const [errorMessage, setErrorMessage] = useState(null);
    const [successMessage, setSuccessMessage] = useState(null);

    return(
        <main className="bg-white h-screen overscroll-x-none sm:bg-c4 sm:w-auto sm:min-h-full">
            <NavBasic heading={"Change family name"} urlBack={"/settings"}/>
            <section className="px-3 my-6 sm:max-w-screen-sm sm:bg-c3 sm:mx-auto sm:p-20 sm:rounded-2xl sm:mt-28 sm:shadow-md sm:flex sm:flex-col">
                {currentFamily.owner === currentUser.uid ?
                    <Formik initialValues={{familyName: ''}}
                            onSubmit={async (values, {resetForm}) => {
                                setSuccessMessage(null);
                                setErrorMessage(null);
                                updateFamilyName(values.familyName, currentFamily, setCurrentFamily, setSuccessMessage, setErrorMessage);
                                resetForm();
                            }}>

                        {({ isSubmitting, errors, touched }) => (
                            <Form>
                                <label htmlFor="familyName" className="text-c0 font-bold text-lg">Family name</label>
                                <svg className="pointer-events-none absolute mt-4 ml-4" width="26" height="21" viewBox="0 0 26 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M3.9 9.1875C5.33406 9.1875 6.5 8.01035 6.5 6.5625C6.5 5.11465 5.33406 3.9375 3.9 3.9375C2.46594 3.9375 1.3 5.11465 1.3 6.5625C1.3 8.01035 2.46594 9.1875 3.9 9.1875ZM22.1 9.1875C23.5341 9.1875 24.7 8.01035 24.7 6.5625C24.7 5.11465 23.5341 3.9375 22.1 3.9375C20.6659 3.9375 19.5 5.11465 19.5 6.5625C19.5 8.01035 20.6659 9.1875 22.1 9.1875ZM23.4 10.5H20.8C20.085 10.5 19.4391 10.7912 18.9678 11.2629C20.605 12.1693 21.7669 13.8059 22.0187 15.75H24.7C25.4191 15.75 26 15.1635 26 14.4375V13.125C26 11.6771 24.8341 10.5 23.4 10.5ZM13 10.5C15.5147 10.5 17.55 8.44512 17.55 5.90625C17.55 3.36738 15.5147 1.3125 13 1.3125C10.4853 1.3125 8.45 3.36738 8.45 5.90625C8.45 8.44512 10.4853 10.5 13 10.5ZM16.12 11.8125H15.7828C14.9378 12.2227 13.9994 12.4688 13 12.4688C12.0006 12.4688 11.0662 12.2227 10.2172 11.8125H9.88C7.29625 11.8125 5.2 13.9289 5.2 16.5375V17.7188C5.2 18.8057 6.07344 19.6875 7.15 19.6875H18.85C19.9266 19.6875 20.8 18.8057 20.8 17.7188V16.5375C20.8 13.9289 18.7037 11.8125 16.12 11.8125ZM7.03219 11.2629C6.56094 10.7912 5.915 10.5 5.2 10.5H2.6C1.16594 10.5 0 11.6771 0 13.125V14.4375C0 15.1635 0.580937 15.75 1.3 15.75H3.97719C4.23312 13.8059 5.395 12.1693 7.03219 11.2629Z" fill="#7F7C82"/>
                                </svg>
                                <Field
                                    name="familyName"
                                    placeholder="Family name"
                                    type="text"
                                    validate={validateFamilyName}
                                    className="defaultInput pl-12"
                                />
                                {touched.familyName && errors.familyName &&
                                <div className="errorMessageInput">⚠ {errors.familyName}</div>
                                }

                                {errorMessage &&
                                <ErrorMessage errorMessage={errorMessage} />
                                }

                                {successMessage &&
                                <SuccessMessage successMessage={successMessage} />
                                }
                                <button className="buttonLarge mb-10 w-full sm:w-full" type="submit" disabled={isSubmitting}>Change family name</button>
                            </Form>
                        )}
                    </Formik>
                    :
                    <div>
                        <WarningMessage warningMessage={"You cannot change family name, you are not owner of this family"} />
                    </div>
                }

            </section>
        </main>
    );
}

export default ChangeFamilyName;