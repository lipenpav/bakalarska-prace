import React, {useEffect, useState} from "react";
import NavFinance from "../components/Nav/NavFinance";
import InfoBlock from "../components/User/InfoBlock";
import email from "../images/InfoIcons/email.svg"
import phone from "../images/InfoIcons/phone.svg"
import cake from "../images/InfoIcons/cake.svg"
import {useParams} from "react-router-dom";
import {getUserData} from "../database/User/getUserData";

const PersonDetail = () => {
    const {id} = useParams();
    const [user, setUser] = useState({});

    useEffect(() => {
        getUserData(id, setUser);
    }, [id]);

    return(
        <main>
            <NavFinance user={user} urlBack={"/members"} />
            <section className="px-10 py-4 xl:max-w-screen-xl xl:mx-auto">
                {/*<InfoBlock icon={person} type={"Login"} data={user.name} />*/}
                <InfoBlock icon={email} type={"Email"} data={user.email} />
                <InfoBlock icon={phone} type={"Phone"} data={user.tel} />
                <InfoBlock icon={cake} type={"Birthday"} data={user.birthday} />
            </section>
        </main>
    );
}

export default PersonDetail;