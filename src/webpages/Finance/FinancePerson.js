import React, {useContext, useEffect, useState} from "react";
import FinanceSummary from "../../components/Finance/FinanceSummary";
import FinanceActivity from "../../components/Finance/FinanceActivity";
import NavFinance from "../../components/Nav/NavFinance";
import {useParams} from "react-router-dom";
import {getUserData} from "../../database/User/getUserData";
import {getUserActivities} from "../../database/Finances/getUserActivities";
import {AuthContext} from "../../auth/Auth";


const FinancePerson = () => {
    const {id} = useParams();
    const { currentFamily, currentUser } = useContext(AuthContext);
    const [user, setUser] = useState({});
    const [allActivities, setAllActivities] = useState([]);
    const [account, setAccount] = useState(0);

    useEffect(() => {
        getUserData(id, setUser);
        getUserActivities(currentFamily.id, id, setAllActivities);
    }, [id]);

    useEffect(() => {
        let tmp = 0;
        allActivities.forEach((item) => {
            if(item.data().user === currentUser.uid && item.data().lentToUser === id){
                tmp += item.data().price;
            }
            if(item.data().user === id && item.data().lentToUser === currentUser.uid){
                tmp -= item.data().price;
            }
        });
        setAccount(tmp);
    }, [allActivities]);
    let indicator = true;
    return(
        <main className="bg-c4 min-h-screen overscroll-x-none">
            <NavFinance user={user} urlBack={"/finance"} urlPlus={"/finance/" + id + "/new"} textTop={"Finances with"}/>
            <section className="xl:max-w-screen-xl xl:mx-auto">
                {account === 0 ?
                    <FinanceSummary finance={account} user={user} currentUser={currentUser} />
                    :
                    account > 0 ?
                        <FinanceSummary isOwesMe={true} finance={account} user={user} currentUser={currentUser}/>
                        :
                        <FinanceSummary isOwesMe={false} finance={Math.abs(account)} user={user} currentUser={currentUser}/>
                }

                <h3 className="mx-5 my-3">Activity: </h3>

                {allActivities.length !== 0 ?
                    allActivities.map((item, index) => {
                        if(item.data().user === currentUser.uid && item.data().lentToUser === id){
                            indicator = false;
                            return(
                                <FinanceActivity
                                    idActivity={item.id}
                                    key={index}
                                    name={item.data().description}
                                    finance={item.data().price}
                                    isOwesMe={true}
                                    datetime={new Date(item.data().datetime)}
                                    settleUp={item.data().settleUp}
                                />
                            );
                        }else if(item.data().user === id && item.data().lentToUser === currentUser.uid) {
                            indicator = false;
                            return(
                                <FinanceActivity
                                    idActivity={item.id}
                                    key={index}
                                    name={item.data().description}
                                    finance={item.data().price}
                                    isOwesMe={false}
                                    datetime={new Date(item.data().datetime)}
                                    settleUp={item.data().settleUp}
                                />
                            );
                        }

                    })
                    :
                    indicator = true
                }
                {indicator &&
                    <div className="px-5 text-c0">No activity yet</div>
                }
            </section>
        </main>
    );
}

export default FinancePerson;
