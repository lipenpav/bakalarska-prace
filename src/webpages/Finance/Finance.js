import React, {useContext, useEffect, useState} from "react";
import NavBasic from "../../components/Nav/NavBasic";
import FinancesItem from "../../components/Finance/FinancesItem";
import {getSettleUpActivities} from "../../database/Finances/getSettleUpActivities";
import {AuthContext} from "../../auth/Auth";
import {getMembersOfFamily} from "../../database/Family/getMembersOfFamily";

const Finance = () => {
    const { currentUser, currentFamily } = useContext(AuthContext);
    const [allSettleUpActivity, setAllSettleUpActivity] = useState([]);
    const [allMembers, setAllMembers] = useState([]);
    const [youLent, setYouLent] = useState([]);
    const [youOwe, setYouOwe] = useState([]);
    const [others, setOthers] = useState([]);

    useEffect(() => {
        getSettleUpActivities(currentFamily.id, setAllSettleUpActivity);
        getMembersOfFamily(currentFamily, currentUser, allMembers, setAllMembers);
    }, [currentFamily]);

    useEffect(() => {
        setOthers([]);
        setYouLent([]);
        setYouOwe([]);
        let tmpOthers = [];
        let tmpYouLent = [];
        let tmpYouOwe = [];
        allMembers.forEach((member) => {
            let account = 0.0;
            if(currentUser.uid !== member.uid){
                allSettleUpActivity.forEach((item) => {
                    if(item.data().user === currentUser.uid && member.uid === item.data().lentToUser){
                        account += item.data().price;
                    }
                    if(item.data().user === member.uid && currentUser.uid === item.data().lentToUser){
                        account -= item.data().price;
                    }
                });
                if(account === 0){
                    member.price = 0;
                    tmpOthers.push(member);
                }else if(account > 0){
                    member.price = account;
                    tmpYouLent.push(member);
                }else if(account < 0){
                    member.price = account;
                    tmpYouOwe.push(member);
                }
            }
        });
        setOthers(others => [...others, ...tmpOthers]);
        setYouLent(youLent => [...youLent, ...tmpYouLent]);
        setYouOwe(youOwe => [...youOwe, ...tmpYouOwe]);
    }, [allMembers, allSettleUpActivity]);



    return(
        <main className="bg-c4 h-screen overscroll-x-none">
            <NavBasic heading={"Finances"} urlBack={"/dashboard"}/>
            <section className="xl:max-w-screen-xl xl:mx-auto">
                <div className="mt-4">
                    <h3 className="px-5 text-c0">You lent:</h3>
                    {youLent.length !== 0 ?
                        youLent.map((member, index) => {
                            return(
                                <FinancesItem key={index}
                                              url={"/finance/" + member.uid}
                                              picture={member.avatar}
                                              name={member.name}
                                              finance={member.price}
                                              isOwesMe={true} />
                            );
                        })
                        :
                        <div className="px-5 text-c0">no one</div>
                    }
                </div>
                <div className="mt-4">
                    <h3 className="px-5 text-c0">You owe:</h3>
                    {youOwe.length !== 0 ?
                        youOwe.map((member, index) => {
                            return(
                                <FinancesItem key={index}
                                              url={"/finance/" + member.uid}
                                              picture={member.avatar}
                                              name={member.name}
                                              finance={Math.abs(member.price)}
                                              isOwesMe={false} />
                            );
                        })
                        :
                        <div className="px-5 text-c0">no one</div>
                    }
                </div>
                <div className="mt-4">
                    <h3 className="px-5 text-c0">Others:</h3>
                    {others.length !== 0 ?
                        others.map((member, index) => {
                            return(
                                <FinancesItem key={index}
                                              url={"/finance/" + member.uid}
                                              picture={member.avatar}
                                              name={member.name}
                                              finance={member.price}/>
                            );
                        })
                        :
                        <div className="px-5 text-c0">no one</div>
                    }
                </div>
            </section>
        </main>
    );
}

export default Finance;
