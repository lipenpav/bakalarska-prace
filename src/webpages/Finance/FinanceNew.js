import React, {useContext, useEffect, useState} from "react";
import NavFinance from "../../components/Nav/NavFinance";
import {Field, Form, Formik} from "formik";
import {validateDate, validateDescription, validateMoney} from "../../validation/Validation";
import {useHistory, useParams} from "react-router-dom";
import {AuthContext} from "../../auth/Auth";
import {getUserData} from "../../database/User/getUserData";
import {createFinanceActivity} from "../../database/Finances/createFinanceActivity";


const FinanceNew = () => {
    const {id} = useParams();
    const history = useHistory();
    const { currentUser, currentFamily } = useContext(AuthContext);
    const [user, setUser] = useState({});

    useEffect(() => {
        getUserData(id, setUser);
    }, [id]);

    return(
        <main className="bg-white min-h-screen">
            <NavFinance user={user} urlBack={"/finance/" + id} textTop={"Finances with"} />
            <section className="px-5 mt-7 xl:max-w-screen-xl xl:mx-auto">
                <Formik initialValues={{ description: '', date: '', money: '', lentOwe: ''}}
                        onSubmit={async (values) => {
                            if(values.lentOwe === 'youLent'){
                                createFinanceActivity(currentFamily.id, values.description, currentUser.uid, user.uid, values.money, values.date);
                            }else if(values.lentOwe === 'youOwe'){
                                createFinanceActivity(currentFamily.id, values.description, user.uid, currentUser.uid, values.money, values.date);
                            }
                            history.push('/finance/' + id);
                        }}>

                    {({ isSubmitting, errors, touched }) => (
                        <Form id="formikEdit">
                            <label htmlFor="description" className="hidden">Description</label>
                            <Field
                                name="description"
                                placeholder="Description"
                                type="text"
                                validate={validateDescription}
                                className="defaultInput px-4"
                            />
                            {touched.description && errors.description &&
                                <div className="errorMessageInput">⚠ {errors.description}</div>
                            }

                            <label htmlFor="date" className="hidden">Date</label>
                            <Field type="datetime-local"
                                   name="date"
                                   validate={validateDate}
                                   className="defaultInput px-4"
                            />
                            {touched.date && errors.date &&
                                <div className="errorMessageInput">⚠ {errors.date}</div>
                            }

                            <label htmlFor="money" className="hidden">Money</label>
                            <svg className="absolute mt-4 ml-4" width="12" height="18" viewBox="0 0 11 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7.79324 7.29375L4.22661 6.30625C3.8138 6.19375 3.52649 5.82812 3.52649 5.42188C3.52649 4.9125 3.96241 4.5 4.50071 4.5H6.69023C7.09312 4.5 7.48942 4.61562 7.81966 4.82812C8.02111 4.95625 8.29191 4.925 8.46364 4.76562L9.61289 3.70312C9.84736 3.4875 9.81433 3.12812 9.55344 2.9375C8.74434 2.3375 7.7338 2.00312 6.69683 2V0.5C6.69683 0.225 6.45906 0 6.16844 0H5.11166C4.82105 0 4.58327 0.225 4.58327 0.5V2H4.50071C2.39706 2 0.706207 3.70938 0.901051 5.7375C1.03975 7.17813 2.20221 8.35 3.6685 8.75625L7.0535 9.69375C7.4663 9.80938 7.75361 10.1719 7.75361 10.5781C7.75361 11.0875 7.31769 11.5 6.77939 11.5H4.58988C4.18698 11.5 3.79069 11.3844 3.46044 11.1719C3.25899 11.0437 2.98819 11.075 2.81647 11.2344L1.66722 12.2969C1.43274 12.5125 1.46577 12.8719 1.72666 13.0625C2.53576 13.6625 3.54631 13.9969 4.58327 14V15.5C4.58327 15.775 4.82105 16 5.11166 16H6.16844C6.45906 16 6.69683 15.775 6.69683 15.5V13.9937C8.23577 13.9656 9.67894 13.1 10.1875 11.7219C10.8975 9.79688 9.70535 7.82187 7.79324 7.29375Z" fill="#7F7C82"/>
                            </svg>
                            <Field type="number"
                                   name="money"
                                   placeholder="0.00"
                                   validate={validateMoney}
                                   className="defaultInput px-4 text-right"
                            />
                            {touched.money && errors.money &&
                                <div className="errorMessageInput">⚠ {errors.money}</div>
                            }
                            <div className="flex flex-col mb-6">
                                <label htmlFor="youLent" className="radioContainer text-cPositive text-lg font-bold">You lent {user.name}
                                    <Field type="radio"
                                           name="lentOwe"
                                           id="youLent"
                                           value="youLent"
                                           className=""/>
                                    <span className="radioCheck ml-3"/>
                                </label>
                                <label htmlFor="youOwe" className="radioContainer text-cNegative text-lg font-bold">You owe {user.name}
                                    <Field type="radio"
                                           name="lentOwe"
                                           id="youOwe"
                                           value="youOwe"
                                           className=""/>
                                    <span className="radioCheck ml-3"/>
                                </label>
                            </div>
                            <button className="buttonLarge mb-10 w-full" type="submit" disabled={isSubmitting}>Add new record</button>
                        </Form>
                    )}
                </Formik>
            </section>
        </main>
    );

}

export default FinanceNew;
