import React, {useContext, useEffect, useState} from "react";
import firebase from "../firebase.js";

export const AuthContext = React.createContext(undefined);

export const AuthProvider = ({ children }) => {
    const [currentUser, setCurrentUser] = useState(null);
    const [currentFamily, setCurrentFamily] = useState(null);

    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            setCurrentUser(user);
            if(user){
                firebase.firestore().collection('users').doc(user.uid).onSnapshot(snapshot => {
                    if(snapshot.data().current_family !== ""){
                        firebase.firestore().collection('families').doc(snapshot.data().current_family).get().then(doc => {
                            setCurrentFamily(doc.data());
                        })
                    }else{
                        setCurrentFamily(null);
                    }
                });
            }
        });
    }, []);

    return (
        <AuthContext.Provider
            value={{
                currentUser,
                currentFamily,
                setCurrentFamily,
            }}
        >
            {children}
        </AuthContext.Provider>
    );
};

export const useAuthState = () => {
    const auth = useContext(AuthContext)
    return { ...auth, isAuthenticated: auth.currentUser != null }
}
