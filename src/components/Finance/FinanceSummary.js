import React, {useContext, useEffect, useState} from "react";
import {settleUpFinances} from "../../database/Finances/settleUpFinances";
import {AuthContext} from "../../auth/Auth";
import moment from "moment";
import {getDevicesByUser} from "../../database/FCM/getDevicesByUser";
import {Notifications} from "../../Notification";

const FinanceSummary = ({isOwesMe, finance, user, currentUser}) => {
    const { currentFamily } = useContext(AuthContext);
    const [devices, setDevices] = useState([]);


    useEffect(() => {
        if(user.uid !== undefined){
            getDevicesByUser(user.uid, setDevices);
        }
    }, [user]);

    function handleSettleUp(){
        if(isOwesMe !== undefined){
            if(isOwesMe){
                settleUpFinances(currentFamily.id, user, currentUser, finance, moment().format("YYYY-MM-DDTHH:mm"));
            }else{
                settleUpFinances(currentFamily.id, currentUser, user, finance, moment().format("YYYY-MM-DDTHH:mm"));
            }
            Notifications(devices, "You and " + user.name + " settle up now");
        }
    }

    return(
        <div className="px-5">
            <div className="flex justify-between items-center mt-6">
                {isOwesMe === undefined ?
                    <h2 className="text-lg">{user.name} and I are settled</h2>
                    :
                    isOwesMe
                ? <h2 className="text-lg sm:text-xl xl:text-2xl">{user.name} owes you</h2>
                : <h2 className="text-lg sm:text-xl xl:text-2xl">You owes {user.name}</h2>}
                <p className={`font-medium text-lg sm:text-xl lg:text-3xl ${isOwesMe === undefined ? "text-black" : isOwesMe === true ? "text-cPositive" : "text-cNegative"}`}>${finance}</p>
            </div>
            <button className={"buttonSmall block ml-auto mt-2"} onClick={handleSettleUp}>
                Settle up
            </button>

        </div>
    );
}

export default FinanceSummary;