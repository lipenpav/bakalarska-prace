import React from "react";
import {Link} from "react-router-dom";
import Avatar from "../User/Avatar";

const FinancesItem = ({url, picture, name, finance, isOwesMe}) => {
    return(
        <Link to={url} className="h-16 px-5 flex justify-between items-center bg-white mb-1">
            <div className="flex items-center">
                <Avatar user={{name: name, avatar: picture}} small={true} />
                <p className="text-black text-lg font-normal ml-3">{name}</p>
            </div>
            <p className={`font-medium text-lg sm:text-xl xl:text-2xl ${isOwesMe === true ? "text-cPositive" : (isOwesMe === undefined) ? "text-c0" : "text-cNegative"}`}>${finance}</p>
        </Link>
    );
}

export default FinancesItem;