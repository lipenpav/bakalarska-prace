import React from "react";
const Avatar = ({user, small, extraSmall}) => {

    return(
        <>
            {user ?
                !user.avatar
                    ?
                    <h2 className={`flex justify-center items-center 
                    ${small ? extraSmall ? "w-8 h-8 text-xl self-end mr-2 mb-4" : "w-10 h-10 text-2xl" : "w-24 h-24 text-5xl"} 
                    text-white bg-c5 rounded-full`}>
                        {String(user.name).charAt(0)}
                    </h2>
                    :
                    <img  className={`${small ? extraSmall ? "w-8 h-8 text-xl self-end mr-2 mb-4" : "w-10 h-10" : "w-24 h-24 "} rounded-full object-cover`} src={user.avatar} alt={"Picture of " + user.name}/>
                :
                <>
                    <h2 className={`flex justify-center items-center ${small ? extraSmall ? "w-8 h-8 text-xl self-end mr-2 mb-4" : "w-10 h-10 text-2xl" : "w-24 h-24 text-5xl"} text-white bg-c5 rounded-full`}>
                        --
                    </h2>
                </>
            }
        </>
    );
}

export default Avatar;