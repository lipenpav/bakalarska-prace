import React from "react";

const InfoBlock = ({icon, type, data}) => {
    return(
        <div className="grid grid-cols-5 grid-rows-2 mb-4 sm:grid-cols-8">
            <img className="row-span-2 self-center" src={icon} alt={"Icon of" + data}/>
            <h3 className="col-span-4 text-c0 sm:col-span-7">{type}</h3>
            {type === "Email"
            ? <a href={"mailto:" + data}>{data}</a>
            : type === "Phone"
                ? <a href={"tel:" + data}>{data}</a>
                    : <p className="col-span-4">{data}</p>

            }
        </div>
    );
}

export default InfoBlock;