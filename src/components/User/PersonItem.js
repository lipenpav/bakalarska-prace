import React from "react";
import {Link} from "react-router-dom";
import LastSeen from "../LastSeen";
import Avatar from "./Avatar";

const PersonItem = ({user, isMe, date, onClick}) => {
    return(
        <>
            {user &&
            <Link to={date ? "#" : "/members/" + user.uid} onClick={onClick} className="h-16 px-5 flex justify-between items-center bg-white my-1">
                <div className="flex items-center">
                    <Avatar user={{name: user.name, avatar: user.avatar}} small={true} />
                    <p className="text-black text-lg font-normal ml-3">{user.name}</p>
                    {isMe &&
                    <p className={`font-medium text-lg text-c0 ml-2`}>(me)</p>
                    }
                </div>
                {date &&
                <LastSeen date={date} />
                }
            </Link>
            }
        </>
    );
}

export default PersonItem;