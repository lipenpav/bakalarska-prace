import React from "react";
import {Link} from "react-router-dom";

const ButtonLarge = ({text, linkUrl}) => {

    return(
        <Link to={linkUrl} className="buttonLarge mb-10 w-full">
            {text}
        </Link>
    );
}

export default ButtonLarge;