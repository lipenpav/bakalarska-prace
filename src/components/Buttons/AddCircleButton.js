import React from "react";

const AddCircleButton = ({onClick}) =>{
    return(
        <button onClick={onClick} className="fixed w-16 h-16 bg-c2 rounded-full bottom-5 right-5 z-10 xl:right-10 xl:bottom-10">
            <svg className="flex m-auto" width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M22.2857 9.625H14.5714V1.75C14.5714 0.783672 13.8038 0 12.8571 0H11.1429C10.1962 0 9.42857 0.783672 9.42857 1.75V9.625H1.71429C0.767679 9.625 0 10.4087 0 11.375V13.125C0 14.0913 0.767679 14.875 1.71429 14.875H9.42857V22.75C9.42857 23.7163 10.1962 24.5 11.1429 24.5H12.8571C13.8038 24.5 14.5714 23.7163 14.5714 22.75V14.875H22.2857C23.2323 14.875 24 14.0913 24 13.125V11.375C24 10.4087 23.2323 9.625 22.2857 9.625Z" fill="white"/>
            </svg>
        </button>
    );
}

export default AddCircleButton;