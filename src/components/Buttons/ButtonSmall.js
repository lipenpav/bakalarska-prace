import React from "react";
import {Link} from "react-router-dom";

const ButtonSmall = ({linkUrl, text}) => {
    return(
        <Link to={linkUrl} className="buttonSmall">
            {text}
        </Link>
    );
}

export default ButtonSmall;