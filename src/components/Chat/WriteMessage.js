import React, {useState} from "react";
import InputEmoji from "react-input-emoji";
import {createMessage} from "../../database/Message/createMessage";


const WriteMessage = ({family, user}) => {
    const [ text, setText ] = useState('')

    function handleOnButton(){
        if(text.trim() !== "" && text.trim() !== "\n"){
            createMessage(family.id, user, text);
            setText("");
        }
    }

    return(
        <section id={"writeMessage"} className="fixed bottom-0 w-full bg-white p-2 xl:max-w-screen-xl xl:mx-auto xl:inset-x-0 xl:shadow-xl">
            <div className="flex">
                <InputEmoji
                    value={text}
                    onChange={setText}
                    cleanOnEnter
                    onEnter={handleOnButton}
                    placeholder="Type a message"
                    fontFamily={"Roboto"}
                    borderColor={"#ffffff"}
                    disableRecent
                />

                <button className="bg-c2 py-2.5 px-4 w-max rounded-lg shadow-md hover:shadow-xlg text-white font-light text-sm text-center justify-self-center self-center"
                       onClick={handleOnButton}>
                    Send
                </button>
            </div>

        </section>

    );
}

export default WriteMessage;