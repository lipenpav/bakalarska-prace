import React from "react";
import Avatar from "../User/Avatar";

const Message = ({me, picture, name, datetime, text, ref}) => {
    return(
        <div ref={ref} className={`flex my-2 ${me ? "justify-end" : ""}`}>
            {!me &&
                <Avatar user={{avatar: picture, name: name}} small={true} extraSmall={true} />
            }

            <div className="flex flex-col w-4/6">
                <p className={`text-sm leading-3.5 p-2 rounded-2xl md:text-base md:p-2.5 ${me ? "bg-c2 text-white": "bg-c1 text-black bg-opacity-30"}`}>{text}</p>
                <div className={`flex flex-row mt-1.5 ${me ? "justify-end" : "justify-between"}`}>
                    {!me &&
                        <p className="messageInfo pl-3">{name}</p>
                    }

                    <p className={`messageInfo pr-3`}>
                        {datetime.toLocaleDateString() + " " + datetime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}
                    </p>
                </div>
            </div>

        </div>
    );
}

export default Message;