import React, {useContext} from "react";
import {updateChecked} from "../../database/TaskList/updateChecked";
import {AuthContext} from "../../auth/Auth";
import {updateCheckedShopping} from "../../database/ShoppingList/updateCheckedShopping";

const TaskItem = ({name, isChecked, isPrice, price, idTasks, id}) => {
    const {currentFamily} = useContext(AuthContext);

    const handleClick = () => {
        if(isPrice){
            updateCheckedShopping(currentFamily.id, idTasks, id, !isChecked)
        }else{
            updateChecked(currentFamily.id, idTasks, id, !isChecked);
        }

    }
    return(
        <label className="h-9 px-5 flex justify-between items-center bg-white mb-1">
            <div className="relative items-center text-c0 font-light text-sm">
                <input className="absolute opacity-0 cursor-pointer h-0 w-0"
                       type={"checkbox"}
                       checked={isChecked}
                       onChange={handleClick}
                />
                <span className="checkMark secondCheckMark absolute top-0 h-5 w-5 bg-c2 rounded"/>
                <span className={`lineThrough ml-8 font-medium`}>{name}</span>
            </div>
            {isPrice &&
            <div className="text-c0 text-lg font-medium w-11">
                {(price && price > 0)
                    ? <span>${price}</span>
                    : <span>$--</span>
                }
            </div>
            }
        </label>
    );
}

export default TaskItem;