import React from "react";
import {Link} from "react-router-dom";

const TaskHead = ({id, name, shared, numberTasks, isShoppingList }) => {
    return (
        <Link to={isShoppingList ? "/shopping-lists/" + id : "/task-lists/" + id} className="flex justify-between mt-1 px-5 bg-white h-16">
            <div className="flex flex-col self-center">
                <p className="text-black text-lg">{name}</p>
                <div className="text-c0">
                    {shared
                    ?   <div className="flex mb-1">
                            <svg width="13" height="12" viewBox="0 0 13 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clipPath="url(#clip0)">
                                    <path d="M9.55816 -0.000961117C7.66233 0.00607013 6.13889 1.62795 6.13889 3.59669V5.24904H1.08333C0.485243 5.24904 0 5.75294 0 6.37404V10.874C0 11.4951 0.485243 11.999 1.08333 11.999H9.02778C9.62587 11.999 10.1111 11.4951 10.1111 10.874V6.37404C10.1111 5.75294 9.62587 5.24904 9.02778 5.24904H7.94444V3.58263C7.94444 2.65451 8.6599 1.88341 9.55365 1.87404C10.4564 1.86466 11.1944 2.62638 11.1944 3.56154V5.43654C11.1944 5.74826 11.4359 5.99904 11.7361 5.99904H12.4583C12.7585 5.99904 13 5.74826 13 5.43654V3.56154C13 1.59279 11.454 -0.00799237 9.55816 -0.000961117Z" fill="#7F7C82"/>
                                </g>
                                <defs>
                                    <clipPath id="clip0">
                                        <rect width="13" height="12" fill="white"/>
                                    </clipPath>
                                </defs>
                            </svg>
                            <p className="text-xs ml-1.5">Shared</p>
                        </div>
                    :   <div className="flex mb-1">
                            <svg width="10" height="12" viewBox="0 0 10 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M8.92857 5.25H8.39286V3.5625C8.39286 1.59844 6.87054 0 5 0C3.12946 0 1.60714 1.59844 1.60714 3.5625V5.25H1.07143C0.479911 5.25 0 5.75391 0 6.375V10.875C0 11.4961 0.479911 12 1.07143 12H8.92857C9.52009 12 10 11.4961 10 10.875V6.375C10 5.75391 9.52009 5.25 8.92857 5.25ZM6.60714 5.25H3.39286V3.5625C3.39286 2.63203 4.11384 1.875 5 1.875C5.88616 1.875 6.60714 2.63203 6.60714 3.5625V5.25Z" fill="#7F7C82"/>
                            </svg>
                            <p className="text-xs ml-1.5">Private</p>
                        </div>
                    }
                </div>
            </div>
            {isShoppingList
                ?
                <div className="flex items-center">
                    <p className="relative w-7 -top-1 -right-11 text-center text-white font-light text-xl z-10">{numberTasks}</p>
                    <svg className="col-span-2" width="49" height="43" viewBox="0 0 49 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M44.9269 25.3061L48.9484 7.83734C49.2387 6.57606 48.2676 5.375 46.9575 5.375H13.5437L12.764 1.61166C12.5697 0.673639 11.7336 0 10.7638 0H2.04167C0.914071 0 0 0.902412 0 2.01562V3.35938C0 4.47259 0.914071 5.375 2.04167 5.375H7.98658L13.9625 34.2182C12.5329 35.0299 11.5694 36.5518 11.5694 38.2969C11.5694 40.8943 13.7023 43 16.3333 43C18.9644 43 21.0972 40.8943 21.0972 38.2969C21.0972 36.9805 20.5488 35.7912 19.666 34.9375H37.5006C36.6179 35.7912 36.0694 36.9805 36.0694 38.2969C36.0694 40.8943 38.2023 43 40.8333 43C43.4644 43 45.5972 40.8943 45.5972 38.2969C45.5972 36.4348 44.5008 34.8256 42.9108 34.0636L43.3801 32.0248C43.6705 30.7636 42.6994 29.5625 41.3893 29.5625H18.5551L17.9983 26.875H42.936C43.8893 26.875 44.7156 26.2238 44.9269 25.3061Z" fill="#C6C0CB"/>
                    </svg>
                </div>
                :
                <div className="flex justify-center text-center self-center bg-c1 rounded-full w-11 h-11">
                    <p className="text-white font-light self-center text-2xl">{numberTasks}</p>
                </div>
            }
        </Link>
    );
}
export default TaskHead;