import React, {useContext} from "react";
import {Field, Form, Formik} from "formik";
import {validateTask} from "../../validation/Validation";
import {createNewTask} from "../../database/TaskList/createNewTask";
import {AuthContext} from "../../auth/Auth";
import {createNewShoppingTask} from "../../database/ShoppingList/createNewShoppingTask";

const AddTask = ({isPrice, taskId}) => {
    const {currentFamily} = useContext(AuthContext);
    return(
        <section id="inputNewTask" className="bg-white xl:max-w-screen-xl xl:mx-auto xl:shadow-xl">
            <Formik initialValues={{ task: '', price: ''}}
                    onSubmit={async (values, {resetForm}) => {
                        if(isPrice){
                            createNewShoppingTask(currentFamily.id, taskId, values.task, values.price);
                        }else{
                            createNewTask(currentFamily.id, taskId, values.task);
                        }
                        resetForm();
                    }}>

                {({ isSubmitting }) => (
                    <Form className="flex mx-2 py-1.5">
                        <label htmlFor="task" className="hidden">Task</label>
                        <Field
                            name="task"
                            placeholder="New task"
                            type="text"
                            validate={validateTask}
                            className={`w-full text-lg py-2 ${isPrice ? "rounded-l-2xl mr-0.5" : "rounded-2xl mr-2"} bg-c4 text-c0 font-medium focus:outline-none focus:ring-2 focus:ring-c2 focus:border-transparent px-3`}
                        />
                        {isPrice &&
                            <>
                                <label htmlFor="price" className="hidden">Price</label>
                                <Field
                                    name="price"
                                    placeholder="Price"
                                    type="number"
                                    className="w-2/6 text-lg py-2 rounded-r-2xl bg-c4 text-c0 font-medium focus:outline-none focus:ring-2 focus:ring-c2 focus:border-transparent px-3 mr-2"
                                />
                            </>
                        }
                        <button className="bg-c2 p-3 rounded-full" type="submit" disabled={isSubmitting}>
                            <svg width="20" height="20" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M22.2857 9.625H14.5714V1.75C14.5714 0.783672 13.8038 0 12.8571 0H11.1429C10.1962 0 9.42857 0.783672 9.42857 1.75V9.625H1.71429C0.767679 9.625 0 10.4087 0 11.375V13.125C0 14.0913 0.767679 14.875 1.71429 14.875H9.42857V22.75C9.42857 23.7163 10.1962 24.5 11.1429 24.5H12.8571C13.8038 24.5 14.5714 23.7163 14.5714 22.75V14.875H22.2857C23.2323 14.875 24 14.0913 24 13.125V11.375C24 10.4087 23.2323 9.625 22.2857 9.625Z" fill="white"/>
                            </svg>

                        </button>
                    </Form>
                )}
            </Formik>

        </section>
    );
}

export default AddTask;