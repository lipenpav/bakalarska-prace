import React, {useContext} from "react";
import { Fragment, useRef } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import {Field, Form, Formik} from "formik";
import {validateDates, validateTitle} from "../validation/Validation";
import {createEvent} from "../database/Calendar/createEvent";
import {AuthContext} from "../auth/Auth";
import moment from "moment";
import ErrorMessage from "./Messages/ErrorMessage";
import {updateEvent} from "../database/Calendar/updateEvent";
import {deleteEvent} from "../database/Calendar/deleteEvent";

const NewEventModal = ({open, setOpen, setUpdate, update, event}) => {
    const today = moment();
    const todayPlusHour = moment().add( 1,'h');

    const cancelButtonRef = useRef(null);
    const { currentUser, currentFamily } = useContext(AuthContext);
    const newEventData = {
        title: '',
        startDate: today.format("YYYY-MM-DD"),
        endDate: todayPlusHour.format("YYYY-MM-DD"),
        startTime: today.format("HH:mm"),
        endTime: todayPlusHour.format("HH:mm"),
        allDay: false,
        color: '#BFA2DB'
    }
    let updateEventData;
    if(update){
        updateEventData = {
            title: event.title,
            startDate: moment(event.start).format("YYYY-MM-DD"),
            endDate: moment(event.end).format("YYYY-MM-DD"),
            startTime: moment(event.start).format("HH:mm"),
            endTime: moment(event.end).format("HH:mm"),
            allDay: event.allDay,
            color: event.color
        }
    }

    const handleDelete = () => {
        if(currentUser.uid === event.owner){
            deleteEvent(currentFamily.id, event.id);
            setOpen(false);
            setUpdate(false);
        }
    }

    return(
        <Transition.Root show={open} as={Fragment}>
            <Dialog as="div" className="fixed z-10 inset-0 overflow-y-auto" initialFocus={cancelButtonRef} onClose={setOpen}>
                <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                    </Transition.Child>

                    {/* This element is to trick the browser into centering the modal contents. */}
                    <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
            &#8203;
          </span>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        enterTo="opacity-100 translate-y-0 sm:scale-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                        leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    >
                        <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                            <div className="bg-white px-4 pt-4 pb-4 sm:p-6 sm:pb-4">
                                <div className="sm:items-start">
                                    <div className="sm:mt-0 sm:text-left flex flex-col">
                                        {update && currentUser.uid === event.owner &&
                                            <button
                                                onClick={handleDelete}
                                                className="mb-4 text-red-600 font-medium self-end flex items-center">
                                                Delete
                                                <svg className="w-3 ml-1 mb-0.5" aria-hidden="true" focusable="false" data-prefix="fas"
                                                     data-icon="trash"
                                                     role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                                    <path fill="currentColor" d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z"/>
                                                </svg>
                                            </button>
                                        }
                                        <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900 self-center">
                                            {update ? "Update event: " + event.title : "Create new event"}
                                        </Dialog.Title>
                                        <div className="mt-2">
                                            <Formik initialValues={update ? updateEventData : newEventData}
                                                    onSubmit={async (values) => {
                                                        setOpen(false);
                                                        setUpdate(false);
                                                        if(update){
                                                            console.log(event);
                                                            updateEvent(
                                                                currentFamily.id,
                                                                event.id,
                                                                values.startDate + " " + values.startTime,
                                                                values.endDate + " " + values.endTime,
                                                                values.allDay,
                                                                values.title,
                                                                values.color,
                                                                currentUser.uid,
                                                            );
                                                        }else{
                                                            createEvent(
                                                                currentFamily.id,
                                                                values.startDate + " " + values.startTime,
                                                                values.endDate + " " + values.endTime,
                                                                values.allDay,
                                                                values.title,
                                                                values.color,
                                                                currentUser.uid,
                                                            );
                                                        }

                                                    }}>

                                                {({ isSubmitting, errors, touched, values }) => (
                                                    <Form>
                                                        <label htmlFor="title" className="hidden">Title</label>
                                                        <Field
                                                            name="title"
                                                            placeholder="Title"
                                                            type="text"
                                                            validate={validateTitle}
                                                            className="defaultInput pl-3 mt-3"
                                                        />
                                                        {touched.title && errors.title &&
                                                        <div className="errorMessageInput">⚠ {errors.title}</div>
                                                        }

                                                        <div className="grid grid-cols-5 gap-3">
                                                            <label htmlFor="startDate" className="hidden">Start date</label>
                                                            <Field
                                                                name="startDate"
                                                                type="date"
                                                                className="defaultInput px-3 col-span-3"
                                                            />
                                                            {touched.startDate && errors.startDate &&
                                                            <div className="errorMessageInput">⚠ {errors.startDate}</div>
                                                            }

                                                            <label htmlFor="startTime" className="hidden">Start time</label>
                                                            <Field
                                                                name="startTime"
                                                                type="time"
                                                                className="defaultInput px-3 col-span-2"
                                                            />
                                                            {touched.startTime && errors.startTime &&
                                                            <div className="errorMessageInput">⚠ {errors.startTime}</div>
                                                            }
                                                        </div>
                                                        <div className="grid grid-cols-5 gap-3">
                                                            <label htmlFor="endDate" className="hidden">End date</label>
                                                            <Field
                                                                name="endDate"
                                                                type="date"
                                                                className="defaultInput px-3 col-span-3"
                                                            />
                                                            {touched.endDate && errors.endDate &&
                                                            <div className="errorMessageInput">⚠ {errors.endDate}</div>
                                                            }

                                                            <label htmlFor="endTime" className="hidden">End time</label>
                                                            <Field
                                                                name="endTime"
                                                                type="time"
                                                                className="defaultInput px-3 col-span-2"
                                                                validate={() => validateDates(
                                                                    values.startDate,
                                                                    values.startTime,
                                                                    values.endDate,
                                                                    values.endTime)}
                                                            />
                                                            {touched.endTime && errors.endTime &&
                                                                <div className="col-span-6">
                                                                    <ErrorMessage className="col-span-6" errorMessage={errors.endTime} />
                                                                </div>
                                                            }

                                                        </div>
                                                        <div className="grid grid-cols-2 gap-3">
                                                            <label htmlFor="allDay" className="flex items-center mb-7">
                                                                <span className="w-16 text-cText text-lg font-normal">All day: </span>
                                                                <Field
                                                                    name="allDay"
                                                                    type="checkbox"
                                                                    className="px-3 col-span-2 ml-4"
                                                                />
                                                            </label>

                                                            <label htmlFor="color" className="flex items-center mb-7">
                                                                <span className="w-16 text-cText text-lg font-normal">Color: </span>
                                                                <Field
                                                                    name="color"
                                                                    type="color"
                                                                    className="px-3 col-span-2 ml-3 h-8 w-14 rounded-lg"
                                                                />
                                                            </label>
                                                        </div>

                                                        <button
                                                            type="submit"
                                                            className="buttonLarge w-full text-base focus:outline-none sm:w-full"
                                                            disabled={isSubmitting}
                                                        >
                                                            {update ? "Update event" : "Create event"}
                                                        </button>
                                                        <button
                                                            type="button"
                                                            className="bg-white py-3 mt-3 w-full rounded-2xl shadow-lg hover:shadow-xl text-cText font-medium text-lg text-center mt-2 focus:outline-none sm:text-sm"
                                                            onClick={() => {
                                                                setOpen(false);
                                                                setUpdate(false);
                                                            }}
                                                            ref={cancelButtonRef}
                                                        >
                                                            Cancel
                                                        </button>
                                                    </Form>
                                                )}
                                            </Formik>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition.Root>
    );

}

export default NewEventModal;