import React from "react";

const ErrorMessage = ({errorMessage}) => {

    return(
        <div className="p-1 px-4 text-red-500 text-sm bg-red-200 rounded-2xl my-3">
            ✖ {errorMessage}
        </div>
    );
}

export default ErrorMessage;