import React from "react";

const SuccessMessage = ({successMessage}) => {

    return(
        <div className="p-1 px-4 py-2 text-green-500 text-sm bg-green-100 rounded-2xl my-3">
            ✓ {successMessage}
        </div>
    );
}

export default SuccessMessage;