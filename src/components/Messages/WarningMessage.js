import React from "react";

const WarningMessage = ({warningMessage}) => {

    return(
        <div className="p-1 px-4 py-2 text-yellow-700 text-sm bg-yellow-100 rounded-2xl my-3">
            ⚠ {warningMessage}
        </div>
    );
}

export default WarningMessage;