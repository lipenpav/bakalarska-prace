import React from "react";
import {Link, NavLink} from "react-router-dom";
import Avatar from "../User/Avatar";


const NavFinance = ({user, textTop, urlBack, urlPlus}) => {

    return(
        <nav className="bg-c2 w-full shadow z-10">
            <div className="flex justify-between px-5 py-1.5 xl:max-w-screen-xl xl:mx-auto">
                <NavLink className="relative z-10 h-minContent" to={urlBack}>
                    <svg className="my-1" width="22" height="23" viewBox="0 0 27 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clipPath="url(#clip0)">
                            <path d="M15.522 24.6158L14.184 25.8298C13.6175 26.3439 12.7014 26.3439 12.141 25.8298L0.424888 15.204C-0.141629 14.69 -0.141629 13.8587 0.424888 13.3501L12.141 2.71889C12.7075 2.20483 13.6235 2.20483 14.184 2.71889L15.522 3.93295C16.0945 4.45248 16.0825 5.30014 15.4979 5.80873L8.2356 12.0869H25.5566C26.3581 12.0869 27.003 12.672 27.003 13.3994V15.1494C27.003 15.8767 26.3581 16.4619 25.5566 16.4619H8.2356L15.4979 22.74C16.0885 23.2486 16.1006 24.0962 15.522 24.6158Z" fill="white"/>
                        </g>
                        <defs>
                            <clipPath id="clip0">
                                <rect width="27" height="28" fill="white"/>
                            </clipPath>
                        </defs>
                    </svg>
                </NavLink>

                <div className="flex flex-col mt-0 mb-2 items-center">
                    {textTop
                        ? <h2 className="text-white mb-2 font-light mt-0.5">{textTop}</h2>
                        : <div className="h-6" />
                    }
                    <Avatar user={user} small={false} />
                    <h1 className="text-white self-center">{user.name}</h1>
                </div>
                {urlPlus
                    ?   <Link className="self-start my-1" to={urlPlus}>
                        <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22.2857 9.625H14.5714V1.75C14.5714 0.783672 13.8038 0 12.8571 0H11.1429C10.1962 0 9.42857 0.783672 9.42857 1.75V9.625H1.71429C0.767679 9.625 0 10.4087 0 11.375V13.125C0 14.0913 0.767679 14.875 1.71429 14.875H9.42857V22.75C9.42857 23.7163 10.1962 24.5 11.1429 24.5H12.8571C13.8038 24.5 14.5714 23.7163 14.5714 22.75V14.875H22.2857C23.2323 14.875 24 14.0913 24 13.125V11.375C24 10.4087 23.2323 9.625 22.2857 9.625Z" fill="white"/>
                        </svg>
                    </Link>
                    : <div className="w-7" />
                }
            </div>

        </nav>
    );

}

export default NavFinance;