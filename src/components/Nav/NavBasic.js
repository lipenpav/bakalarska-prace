import React from "react";
import {NavLink} from "react-router-dom";

const NavBasic = ({heading, urlBack}) => {
    return(
        <nav id={"navRef"} className=" bg-c2 w-full shadow z-10">
            <div className="px-5 py-1.5 h-12 flex justify-between items-center xl:max-w-screen-xl xl:mx-auto xl:px-0 xl:h-14">
                <NavLink className="relative z-10" to={urlBack}>
                    <svg className="my-1" width="22" height="23" viewBox="0 0 27 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clipPath="url(#clip0)">
                            <path d="M15.522 24.6158L14.184 25.8298C13.6175 26.3439 12.7014 26.3439 12.141 25.8298L0.424888 15.204C-0.141629 14.69 -0.141629 13.8587 0.424888 13.3501L12.141 2.71889C12.7075 2.20483 13.6235 2.20483 14.184 2.71889L15.522 3.93295C16.0945 4.45248 16.0825 5.30014 15.4979 5.80873L8.2356 12.0869H25.5566C26.3581 12.0869 27.003 12.672 27.003 13.3994V15.1494C27.003 15.8767 26.3581 16.4619 25.5566 16.4619H8.2356L15.4979 22.74C16.0885 23.2486 16.1006 24.0962 15.522 24.6158Z" fill="white"/>
                        </g>
                        <defs>
                            <clipPath id="clip0">
                                <rect width="27" height="28" fill="white"/>
                            </clipPath>
                        </defs>
                    </svg>

                </NavLink>

                <h2 className="text-white self-center">{heading}</h2>

                <div className="w-7"/>
            </div>
        </nav>
    );
}

export default NavBasic;