import React from "react";


const Footer = () => {


    return(
        <footer className="bg-c2">

            <p className="text-xs text-gray-50 font-light text-center py-5">© {new Date().getFullYear()} Family organizer App - Pavel Lipenský</p>
        </footer>
    );

}

export default Footer;