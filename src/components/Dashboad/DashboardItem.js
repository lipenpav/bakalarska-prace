import React, {useState} from "react";
import {Link} from "react-router-dom";

const DashboardItem = ({linkUrl, heading, description, icon}) => {
    const [nameIcon, setNameIcon] = useState('');


    const fetchIcons = async function() {
        let importedIcon = await import('../../images/dashboardIcons/' + icon  + '.svg');
        return importedIcon.default;
    }
    fetchIcons().then(r => setNameIcon(r));



    return(
        <Link className="bg-white rounded-lg p-4 shadow-md grid grid-cols-3 grid-rows-2 h-28 sm:p-6 sm:h-40 xl:p-8" to={linkUrl}>
            <div className="col-span-2">
                <p className="text-base text-cText font-medium sm:text-xl">{heading}</p>
                <p className="text-xs text-cText font-normal sm:text-base">{description}</p>
            </div>
            <img className="justify-self-end self-end col-start-3 row-span-full sm:w-12" src={nameIcon} alt={"Icon of " + nameIcon}/>
        </Link>
    );
}

export default DashboardItem;