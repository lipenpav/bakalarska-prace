import React, {useContext, useEffect, useState} from "react";
import DashboardItem from "./DashboardItem";
import {AuthContext} from "../../auth/Auth";
import {getCountOfMembers} from "../../database/Family/getCountOfMembers";
import {getTaskListsOfUser} from "../../database/TaskList/getTaskListsOfUser";
import {getTaskListsVisible} from "../../database/TaskList/getTaskListsVisible";
import {getShoppingListsOfUser} from "../../database/ShoppingList/getShoppingListsOfUser";
import {getShoppingListsVisible} from "../../database/ShoppingList/getShoppingListsVisible";
import {countEventThisWeek} from "../../database/Calendar/countEventThisWeek";

const DashboardTable = () => {
    const { currentFamily, currentUser } = useContext(AuthContext);
    const [countMembers, setCountMembers] = useState(0);
    const [privateTasks, setPrivateTasks] = useState([]);
    const [publicTasks, setPublicTasks] = useState([]);
    const [allTasks, setAllTasks] = useState([]);
    const [privateShopping, setPrivateShopping] = useState([]);
    const [publicShopping, setPublicShopping] = useState([]);
    const [allShopping, setAllShopping] = useState([]);
    const [countEvents, setCountEvents] = useState();

    useEffect(() => {
        getCountOfMembers(currentFamily.id, setCountMembers);
        getTaskListsOfUser(currentFamily.id, currentUser, setPrivateTasks);
        getTaskListsVisible(currentFamily.id, setPublicTasks);
        getShoppingListsOfUser(currentFamily.id, currentUser, setPrivateShopping);
        getShoppingListsVisible(currentFamily.id, setPublicShopping)
    },[currentFamily, currentUser]);

    useEffect(() => {
        let tmp = [];
        tmp = [...tmp, ...publicTasks];
        tmp = [...tmp, ...privateTasks];
        const filtered = tmp.filter(function({id}) {
            const key =`${id}`;
            return !this.has(key) && this.add(key);
        }, new Set());

        setAllTasks(Array.from(filtered));
    }, [privateTasks, publicTasks])

    useEffect(() => {
        let tmp = [];
        tmp = [...tmp, ...publicShopping];
        tmp = [...tmp, ...privateShopping];
        const filtered = tmp.filter(function({id}) {
            const key =`${id}`;
            return !this.has(key) && this.add(key);
        }, new Set());

        setAllShopping(Array.from(filtered));
    }, [privateShopping, publicShopping]);

    useEffect(() => {
        countEventThisWeek(currentFamily.id, setCountEvents)
    },[currentFamily]);

    return(
        <section className="grid grid-cols-2 gap-5 mx-5 my-6 sm:grid-cols-3 xl:grid-cols-4">
            <DashboardItem linkUrl={"/calendar"} heading={"Calendar"} description={countEvents ? countEvents + " events this week" : "0 events this week"} icon={"calendar"} />
            <DashboardItem linkUrl={"/messenger"} heading={"Messages"} description={""} icon={"comment"} />
            <DashboardItem linkUrl={"/task-lists"} heading={"Task lists"} description={allTasks.length + " lists"} icon={"clipboard"} />
            <DashboardItem linkUrl={"/shopping-lists"} heading={"Shopping"} description={allShopping.length + " list"} icon={"shopping"} />
            <DashboardItem linkUrl={"/finance"} heading={"Finance"} description={""} icon={"money"} />
            <DashboardItem linkUrl={"/location"} heading={"Location"} description={""} icon={"map"} />
            <DashboardItem linkUrl={"/members"} heading={"My family"} description={countMembers + " members"} icon={"family"} />
            <DashboardItem linkUrl={"/settings"} heading={"Settings"} description={""} icon={"settings"} />
        </section>
    );
}

export default DashboardTable;