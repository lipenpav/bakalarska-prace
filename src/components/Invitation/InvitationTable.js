import React, {useContext, useEffect, useState} from "react";
import {AuthContext} from "../../auth/Auth";
import {getInvitations} from "../../database/Invitation/getInvitations";
import Invitation from "./Invitation";
import ErrorMessage from "../Messages/ErrorMessage";
import SuccessMessage from "../Messages/SuccessMessage";

const InvitationTable = () => {
    const { currentUser } = useContext(AuthContext);
    const [invitations, setInvitations] = useState([]);
    const [errorMessage, setErrorMessage] = useState(null);
    const [successMessage, setSuccessMessage] = useState(null);

    useEffect(() => {
        getInvitations(currentUser,setInvitations);
        
        return() => {
            setInvitations([]);
        }
    }, [currentUser]);


    return(
        <section>
            <h3 className="text-center mt-4">Your invitation</h3>
            {errorMessage &&
            <ErrorMessage errorMessage={errorMessage} />
            }

            {successMessage &&
            <SuccessMessage successMessage={successMessage} />
            }
            <div className="grid grid-cols-5 mt-2">
                <p className="text-c0 text-sm font-medium col-span-2">Person</p>
                <p className="text-c0 text-sm font-medium col-span-3">Family</p>
                <div className="bg-c1 h-px w-full col-span-5 mb-4 mt-1"/>
                <div className="flex flex-col col-span-5">
                    {invitations.length === 0 ?
                        <p>You have no invitations</p>
                        :
                        <>
                        {invitations.map((i, index) => {
                            return (
                                <Invitation key={index}
                                            name={i.data().creator_user_name}
                                            family={i.data().family_name}
                                            familyId={i.data().family_id}
                                            avatar={i.data().creator_user_avatar}
                                            idInvitation={i.id}
                                            setSuccessMessage={setSuccessMessage}
                                            setErrorMessage={setErrorMessage}
                                />
                            );
                        })}
                        </>
                    }

                </div>
            </div>
        </section>
    );
}

export default InvitationTable;