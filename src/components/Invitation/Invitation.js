import React, {useContext, useEffect, useState} from "react";
import Avatar from "../User/Avatar";
import {acceptInvitation} from "../../database/Invitation/acceptInvitation";
import {useHistory} from "react-router-dom";
import {getDevicesByAllFamily} from "../../database/FCM/getDevicesByAllFamily";
import {AuthContext} from "../../auth/Auth";
import {Notifications} from "../../Notification";
import {getUserData} from "../../database/User/getUserData";


const Invitation = ({name, family, familyId, avatar, idInvitation, setSuccessMessage, setErrorMessage}) => {
    const history = useHistory();
    const { currentFamily, currentUser } = useContext(AuthContext);
    const [devices, setDevices] = useState([]);
    const [user, setUser] = useState({});


    useEffect(() =>{
        getUserData(currentUser.uid, setUser);
        return() => {setUser({})}
    },[currentUser.uid]);

    useEffect(() => {
        getDevicesByAllFamily(familyId, setDevices);
    }, [currentFamily]);

    function handleAccept(){
        acceptInvitation(idInvitation, setSuccessMessage, setErrorMessage);
        Notifications(devices, user.name + " is a new member of the " + family + " family");
        history.push("/dashboard");
    }
    return(
        <div className="grid grid-cols-5 mb-4">
            <div className="flex items-center col-span-2">
                <Avatar user={{name: name, avatar: avatar}} small={true} />
                <p className="text-xs ml-1.5">{name}</p>
            </div>

            <p className="text-xs flex items-center col-span-2">{family}</p>
            <button className={"buttonSmall"} onClick={handleAccept}>Accept</button>
        </div>
    );

}

export default Invitation;