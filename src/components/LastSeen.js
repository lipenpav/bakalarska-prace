import React from 'react'
import ReactTimeAgo from 'react-time-ago'

const LastSeen = ({date}) => {

    return (
        <ReactTimeAgo className="text-c0 text-sm" date={date} locale="cs" timeStyle="round"/>
    );
}
export default LastSeen;