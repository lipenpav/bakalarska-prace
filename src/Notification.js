

export const Notification = (token, title, description) => {
    fetch('https://fcm.googleapis.com/fcm/send', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'key=' + process.env.REACT_APP_SERVER_KEY,
        },
        body: JSON.stringify({
            to : "dAZoCDqf0Uf6E8hR73x6SW:APA91bH249FGXv8gf3GU3znKqSf8SFfuBJy2x-2gZXVsj24CSoqB65isinA0voORk5Pc7XThKxd1oKGMZRHxurtXZ581g-eLp-1oa4wWocJpd1nDqwL1-06xz_-lJHbzodcPovid3vqE",
            notification: {
                title: title,
                body: description,
                mutable_content: true,
            },
            data: {
                url: "https://firebasestorage.googleapis.com/v0/b/family-app-329209.appspot.com/o/profilePhoto%2FIMG_20211029_152144.jpg?alt=media&token=f3693f11-5f95-4a4a-abec-85bdd3dfb4f8",
            }
        })
    }).then(r => {console.log(r)});
}

export const Notifications = (users, title, description) => {
    console.log(users);
    users.forEach(item => {
        Notification(item.token, title, description);
    })
}