import React, {useState} from "react";
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import './style/style.css';
import 'mapbox-gl/dist/mapbox-gl.css';
import 'react-toastify/dist/ReactToastify.min.css'

import HomePage from "./webpages/HomePage";
import LogIn from "./webpages/Authentication/LogIn";
import ForgotPassword from "./webpages/Authentication/ForgotPassword";
import SingUp from "./webpages/Authentication/SingUp";
import WithoutFamily from "./webpages/WithoutFamily";
import Dashboard from "./webpages/Dashboard";
import Calendar from "./webpages/Calendar";
import Messages from "./webpages/Messages";
import TaskLists from "./webpages/TaskList/TaskLists";
import EditProfile from "./webpages/EditProfile";
import ChangePassword from "./webpages/Authentication/ChangePassword";
import ShoppingLists from "./webpages/ShoppingList/ShoppingLists";
import TaskList from "./webpages/TaskList/TaskList";
import ShoppingList from "./webpages/ShoppingList/ShoppingList";
import Finance from "./webpages/Finance/Finance";
import FinancePerson from "./webpages/Finance/FinancePerson";
import FinanceNew from "./webpages/Finance/FinanceNew";
import Location from "./webpages/Location";
import Members from "./webpages/Members";
import PersonDetail from "./webpages/PersonDetail";
import InviteNewPerson from "./webpages/InviteNewPerson";
import Settings from "./webpages/Settings";
import ChangeFamilyName from "./webpages/ChangeFamilyName";
import My404 from "./webpages/My404";
import VerifyEmail from "./webpages/Authentication/VerifyEmail";

import {AuthProvider, useAuthState} from "./auth/Auth";
import NewFamily from "./webpages/NewFamily";
import NewTaskList from "./webpages/TaskList/NewTaskList";
import NewShoppingList from "./webpages/ShoppingList/NewShoppingList";
import {onMessageListener} from "./firebase";
import {toast, ToastContainer} from "react-toastify";


const AuthenticatedRoute = ({ component: C, ...props }) => {
    const { isAuthenticated } = useAuthState();

    return (
        <Route
            {...props}
            render={routeProps =>
                isAuthenticated ? <C {...routeProps} /> : <Redirect to="/login" />
            }
        />
    )
}

const App = () => {
    const [show, setShow] = useState(false);
    const [notification, setNotification] = useState({ title: "", body: "" });
    onMessageListener()
        .then((payload) => {
            setShow(true);
            setNotification({
                title: payload.notification.title,
                body: payload.notification.body,
            });

                let text = payload.notification.title;
                if(payload.notification.body !== undefined){
                    text += " - " + payload.notification.body;
                }
                toast(text);
        })
        .catch((err) => console.log("failed: ", err));


    return(
        <AuthProvider>
            <Router>
                <ToastContainer
                    position="top-center"
                    autoClose={6000}
                    hideProgressBar={false}
                    newestOnTop
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <Switch>
                    <Route path="/" exact component={HomePage} />
                    <Route path="/login" exact component={LogIn} />
                    <Route path="/forgot-password" exact component={ForgotPassword} />
                    <Route path="/singup" exact component={SingUp} />
                    <Route path="/verify-email" exact component={VerifyEmail} />
                    <AuthenticatedRoute path="/without-family" exact component={WithoutFamily} />
                    <AuthenticatedRoute path="/new-family" exact component={NewFamily} />
                    <AuthenticatedRoute path="/dashboard" exact component={Dashboard} />
                    <AuthenticatedRoute path="/calendar" exact component={Calendar} />
                    <AuthenticatedRoute path="/messenger" exact component={Messages} />
                    <AuthenticatedRoute path="/task-lists" exact component={TaskLists} />
                    <AuthenticatedRoute path="/task-lists/new" exact component={NewTaskList} />
                    <AuthenticatedRoute path="/task-lists/:id" exact component={TaskList} />
                    <AuthenticatedRoute path="/shopping-lists" exact component={ShoppingLists} />
                    <AuthenticatedRoute path="/shopping-lists/new" exact component={NewShoppingList} />
                    <AuthenticatedRoute path="/shopping-lists/:id" exact component={ShoppingList} />
                    <AuthenticatedRoute path="/finance" exact component={Finance} />
                    <AuthenticatedRoute path="/finance/:id" exact component={FinancePerson} />
                    <AuthenticatedRoute path="/finance/:id/new" exact component={FinanceNew} />
                    <AuthenticatedRoute path="/location" exact component={Location} />
                    <AuthenticatedRoute path="/members" exact component={Members} />
                    <AuthenticatedRoute path="/members/new" exact component={InviteNewPerson} />
                    <AuthenticatedRoute path="/members/:id" exact component={PersonDetail} />
                    <AuthenticatedRoute path="/settings" exact component={Settings} />
                    <AuthenticatedRoute path="/settings/family-name" exact component={ChangeFamilyName} />

                    <AuthenticatedRoute path="/edit-profile" exact component={EditProfile} />
                    <AuthenticatedRoute path="/change-password" exact component={ChangePassword} />


                    <Route path="*" exact component={My404} />

                </Switch>
            </Router>
        </AuthProvider>

    )
}

export default App;