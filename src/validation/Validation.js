export function validateEmail(value) {
    let error;
    if ( !value ) {
        error = 'Email is not filled';
    } else if ( !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ) {
        error = 'Invalid email address';
    }
    return error;
}

export function validatePassword(value) {
    let error;
    if (!value) {
        error = 'Password is not filled';
    }
    return error;
}

export function validateConfirmPassword(pass, value) {
    let error;

    if (!pass && value) {
        error = "Confirm the password."
    }else if ( (pass && value) && (pass !== value) ) {
        error = "Password not matched";
    }
    return error;
}

export function validateName(value) {
    let error;
    if( value.length < 3 || value.length > 20 ){
        error = 'At least 3 chars and at most 20 chars.';
    } else if (!value) {
        error = 'Name is not filled';
    }
    return error;
}

export function validateFamilyName(value) {
    let error;
    if( value.length < 3 || value.length > 20 ){
        error = 'At least 3 chars and at most 16 chars.';
    } else if (!value) {
        error = 'Family is not filled';
    }
    return error;
}

export function validatePhone(value) {
    let error;
    const phoneRegExp = /\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/;
    if ( value && value.length !== 0 && !phoneRegExp.test(value)) {
        error = 'Invalid phone number (format: +420555777111)';
    }
    return error;
}

export function validateTask(value) {
    let error;
    if (!value) {
        error = 'Username is not filled';
    }
    return error;
}

export function validateTitle(value) {
    let error;
    if (!value) {
        error = 'Title event is not filled';
    }else if(value.length < 3){
        error = 'At least 2 chars';
    }
    return error;
}

export function validateDates(startDate, startTime, endDate, endTime) {
    let error;


    if (!startDate || !startTime || !endDate || !endTime) {
        error = "Fill all dates and times"
    }else if (new Date(startDate + " " + startTime) > new Date(endDate + " " + endTime)) {
        error = "Start date is lower than end date";
    }
    return error;
}

export function validateDescription(value) {
    let error;
    if(!value){
        error = 'Description is not filled';
    }else if( value.length > 20 ){
        error = 'At most 20 chars';
    }
    return error;
}


export function validateDate(value) {
    let error;
    if( !value ){
        error = 'Date is not filled';
    }
    return error;
}

export function validateMoney(value) {
    let error;
    if ( !value ) {
        error = 'Money is not filled';
    } else if (value < 0) {
        error = 'Amount must be greater than zero';
    }
    return error;
}