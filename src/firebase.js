import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/auth';
import 'firebase/compat/storage';
import 'firebase/compat/messaging';

const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIREBASE_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID,
    appId: process.env.REACT_APP_MESSAGING_APP_ID,
    measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT
};

firebase.initializeApp(firebaseConfig);
const fireDb = firebase.firestore();
const messaging = firebase.messaging();

fireDb.enablePersistence().catch((err) => {
    if (err.code === 'failed-precondition') {
        console.log("Multiple tabs open, persistence can only be enabled in one tab at a time");
    } else if (err.code === 'unimplemented') {
        console.log("The current browser does not support all of the features required to enable persistence");
    }
});

export default firebase;

export const getToken = () => {
    return messaging.getToken({vapidKey: process.env.REACT_APP_MESSAGE_KEY}).then((currentToken) => {
        if (currentToken) {
            return currentToken;
        } else {
            console.log('No registration token available. Request permission to generate one.');
        }
    }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
    });
}

export const onMessageListener = () =>
    new Promise((resolve) => {
        messaging.onMessage((payload) => {
            resolve(payload);
        });
    });