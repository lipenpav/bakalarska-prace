import React from "react";
import ReactDom from "react-dom";
import './style/style.css';
import TimeAgo from 'javascript-time-ago'

import en from 'javascript-time-ago/locale/en.json';
import ru from 'javascript-time-ago/locale/ru.json';

import App from "./App";


TimeAgo.addDefaultLocale(en);
TimeAgo.addLocale(ru);

ReactDom.render(<App />, document.getElementById('root'));



