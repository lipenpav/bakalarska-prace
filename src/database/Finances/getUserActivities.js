import firebase from "../../firebase";

export const getUserActivities = (familyId, userId, setActivities) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('finances')
        .where('user', '==', userId)
        .orderBy('datetime', "desc")
        .onSnapshot((res1) => {
        firebase.firestore().collection('families').doc(familyId)
            .collection('finances')
            .where('lentToUser', '==', userId)
            .orderBy('datetime', "desc")
            .onSnapshot((res2) => {
                let tmp = [...res1.docs, ...res2.docs];
                tmp.sort((a, b) => a.data().datetime < b.data().datetime ? 1 : -1 );
            setActivities(tmp);
        })

    })
}