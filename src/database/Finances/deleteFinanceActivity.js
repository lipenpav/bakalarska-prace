import firebase from "../../firebase";

export const deleteFinanceActivity = (familyId, activityId) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('finances').doc(activityId).delete().then(() => {});
}