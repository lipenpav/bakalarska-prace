import firebase from "../../firebase";

export const createFinanceActivity = (familyId, description, user, lentToUser, price, datetime) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('finances').add({
        user: user,
        lentToUser: lentToUser,
        price: price,
        description: description,
        datetime: datetime,
        settleUp: false
    }).then();
}