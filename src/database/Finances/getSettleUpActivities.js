import firebase from "../../firebase";

export const getSettleUpActivities = (familyId, setActivities) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('finances')
        .where('settleUp', '==', false)
        .get().then((res) => {
            setActivities(res.docs);
    })
}