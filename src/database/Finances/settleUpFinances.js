import firebase from "../../firebase";

export const settleUpFinances = (familyId, user, lentUser, priceAccount, datetime) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('finances')
        .where("user", "==", user.uid)
        .where("lentToUser", "==", lentUser.uid)
        .where("settleUp", "==", false).get().then((res1) => {

            const batch = firebase.firestore().batch();
            res1.docs.forEach((r) => {
                batch.update(r.ref, {settleUp: true});
            });
            batch.commit().then();

            firebase.firestore().collection('families').doc(familyId)
                .collection('finances')
                .where("user", "==", lentUser.uid)
                .where("lentToUser", "==", user.uid)
                .where("settleUp", "==", false).get().then((res2) => {

                const batch = firebase.firestore().batch();
                res2.docs.forEach((r) => {
                    batch.update(r.ref, {settleUp: true});
                });
                batch.commit().then();

                firebase.firestore().collection("families").doc(familyId)
                    .collection("finances").add({
                    user: user.uid,
                    lentToUser: lentUser.uid,
                    price: priceAccount,
                    description: "Settle up",
                    datetime: datetime,
                    settleUp: true
                }).then();
        });
    });
}