import firebase from "../../firebase";


export const UpdateProfilePhoto = (currentUser, url) => {
    firebase.firestore().collection('users').doc(currentUser.uid).update({
        avatar: url
    }).then(() => {});
}