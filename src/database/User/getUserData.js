import firebase from "../../firebase";

export const getUserData = (uid, setUser) => {
    const db = firebase.firestore();
    db.collection('users').doc(uid).get().then((res) => {
        setUser(res.data());
    })
}