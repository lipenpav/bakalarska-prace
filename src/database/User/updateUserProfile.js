import firebase from "../../firebase";


export const updateUserProfile = (currentUser, name, email, phone, birthday, setSuccessMessage, setErrorMessage, setWarningMessage) => {
    setSuccessMessage(null);
    setErrorMessage(null);
    firebase.firestore().collection('users').doc(currentUser.uid).update({
        name: name,
        email: email.toLowerCase(),
        tel: phone,
        birthday: birthday
    }).then(() => {
        if(email !== currentUser.email){
            firebase.auth().currentUser.updateEmail(email).then(() => {
                setSuccessMessage(true);
                setWarningMessage("Use " + email + " to sign in now.");
            }).catch((err) => {
                setErrorMessage(err.message);
            })
        }else{
            setSuccessMessage(true);
        }

    }).catch((err) => setErrorMessage(err.message));
}