import firebase from "../../firebase";

export const createEvent = (familyId, start, end, allDay, title, color, ownerId) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('events').add({
        start: start,
        end: end,
        allDay: allDay,
        title: title,
        owner: ownerId,
        color: color
    })
}