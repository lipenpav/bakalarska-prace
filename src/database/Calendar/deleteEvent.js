import firebase from "../../firebase";

export const deleteEvent = (familyId, idEvent) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('events').doc(idEvent).delete().then();
}