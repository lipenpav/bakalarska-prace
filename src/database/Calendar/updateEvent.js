import firebase from "../../firebase";

export const updateEvent = (familyId, idEvent, start, end, allDay, title, color, ownerId) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('events').doc(idEvent).update({
        start: start,
        end: end,
        allDay: allDay,
        title: title,
        color: color
    })
}