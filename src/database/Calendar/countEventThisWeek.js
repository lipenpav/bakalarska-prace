import firebase from "../../firebase";
import moment from "moment";

const startThisWeek = moment().startOf('week').isoWeekday(1);
const endThisWeek = moment().endOf('week');



export const countEventThisWeek = (familyId, setCountEvent) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('events')
        .where('start', '>=', startThisWeek.format("YYYY-MM-DD HH:mm"))
        .get().then((r) => {
        const tmp = [];
        r.docs.forEach((item) => {
            if(item.data().start <= endThisWeek.format("YYYY-MM-DD HH:mm")){
                tmp.push(item.data());
            }
        })
        console.log(tmp);
        setCountEvent(tmp.length);
    })
}
