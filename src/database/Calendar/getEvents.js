import firebase from "../../firebase";

export const getEvents = (familyId, setEvents, setLoad) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('events').onSnapshot((snap) => {
            let tmp = [];
            snap.docs.forEach((r) => {
                tmp.push({
                    id: r.id,
                    start: new Date(r.data().start),
                    end: new Date(r.data().end),
                    allDay: r.data().allDay,
                    title: r.data().title,
                    owner: r.data().owner,
                    color: r.data().color
                });
            });
            setEvents([...tmp]);
            setLoad(true);
    });
}