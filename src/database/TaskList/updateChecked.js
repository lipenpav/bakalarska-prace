import firebase from "../../firebase";

export const updateChecked = (familyId, taskId, itemID, check, name) => {
    const increment = firebase.firestore.FieldValue.increment(1);
    const decrement = firebase.firestore.FieldValue.increment(-1);
    firebase.firestore().collection('families').doc(familyId)
        .collection('tasks').doc(taskId)
        .collection('items').doc(itemID).update({
        checked: check
    }).then(() => {
        if(check){
            firebase.firestore().collection('families').doc(familyId)
                .collection('tasks').doc(taskId).update({
                count: decrement
            })
        }else{
            firebase.firestore().collection('families').doc(familyId)
                .collection('tasks').doc(taskId).update({
                count: increment
            })
        }

    });
}