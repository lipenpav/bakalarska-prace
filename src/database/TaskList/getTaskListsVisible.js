import firebase from "../../firebase";

export const getTaskListsVisible = (familyId, setTaskLists) => {
    firebase.firestore().collection('families').doc(familyId).collection('tasks')
        .where('visible', '==', true).onSnapshot((snap) => {
            setTaskLists(snap.docs);
    })
}