import firebase from "../../firebase";

export const deleteTaskList = (familyId, listId) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('tasks').doc(listId).delete().then();
}