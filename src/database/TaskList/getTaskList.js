import firebase from "../../firebase";

export const getTaskList = (id, familyId, setList, setTasks, setId) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('tasks').doc(id).onSnapshot((snap) => {
            setList(snap.data());
            setId(snap.id);

    })
    firebase.firestore().collection('families').doc(familyId)
        .collection('tasks').doc(id).collection('items').orderBy('checked').orderBy('name').onSnapshot(r => {
        setTasks(r.docs);
    });
}