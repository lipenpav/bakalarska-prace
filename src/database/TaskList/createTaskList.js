import firebase from "../../firebase";

export const createTaskList = (familyId, name, currentUser, visible, setSuccessMessage, setErrorMessage) => {
    let tmpVisible = false;
    if(visible !== undefined)
        tmpVisible = visible;
    firebase.firestore().collection('families').doc(familyId).collection('tasks').add({
        name: name,
        owner: currentUser.uid,
        visible: tmpVisible,
        count: 0
    }).then(() => setSuccessMessage("To-do list has been created"))
        .catch((err) => setErrorMessage(err.message));
}