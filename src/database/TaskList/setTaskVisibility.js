import firebase from "../../firebase";

export const setTaskVisibility = (familyId, listId, visible) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('tasks').doc(listId).update({
        visible: visible
    }).then();
}