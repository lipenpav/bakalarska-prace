import firebase from "../../firebase";

export const createNewTask = (familyId, taskId, name, price) => {
    const increment = firebase.firestore.FieldValue.increment(1);
    firebase.firestore().collection('families').doc(familyId)
        .collection('tasks').doc(taskId)
        .collection('items').add({
        name: name,
        checked: false,
    }).then(() => {
        firebase.firestore().collection('families').doc(familyId)
            .collection('tasks').doc(taskId).update({
            count: increment
        }).then();
    })
}