import firebase from "../../firebase";

export const getTaskListsOfUser = (familyId, currentUser, setTasks) => {
    firebase.firestore().collection('families').doc(familyId).collection('tasks')
        .where('owner', '==', currentUser.uid).onSnapshot((snap) => {
            setTasks(snap.docs);
    })
}