import firebase from "../../firebase";

export const getMembersOfFamily = (currentFamily, currentUser, members, setMembers) => {
    firebase.firestore().collection('users_family')
        .where('family_id', '==', currentFamily.id)
        .onSnapshot((res) => {
            setMembers([]);
            res.docs.forEach((r) => {
                firebase.firestore().collection('users').doc(r.data().user_id).get().then((detailRes) => {
                    setMembers(members => ([...members, detailRes.data()]));
                }).catch((err) => console.log(err));
            });

    })
}