import firebase from "../../firebase";

export const getCountOfMembers = (familyId, setCount) => {
    firebase.firestore().collection('users_family')
        .where('family_id', '==', familyId)
        .get().then((res) => setCount(res.size))
}