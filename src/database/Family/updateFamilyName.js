import firebase from "../../firebase";


export const updateFamilyName = (name, currentFamily, setCurrentFamily, setSuccessMessage, setErrorMessage) => {
    firebase.firestore().collection('families').doc(currentFamily.id).update({
        name: name
    }).then((r) => {
        firebase.firestore().collection('users_family')
            .where('family_id', "==", currentFamily.id).get().then((res) => {
            console.log(res.docs);
            res.docs.forEach((doc) => {
                firebase.firestore().collection('users_family').doc(doc.id).update({
                    family_name: name
                }).then(() => {

                }).catch((err) => setErrorMessage(err.message))
            })
        }).then(() => {
            setSuccessMessage("Name of your family has been changed");
            setCurrentFamily({
                id: currentFamily.id,
                name: name,
                owner: currentFamily.owner
            });
        })
            .catch((err) => setErrorMessage(err.message))
    });
}