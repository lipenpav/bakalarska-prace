import firebase from "../../firebase";

export const deleteFamily = (familyId) => {
    firebase.firestore().collection("families").doc(familyId).delete().then(() => {
        firebase.firestore().collection("users_family")
            .where("family_id", "==", familyId)
            .get().then((res) => {
            res.forEach((i) => {
                i.ref.delete().then(() => {
                    firebase.firestore().collection("users_family")
                        .where("user_id", "==", i.data().user_id).get().then((res2) => {
                        if(res2.docs.length === 0){
                            firebase.firestore().collection('users').doc(i.data().user_id).update({
                                current_family: ""
                            }).then(() => {});
                        }else{
                            firebase.firestore().collection('users').doc(i.data().user_id).update({
                                current_family: res2.docs[0].data().family_id
                            }).then(() => {});
                        }
                    })
                });
            })
        })
    });


}