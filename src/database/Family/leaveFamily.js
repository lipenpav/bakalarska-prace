import firebase from "../../firebase";

export const leaveFamily = (familyId, userId) => {
    firebase.firestore().collection("users_family")
        .where("family_id", "==", familyId)
        .where("user_id", "==", userId)
        .get().then((res) => {
            firebase.firestore().collection('users_family').doc(res.docs[0].id).delete().then(() => {
                firebase.firestore().collection("users_family")
                    .where("user_id", "==", userId).get().then((res2) => {
                        if(res2.docs.length === 0){
                            firebase.firestore().collection('users').doc(userId).update({
                                current_family: ""
                            }).then(() => {});
                        }else{
                            firebase.firestore().collection('users').doc(userId).update({
                                current_family: res2.docs[0].data().family_id
                            }).then(() => {});
                        }
                })
            })
        const family = firebase.firestore().collection("families").doc(familyId);
            family.collection("locations").doc(userId).delete().then();
            family.collection("events").where("owner", "==", userId).get().then((r) => {
                r.forEach((i) => {i.ref.delete().then()})
            })
            family.collection("tasks").where("owner", "==", userId).get().then((r) => {
                r.forEach((i) => {i.ref.delete().then()})
            })
            family.collection("shoppings").where("owner", "==", userId).get().then((r) => {
                r.forEach((i) => {i.ref.delete().then()})
            })
            family.collection("finances").where("user", "==", userId).get().then((r) => {
                r.forEach((i) => {i.ref.delete().then()})
            })
            family.collection("finances").where("lentToUser", "==", userId).get().then((r) => {
                r.forEach((i) => {i.ref.delete().then()})
            })

    })
}