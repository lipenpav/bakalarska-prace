import firebase from "../../firebase";

export const createNewFamily = (name, setSuccessMessage, setErrorMessage, currentUser) => {
    const db = firebase.firestore();

    db.collection('families').add({
        id: "",
        name: name,
        owner: "",
    }).then(familyRes => {
        db.collection('families').doc(familyRes.id).update({
            id: familyRes.id,
            owner: currentUser.uid
        }).then(() => {
            db.collection('users').doc(currentUser.uid).update({
                current_family: familyRes.id
            }).then(userRes => {
                db.collection('users_family').add({
                    family_id: familyRes.id,
                    user_id: currentUser.uid,
                    family_name: name
                }).then(() => {
                    setSuccessMessage("The new family has been created.")
                }).catch((err) => {
                    setErrorMessage(err.message);
                })
            })
        })
    })
}
