import firebase from "../../firebase";

export const getDevicesByUser = (userId, setDevices) => {
    firebase.firestore().collection('devices')
        .where("userId", "==", userId).get().then(res => {
            setDevices(res);
    })
}