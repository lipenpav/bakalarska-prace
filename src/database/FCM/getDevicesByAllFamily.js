import firebase from "../../firebase";

export const getDevicesByAllFamily = (familyId, setDevices) => {
    let tmp = [];
    firebase.firestore().collection('users_family')
        .where('family_id', '==', familyId)
        .get().then(res => {
            res.docs.forEach(r => {
                firebase.firestore().collection('devices')
                    .where("userId", "==", r.data().user_id).get().then(devicesUser => {
                    tmp.push([...devicesUser.docs]);
                })
            });
        setDevices(tmp);
        })
}