import firebase from "../../firebase";

export const saveToken = (user, token) => {
    firebase.firestore().collection('devices').doc(token).set({
        token: token,
        userId: user.uid
    }).then();
}