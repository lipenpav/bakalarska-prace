import firebase from "../../firebase";

export const createMessage = (familyId, user, text) => {
    firebase.firestore().collection('families').doc(familyId).collection('messages').add({
        user_id: user.uid,
        avatar: user.avatar,
        text: text,
        user_name: user.name,
        timestamp: firebase.firestore.Timestamp.now()
    }).then().catch((err) => console.error(err));
}
