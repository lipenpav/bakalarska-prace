import firebase from "../../firebase";

export const getMessages = (familyId, messages, setMessages, latestDoc) => {
    if(latestDoc){
        firebase.firestore().collection('families').doc(familyId).collection('messages')
            .orderBy('timestamp', "desc")
            .startAfter(latestDoc)
            .limit(20)
            .onSnapshot((snap) => {
                setMessages(messages => [...messages, ...snap.docs]);
            });
    }else{
        firebase.firestore().collection('families').doc(familyId).collection('messages')
            .orderBy('timestamp', "desc")
            .limit(20)
            .onSnapshot((snap) => {
                setMessages(snap.docs);
            });
    }

}