import firebase from "../../firebase";

export const getLocations = (familyId, setLocations) => {
    firebase.firestore().collection('families').doc(familyId).collection('locations').onSnapshot((snapshot) => {
        setLocations(snapshot.docs);
    })
}