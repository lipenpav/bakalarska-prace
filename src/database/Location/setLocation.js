import firebase from "../../firebase";

export const setLocation = (familyId, userId, coordinates) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('locations').doc(userId).set({
            lat: coordinates.lat,
            lng: coordinates.lng,
            timestamp: firebase.firestore.Timestamp.now(),
            user_id: userId
        }).then();
}