import firebase from "../../firebase";

export const getShoppingListsOfUser = (familyId, currentUser, setTasks) => {
    firebase.firestore().collection('families').doc(familyId).collection('shoppings')
        .where('owner', '==', currentUser.uid).onSnapshot((snap) => {
        setTasks(snap.docs);
    })
}