import firebase from "../../firebase";

export const setShoppingVisibility = (familyId, listId, visible) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('shoppings').doc(listId).update({
        visible: visible
    }).then();
}