import firebase from "../../firebase";

export const deleteShoppingList = (familyId, listId) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('shoppings').doc(listId).delete().then();
}