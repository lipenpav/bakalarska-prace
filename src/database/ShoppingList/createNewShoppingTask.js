import firebase from "../../firebase";

export const createNewShoppingTask = (familyId, taskId, name, price) => {
    const increment = firebase.firestore.FieldValue.increment(1);
    firebase.firestore().collection('families').doc(familyId)
        .collection('shoppings').doc(taskId)
        .collection('items').add({
        name: name,
        checked: false,
        price: price
    }).then(() => {
        firebase.firestore().collection('families').doc(familyId)
            .collection('shoppings').doc(taskId).update({
            count: increment
        }).then();
    })
}