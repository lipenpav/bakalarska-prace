import firebase from "../../firebase";

export const updateCheckedShopping = (familyId, taskId, itemID, check) => {
    const increment = firebase.firestore.FieldValue.increment(1);
    const decrement = firebase.firestore.FieldValue.increment(-1);
    firebase.firestore().collection('families').doc(familyId)
        .collection('shoppings').doc(taskId)
        .collection('items').doc(itemID).update({
        checked: check
    }).then(() => {
        if(check){
            firebase.firestore().collection('families').doc(familyId)
                .collection('shoppings').doc(taskId).update({
                count: decrement
            })
        }else{
            firebase.firestore().collection('families').doc(familyId)
                .collection('shoppings').doc(taskId).update({
                count: increment
            })
        }

    });
}