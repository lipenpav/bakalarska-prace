import firebase from "../../firebase";

export const createShoppingList = (familyId, name, currentUser, visible, setSuccessMessage, setErrorMessage) => {
    let tmpVisible = false;
    if(visible !== undefined)
        tmpVisible = visible;
    firebase.firestore().collection('families').doc(familyId).collection('shoppings').add({
        name: name,
        owner: currentUser.uid,
        visible: tmpVisible,
        count: 0
    }).then(() => setSuccessMessage("Shopping list has been created"))
        .catch((err) => setErrorMessage(err.message));
}