import firebase from "../../firebase";

export const getShoppingList = (id, familyId, setList, setTasks, setId) => {
    firebase.firestore().collection('families').doc(familyId)
        .collection('shoppings').doc(id).onSnapshot((snap) => {
        setList(snap.data());
        setId(snap.id);

    })
    firebase.firestore().collection('families').doc(familyId)
        .collection('shoppings').doc(id).collection('items')
        .orderBy('checked').orderBy('name').onSnapshot(r => {
        setTasks(r.docs);
    });
}