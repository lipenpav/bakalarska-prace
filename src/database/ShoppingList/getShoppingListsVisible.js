import firebase from "../../firebase";

export const getShoppingListsVisible = (familyId, setTaskLists) => {
    firebase.firestore().collection('families').doc(familyId).collection('shoppings')
        .where('visible', '==', true).onSnapshot((snap) => {
        setTaskLists(snap.docs);
    })
}