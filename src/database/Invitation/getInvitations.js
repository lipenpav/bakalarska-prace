import firebase from "../../firebase";

export const getInvitations = (currentUser, setInvitations) => {
    firebase.firestore().collection('invitations')
        .where('invited_user_id', '==', currentUser.uid).onSnapshot((res) => {
            setInvitations(res.docs);
    })
}