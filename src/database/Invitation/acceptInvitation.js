import firebase from "../../firebase";
import {deleteInvitation} from "./deleteInvitation";

export const acceptInvitation = (idInvitation, setSuccessMessage, setErrorMessage) => {
    firebase.firestore().collection('invitations').doc(idInvitation).get().then((res) => {
        firebase.firestore().collection('users_family').add({
            family_id: res.data().family_id,
            family_name: res.data().family_name,
            user_id: res.data().invited_user_id
        }).then(() => {
                firebase.firestore().collection('users').doc(res.data().invited_user_id).update({
                    current_family: res.data().family_id
                }).then(() =>{
                    setSuccessMessage("You have accepted the invitation.");
                    deleteInvitation(idInvitation);
                });
        })
            .catch((err) => setErrorMessage(err.message))
    })
}