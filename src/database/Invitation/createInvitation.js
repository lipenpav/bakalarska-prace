import firebase from "../../firebase";


export const createInvitation = (email, currentUser, user, currentFamily, setSuccessMessage, setErrorMessage) => {
    firebase.firestore().collection('users').where('email', '==', email.toLowerCase()).get().then((doc) => {
        if(!doc.empty){
            doc.forEach((d)=>{
                if(d.data().email.toLowerCase() !== currentUser.email){
                    firebase.firestore().collection('invitations')
                        .where('family_id', '==', currentFamily.id)
                        .where('invited_user_id', '==', d.data().uid).get().then((res) => {
                        if(res.empty){
                            firebase.firestore().collection('users_family')
                                .where('family_id', '==', currentFamily.id)
                                .where('user_id', '==', d.data().uid).get().then((res) => {
                                if(res.empty){
                                    firebase.firestore().collection('invitations').add({
                                            family_id: currentFamily.id,
                                            family_name: currentFamily.name,
                                            invited_user_id: d.data().uid,
                                            creator_user_id: currentUser.uid,
                                            creator_user_name: user.name,
                                            creator_user_avatar: user.avatar
                                        }
                                    ).then(() => setSuccessMessage("The invitation has been sent."))
                                }else{
                                    setErrorMessage("The user is already in this family");
                                }
                            })
                        }else{
                            setErrorMessage("The invitation is already been sent.");
                        }
                    })
                }else{
                    setErrorMessage("You can't invite yourself.");
                }
            })
        }else{
            setErrorMessage("User with this email does not exist.");
        }
    })

}