import firebase from "../../firebase";

export const deleteInvitation = (id) => {
    firebase.firestore().collection('invitations').doc(id).delete().then();
}