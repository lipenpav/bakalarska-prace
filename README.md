# Family organizer

## Installation

- This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
- Install local repositories using `npm install`
- Then `npm start` to run development app
- `npm run build` to builds the app for production to the `build` folder.


- For dev run `npm start`

### Firebase

- Set authenticate tokens in file `.env` and same in `/public/firebase-messaging-sw.js`
- Upload backend validation to Firebase from file `firestore.rules`
- Create list of indexes in Firebase UI from `firestore.indexes.json` or install Firebase CLI by `npm install -g firebase-tools` and then run `firebase deploy --only firestore:indexes`

## Tests
- Run `firebase emulators:start` (first install) to emulate Firestore localy
- Run tests `npm run test`

## Tech stack

- **frontend**: React, Tailwind CSS
- **backend**: Firebase Firestore, Firebase Authentication, Firebase Cloud Storage